<?php

namespace App\Traits;

use File;
use ImgUploader;
use Auth;
use DB;
use Input;
use Carbon\Carbon;
use Redirect;
use App\User;
use App\ProfileCv;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Requests\ProfileCvFormRequest;
use App\Http\Requests\ProfileCvFileFormRequest;
use Illuminate\Support\Facades\Gate;

trait ProfileCvsTrait
{

    public function showProfileCvs(Request $request, $user_id)
    {
        $user = $post = User::find($user_id);

        if (!Gate::allows('check-user', $post) && !Auth::guard('admin')->user()) {
            echo view('errors.404')->render();
            return;
        }

        $html = '<div class="col-mid-12"><table class="table table-bordered table-condensed">';
        if (isset($user) && count($user->profileCvs)):
            $cvCounter = 0;
            foreach ($user->profileCvs as $cv):
                $default = 'Not Default';
                if ($cv->is_default == 1)
                    $default = 'Default';

                if ($cv->title ==''){
                    continue;
                }

                $html .= '<tr id="cv_' . $cv->id . '">
									<td>' . ImgUploader::get_doc("cvs/$cv->cv_file", $cv->title, $cv->title) . '</td>
									<td><span class="text fnt-theme-dark">' . $default . '</span></td>
									<td><a class="fnt-theme" href="javascripr:;" onclick="showProfileCvEditModal(' . $cv->id . ');" class="text text-warning">' . __('Edit') . '</a>&nbsp;|&nbsp;<a href="javascripr:;" onclick="delete_profile_cv(' . $cv->id . ');" class="text text-danger">' . __('Delete') . '</a></td>
								</tr>';
            endforeach;
        endif;

        echo $html . '</table></div>';
    }

    public function showProfileCvsSelect(Request $request, $user_id)
    {
        $user = $post = User::find($user_id);

        if (!Gate::allows('check-user', $post) && !Auth::guard('admin')->user()) {
            echo view('errors.404')->render();
        }

        $html = '<select class="form-control" id="cv_id" name="cv_id">';
        $html .= '<option value="" selected="selected">Select CV</option>';
        if (isset($user) && count($user->profileCvs)):
            foreach ($user->profileCvs as $cv):
                if ($cv->title ==''){
                    continue;
                }
                $html .= '<option value="'.$cv->id.'">'.$cv->title.'</option>';
            endforeach;
        endif;

        echo $html . '</select>';
    }

    public function uploadCvFile($request)
    {
        $fileName = '';
        if ($request->hasFile('cv_file')) {
            $cv_file = $request->file('cv_file');
            $fileName = ImgUploader::UploadDoc('cvs', $cv_file, $request->input('title'));
        }
        return $fileName;
    }

    public function getFrontProfileCvForm(Request $request, $user_id)
    {
        $user = $post = User::find($user_id);

        if (!Gate::allows('check-user', $post)) {
            $returnHTMLBody = view('errors.404')->render();
            $returnHTMLFooter = '<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>';
            $returnHTMLForm = '';
			$title = __('Error 404');
            return response()->json(array('success' => false,
                'html_body' => $returnHTMLBody, 'html_footer' => $returnHTMLFooter, 'html_form' => $returnHTMLForm,'title' => $title),404);
        }

        $returnHTMLBody = view('user.forms.cv.cv_modal_body')->with('user', $user)->render();
        $returnHTMLFooter = view('user.forms.cv.cv_modal_footer')->with('user', $user)->render();
        $returnHTMLForm = view('user.forms.cv.cv_modal_form')->with('user', $user)->render();
		$title = __('Add CV');
        return response()->json(array('success' => true,
            'html_body' => $returnHTMLBody, 'html_footer' => $returnHTMLFooter, 'html_form' => $returnHTMLForm,'title' => $title));
    }

    public function getProfileCvForm(Request $request, $user_id)
    {
        $user = $post = User::find($user_id);

        if (!Gate::allows('check-user', $post) && !Auth::guard('admin')->user()) {
            return response()->json(array('success' => false, 'html' => view('errors.404')->render()),404);
        }

        $returnHTML = view('admin.user.forms.cv.cv_modal')->with('user', $user)->render();
        return response()->json(array('success' => true, 'html' => $returnHTML));
    }

    public function storeProfileCv(ProfileCvFormRequest $request, $user_id)
    {
//        dd($request->file('cv_file')->getMimeType());die();
        $user = $post = User::find($user_id);

        $cv_file = $request->file('cv_file');

        if(strtolower($cv_file->getClientOriginalExtension()) != 'doc' AND
           strtolower($cv_file->getClientOriginalExtension()) != 'docx' AND
           strtolower($cv_file->getClientOriginalExtension()) != 'pdf'){
            return response()->json(array('message' => 'The given data was invalid.', 'errors' => array('cv_file'=>array('Only PDF and DOC and DOCX files can be uploaded.'))),422);
        }


        if (!Gate::allows('check-user', $post) && !Auth::guard('admin')->user()) {
            return response()->json(array('success' => false, 'html' => view('errors.404')->render()),404);
        }

        $profileCv = new ProfileCv();
        $profileCv = $this->assignValues($profileCv, $request, $user_id);
        $profileCv->save();

        $returnHTML = view('admin.user.forms.cv.cv_thanks')->render();
        return response()->json(array('success' => true, 'status' => 200, 'html' => $returnHTML), 200);
    }

    public function storeFrontProfileCv(ProfileCvFormRequest $request, $user_id)
    {
        $user = $post = User::find($user_id);

        if (!Gate::allows('check-user', $post)) {
            $returnHTMLBody = view('errors.404')->render();
            $returnHTMLFooter = '<button type="button" class="btn btn-theme" data-dismiss="modal">Close</button>';
            $returnHTMLForm = '';
            $title = __('Error 404');
            return response()->json(array('success' => false,
                'html_body' => $returnHTMLBody, 'html_footer' => $returnHTMLFooter, 'html_form' => $returnHTMLForm,'title' => $title),404);
        }

        $profileCv = new ProfileCv();
        $profileCv = $this->assignValues($profileCv, $request, $user_id);
        $profileCv->save();

        $returnHTMLBody = view('user.forms.cv.cv_thanks')->render();
        $returnHTMLFooter = '<button type="button" class="btn btn-theme" data-dismiss="modal">Close</button>';
        $returnHTMLForm = '';
        $title = __('Add CV');
        return response()->json(array('success' => true, 'status' => 200,
            'html_body' => $returnHTMLBody, 'html_footer' => $returnHTMLFooter, 'html_form' => $returnHTMLForm,'title' => $title), 200);

    }

    private function assignValues($profileCv, $request, $user_id)
    {
        $user = $post = User::find($user_id);

        if (!Gate::allows('check-user', $post) && !Auth::guard('admin')->user()) {
            return response()->json(array('success' => false, 'html' => view('errors.404')->render()),404);
        }

        $profileCv->user_id = $user_id;
        $profileCv->title = $request->input('title');
        $profileCv->is_default = $request->input('is_default');

        /*         * ************************************ */
        if ((int) $request->input('is_default') == 1) {
            $this->updateDefaultCv($profileCv->id);
        }
        /*         * ************************************ */
        if ($request->hasFile('cv_file') && $profileCv->id === null){ //add cv
            $profileCv->cv_file = $this->uploadCvFile($request);
        }
        if ($request->hasFile('cv_file') && $profileCv->id > 0) { //edit cv file exists
            $this->deleteCv($profileCv->id);
            $profileCv->cv_file = $this->uploadCvFile($request);
        }
        if (!$request->hasFile('cv_file') && $profileCv->id > 0) { //edit cv file not exists

        }

        return $profileCv;
    }

    public function getProfileCvEditForm(Request $request, $user_id)
    {
        $user = $post = User::find($user_id);

        if (!Gate::allows('check-user', $post) && !Auth::guard('admin')->user()) {
            return response()->json(array('success' => false, 'html' => view('errors.404')->render()),404);
        }

        $cv_id = $request->input('cv_id');
        $profileCv = ProfileCv::find($cv_id);
        $user = User::find($user_id);
        $returnHTML = view('admin.user.forms.cv.cv_edit_modal')
                ->with('user', $user)
                ->with('profileCv', $profileCv)
                ->render();
        return response()->json(array('success' => true, 'html' => $returnHTML));
    }

    public function getFrontProfileCvEditForm(Request $request, $user_id)
    {
        $user = $post = User::find($user_id);

        if (!Gate::allows('check-user', $post)) {
            $returnHTMLBody = view('errors.404')->render();
            $returnHTMLFooter = '<button type="button" class="btn btn-theme" data-dismiss="modal">Close</button>';
            $returnHTMLForm = '';
            $title = __('Error 404');
            return response()->json(array('success' => false,
                'html_body' => $returnHTMLBody, 'html_footer' => $returnHTMLFooter, 'html_form' => $returnHTMLForm,'title' => $title),404);
        }

        $cv_id = $request->input('cv_id');
        $profileCv = ProfileCv::find($cv_id);
        $user = User::find($user_id);
        $returnHTMLBody = view('user.forms.cv.cv_edit_modal_body')
                ->with('user', $user)
                ->with('profileCv', $profileCv)
                ->render();
        $returnHTMLFooter = view('user.forms.cv.cv_edit_modal_footer')
            ->with('user', $user)
            ->with('profileCv', $profileCv)
            ->render();
        $returnHTMLForm = view('user.forms.cv.cv_edit_modal_form')
            ->with('user', $user)
            ->with('profileCv', $profileCv)
            ->render();
		$title = __('Edit CV');
        return response()->json(array('success' => true,
            'html_body' => $returnHTMLBody, 'html_footer' => $returnHTMLFooter, 'html_form' => $returnHTMLForm,'title' => $title));
    }

    public function updateProfileCv(ProfileCvFormRequest $request, $cv_id, $user_id)
    {
        $user = $post = User::find($user_id);

        if (!Gate::allows('check-user', $post) && !Auth::guard('admin')->user()) {
            return response()->json(array('success' => false, 'html' => view('errors.404')->render()),404);
        }

        $profileCv = ProfileCv::find($cv_id);
        $profileCv = $this->assignValues($profileCv, $request, $user_id);
        $profileCv->update();

        $returnHTML = view('admin.user.forms.cv.cv_edit_thanks')->render();
        return response()->json(array('success' => true, 'status' => 200, 'html' => $returnHTML), 200);
    }

    public function updateFrontProfileCv(ProfileCvFormRequest $request, $cv_id, $user_id)
    {
        $user = $post = User::find($user_id);

        if (!Gate::allows('check-user', $post)) {
            $returnHTMLBody = view('errors.404')->render();
            $returnHTMLFooter = '<button type="button" class="btn btn-theme" data-dismiss="modal">Close</button>';
            $returnHTMLForm = '';
            $title = __('Error 404');
            return response()->json(array('success' => false,
                'html_body' => $returnHTMLBody, 'html_footer' => $returnHTMLFooter, 'html_form' => $returnHTMLForm,'title' => $title),404);
        }

//////
//        $profileCvs = ProfileCv::find($cv_id)->where('user_id', '=', $user_id)->get();
//        foreach ($profileCvs as $profileCv) {
//            echo $this->removeProfileCv($profileCv->id);
//            $this->deleteCv($profileCv->id);
//        }
//////
        $profileCv = ProfileCv::find($cv_id);

//        if($request->hasFile('cv_file')){
//            $this->deleteCv($profileCv->id);
//        }


        $profileCv = $this->assignValues($profileCv, $request, $user_id);
        $profileCv->update();
        
        $returnHTMLBody = view('user.forms.cv.cv_edit_thanks')->render();
        $returnHTMLFooter = '<button type="button" class="btn btn-theme" data-dismiss="modal">Close</button>';
        $returnHTMLForm = '';
        $title = __('Edit CV');
        return response()->json(array('success' => true, 'status' => 200,
            'html_body' => $returnHTMLBody, 'html_footer' => $returnHTMLFooter, 'html_form' => $returnHTMLForm,'title' => $title), 200);

    }

    public function makeDefaultCv(Request $request)
    {
        $id = $request->input('id');
        try {
            $profileCv = ProfileCv::findOrFail($id);
            $profileCv->is_default = 1;
            $profileCv->update();
            $this->updateDefaultCv($id);
            echo 'ok';
        } catch (ModelNotFoundException $e) {
            echo 'notok';
        }
    }

    private function updateDefaultCv($cv_id)
    {
        ProfileCv::where('is_default', '=', 1)->where('id', '<>', $cv_id)->update(['is_default' => 0]);
    }

    public function deleteAllProfileCvs($user_id)
    {
        $user = $post = User::find($user_id);

        if (!Gate::allows('check-user', $post) && !Auth::guard('admin')->user()) {
            echo view('errors.404')->render();
            return;
        }

        $profileCvs = ProfileCv::where('user_id', '=', $user_id)->get();
        foreach ($profileCvs as $profileCv) {
            echo $this->removeProfileCv($profileCv->id);
        }
    }

    public function deleteProfileCv(Request $request)
    {
        $id = $request->input('id');
        echo $this->removeProfileCv($id);
    }

    private function removeProfileCv($id)
    {
        try {
            $profileCv = $post = ProfileCv::findOrFail($id);

            if (!Gate::allows('check-user-with-user-id', $post) && !Auth::guard('admin')->user()) {
                return 'notok';
            }
            $this->deleteCv($id);
            $profileCv->delete();
            return 'ok';
        } catch (ModelNotFoundException $e) {
            return 'notok';
        }
    }

    private function deleteCv($id)
    {
        try {
            $profileCv = $post = ProfileCv::findOrFail($id);

            if (!Gate::allows('check-user-with-user-id', $post) && !Auth::guard('admin')->user()) {
                return 'notok';
            }

            $file = $profileCv->cv_file;
            if (!empty($file)) {
                File::delete(ImgUploader::real_public_path() . 'cvs/' . $file);
            }
            return 'ok';
        } catch (ModelNotFoundException $e) {
            return 'notok';
        }
    }

}
