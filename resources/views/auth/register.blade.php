@extends('layouts.app')
@section('content')
<!-- Header start -->
@include('includes.header')
<!-- Header end -->
<!-- Inner Page Title start -->
<?php /*@include('includes.inner_page_title', ['page_title'=>__('Register')]) */ ?>
<!-- Inner Page Title end -->
<section class="py-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12 mt-5">
                @include('flash::message')
                <div class="row">
                    <div class="col-md-6 offset-md-3">
                        <div class="userccount userbtns">
                            <ul class="nav nav-tabs">
                                <?php
                                $c_or_e = old('candidate_or_employer', 'candidate');
                                ?>
                                <li class="nav-item">
                                    <a class="nav-link {{($c_or_e == 'candidate')? 'active':''}}" href="#candidate" role="tab" data-toggle="tab" aria-expanded="true">{{__('Candidate')}}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link {{($c_or_e == 'employer')? 'active':''}}" href="#employer" role="tab" data-toggle="tab" aria-expanded="false">{{__('Employer')}}</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div id="candidate" class="formpanel tab-pane fade {{($c_or_e == 'candidate')? 'active in show':''}}">
                                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="candidate_or_employer" value="candidate" />
                                        <div class="formrow{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                            <input type="text" name="first_name" class="form-control" required="required"
                                                   placeholder="{{__('First Name').' '.__('(required)')}}"
                                                   value="{{old('first_name')}}">
                                            @if ($errors->has('first_name')) <span class="help-block"> <strong>{{ $errors->first('first_name') }}</strong> </span> @endif </div>
                                        <div class="formrow{{ $errors->has('middle_name') ? ' has-error' : '' }}">
                                            <input type="text" name="middle_name" class="form-control"
                                                   placeholder="{{__('Middle Name')}}"
                                                   value="{{old('middle_name')}}">
                                            @if ($errors->has('middle_name')) <span class="help-block"> <strong>{{ $errors->first('middle_name') }}</strong> </span> @endif </div>
                                        <div class="formrow{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                            <input type="text" name="last_name" class="form-control" required="required"
                                                   placeholder="{{__('Last Name').' '.__('(required)')}}"
                                                   value="{{old('last_name')}}">
                                            @if ($errors->has('last_name')) <span class="help-block"> <strong>{{ $errors->first('last_name') }}</strong> </span> @endif </div>
                                        <div class="formrow{{ $errors->has('email') ? ' has-error' : '' }}">
                                            <input type="email" name="email" class="form-control" required="required"
                                                   placeholder="{{__('Email').' '.__('(required)')}}"
                                                   value="{{old('email')}}">
                                            @if ($errors->has('email')) <span class="help-block"> <strong>{{ $errors->first('email') }}</strong> </span> @endif </div>
                                        <div class="formrow{{ $errors->has('password') ? ' has-error' : '' }}">
                                            <div class="input-group">
                                                <input type="password" id="password" name="password" class="form-control" required="required"
                                                   placeholder="{{__('Password').' '.__('(required)')}}"
                                                   value="" data-toggle="password" data-toggle="password">
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i class="fa fa-eye-slash"></i></span>
                                                </div>
                                            </div>
                                            @if ($errors->has('password')) <span class="help-block"> <strong>{{ $errors->first('password') }}</strong> </span> @endif </div>
                                        <div class="formrow{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                            <div class="input-group">
                                                <input type="password" name="password_confirmation" class="form-control" required="required"
                                                       placeholder="{{__('Password Confirmation').' '.__('(required)')}}"
                                                       value="" data-toggle="password">
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i class="fa fa-eye-slash"></i></span>
                                                </div>
                                            </div>
                                            @if ($errors->has('password_confirmation')) <span class="help-block"> <strong>{{ $errors->first('password_confirmation') }}</strong> </span> @endif </div>
                                            <div class="formrow{{ $errors->has('is_subscribed') ? ' has-error' : '' }}">
            <?php
            $is_checked = '';
            if (old('is_subscribed', 1)) {
                $is_checked = 'checked="checked"';
            }
            ?>

                                            <input type="checkbox" value="1" name="is_subscribed" {{$is_checked}} />{{__('Subscribe to news letter')}}
                                            @if ($errors->has('is_subscribed')) <span class="help-block"> <strong>{{ $errors->first('is_subscribed') }}</strong> </span> @endif </div>


                                        <div class="formrow{{ $errors->has('terms_of_use') ? ' has-error' : '' }}">
                                            <input type="checkbox" value="1" name="terms_of_use" />
                                            <a class="fnt-theme" href="{{url('cms/terms-of-use')}}">{{__('I accept Terms of Use')}}</a>

                                            @if ($errors->has('terms_of_use')) <span class="help-block"> <strong>{{ $errors->first('terms_of_use') }}</strong> </span> @endif </div>
                                        <div class="formrow{{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}"> {!! app('captcha')->display() !!}
                                            @if ($errors->has('g-recaptcha-response')) <span class="help-block"> <strong>{{ $errors->first('g-recaptcha-response') }}</strong> </span> @endif </div>
                                        <input type="submit" class="btn btn-theme" value="{{__('Register')}}">
                                    </form>
                                </div>
                                <div id="employer" class="formpanel tab-pane fade {{($c_or_e == 'employer')? 'active in show':''}}">
                                    <form class="form-horizontal" method="POST" action="{{ route('company.register') }}">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="candidate_or_employer" value="employer" />
                                        <div class="formrow{{ $errors->has('name') ? ' has-error' : '' }}">
                                            <input type="text" name="name" class="form-control" required="required"
                                                   placeholder="{{__('Name').' '.__('(required)')}}"
                                                   value="{{old('name')}}">
                                            @if ($errors->has('name')) <span class="help-block"> <strong>{{ $errors->first('name') }}</strong> </span> @endif </div>
                                        <div class="formrow{{ $errors->has('email') ? ' has-error' : '' }}">
                                            <input type="email" name="email" class="form-control" required="required"
                                                   placeholder="{{__('Email').' '.__('(required)')}}"
                                                   value="{{old('email')}}">
                                            @if ($errors->has('email')) <span class="help-block"> <strong>{{ $errors->first('email') }}</strong> </span> @endif </div>
                                        <div class="formrow{{ $errors->has('password') ? ' has-error' : '' }}">
                                            <div class="input-group">
                                                <input type="password" name="password" class="form-control" required="required"
                                                       placeholder="{{__('Password').' '.__('(required)')}}"
                                                       value="" data-toggle="password">
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i class="fa fa-eye-slash"></i></span>
                                                </div>
                                            </div>
                                        @if ($errors->has('password')) <span class="help-block"> <strong>{{ $errors->first('password') }}</strong> </span> @endif </div>
                                        <div class="formrow{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                            <div class="input-group">
                                                <input type="password" name="password_confirmation" class="form-control" required="required"
                                                       placeholder="{{__('Password Confirmation').' '.__('(required)')}}"
                                                       value="" data-toggle="password">
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i class="fa fa-eye-slash"></i></span>
                                                </div>
                                            </div>
                                            @if ($errors->has('password_confirmation')) <span class="help-block"> <strong>{{ $errors->first('password_confirmation') }}</strong> </span> @endif </div>
                                            <div class="formrow{{ $errors->has('is_subscribed') ? ' has-error' : '' }}">
            <?php
            $is_checked = '';
            if (old('is_subscribed', 1)) {
                $is_checked = 'checked="checked"';
            }
            ?>

                                            <input type="checkbox" value="1" name="is_subscribed" {{$is_checked}} />{{__('Subscribe to news letter')}}
                                            @if ($errors->has('is_subscribed')) <span class="help-block"> <strong>{{ $errors->first('is_subscribed') }}</strong> </span> @endif </div>
                                        <div class="formrow{{ $errors->has('terms_of_use') ? ' has-error' : '' }}">
                                            <input type="checkbox" value="1" name="terms_of_use" />
                                            <a class="fnt-theme" href="{{url('terms-of-use')}}">{{__('I accept Terms of Use')}}</a>

                                            @if ($errors->has('terms_of_use')) <span class="help-block"> <strong>{{ $errors->first('terms_of_use') }}</strong> </span> @endif </div>
                                        <div class="formrow{{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}"> {!! app('captcha')->display() !!}
                                            @if ($errors->has('g-recaptcha-response')) <span class="help-block"> <strong>{{ $errors->first('g-recaptcha-response') }}</strong> </span> @endif </div>
                                        <input type="submit" class="btn btn-theme" value="{{__('Register')}}">
                                    </form>
                                </div>
                            </div>
                            <!-- sign up form -->
                            <div class="newuser"><i class="fa fa-user" aria-hidden="true"></i> {{__('Have Account')}}? <a class="fnt-theme" href="{{route('login')}}">{{__('Sign in')}}</a></div>
                            <!-- sign up form end-->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('includes.footer')
@endsection