<?php

namespace App\Traits;

use App\JobLocationManager;
use App\City;
use App\Country;
trait Location
{

    private function storeJobLocation($request, $job_id , $Joblocations)
    {
        JobLocationManager::where('job_id', '=', $job_id)->delete();
        foreach ($Joblocations as $key2 => $value2) {
            $country_id='locations_'.$key2;
             if ($request->has($country_id)) { 
                    $locations = $request->input($country_id);
            foreach ($locations as $key => $value) {
                $JobLocationManager = new JobLocationManager();
                $JobLocationManager->job_id = $job_id;
                $JobLocationManager->city_id = $value; 
                $JobLocationManager->state_id = $this->Stateid($value);
                $JobLocationManager->country_id=$key2;
                $JobLocationManager->is_city=1; 
                $JobLocationManager->save();
            }
             }
        } 
    }
    private function getJobLocation($country_id , $id)
    { 
        $array = array();
        if($country_id==0){
            $cityes = JobLocationManager::where('is_city', 0)
                ->where('job_id', $id) 
                ->get(['country_id']);
            foreach ($cityes as $key => $value) { 
                $array[$key] = $value->country_id; 
            }
        }else{
            $cityes = JobLocationManager::where('country_id', $country_id) 
                ->where('job_id', $id)
                ->get(['city_id']);
            foreach ($cityes as $key => $value) { 
                $array[$key] = $value->city_id;
            }
        }  
        return $array;
    }
    private function getCountryList($id)
    {
        $array = array();
        $cityes = JobLocationManager::where('is_city', 1)
            ->where('job_id', $id) 
            ->groupBy('country_id')
            ->get(['country_id']);
        foreach ($cityes as $key => $value) { 
            $array[$key] = $value->country_id; 
        } 
        $cityes2 = JobLocationManager::where('is_city', 0)
            ->where('job_id', $id)  
            ->first(['country_id']); 
        if(!empty($cityes2)){ 
            array_push($array,0);
        }
        return $array;
    }
    private function Stateid($city_id)
    { 
        $stateid = City::where('city_id', $city_id)  
            ->get(['state_id']);
        foreach ($stateid as $key => $value) { 
             return $value->state_id;
        } 
        return 0;
    }
    private function countryname($countryid)
    { 
        $stateid = Country::where('country_id', $countryid)->lang()
            ->get(['country']);
        foreach ($stateid as $key => $value) { 
             return $value->country;
        } 
        return 0;
    }

}
