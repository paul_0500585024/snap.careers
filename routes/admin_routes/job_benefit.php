<?php

/* * ******  JobBenefit Start ********** */
Route::get('list-job-benefits', array_merge(['uses' => 'Admin\JobBenefitController@indexJobBenefits'], $all_users))->name('list.job.benefits');
Route::get('create-job-benefit', array_merge(['uses' => 'Admin\JobBenefitController@createJobBenefit'], $all_users))->name('create.job.benefit');
Route::post('store-job-benefit', array_merge(['uses' => 'Admin\JobBenefitController@storeJobBenefit'], $all_users))->name('store.job.benefit');
Route::get('edit-job-benefit/{id}', array_merge(['uses' => 'Admin\JobBenefitController@editJobBenefit'], $all_users))->name('edit.job.benefit');
Route::put('update-job-benefit/{id}', array_merge(['uses' => 'Admin\JobBenefitController@updateJobBenefit'], $all_users))->name('update.job.benefit');
Route::delete('delete-job-benefit', array_merge(['uses' => 'Admin\JobBenefitController@deleteJobBenefit'], $all_users))->name('delete.job.benefit');
Route::get('fetch-job.benefits', array_merge(['uses' => 'Admin\JobBenefitController@fetchJobBenefitsData'], $all_users))->name('fetch.data.job.benefits');
Route::put('make-active-job-benefit', array_merge(['uses' => 'Admin\JobBenefitController@makeActiveJobBenefit'], $all_users))->name('make.active.job.benefit');
Route::put('make-not-active-job-benefit', array_merge(['uses' => 'Admin\JobBenefitController@makeNotActiveJobBenefit'], $all_users))->name('make.not.active.job.benefit');
Route::get('sort-job-benefits', array_merge(['uses' => 'Admin\JobBenefitController@sortJobBenefits'], $all_users))->name('sort.job.benefits');
Route::get('job-benefit-sort-data', array_merge(['uses' => 'Admin\JobBenefitController@jobBenefitSortData'], $all_users))->name('job.benefit.sort.data');
Route::put('job-benefit-sort-update', array_merge(['uses' => 'Admin\JobBenefitController@jobBenefitSortUpdate'], $all_users))->name('job.benefit.sort.update');
/* * ****** End JobBenefit ********** */