<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CountryDetailFormRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
            case 'POST': {
                    $id = (int) $this->input('id', 0);
                    //country_id
                    $country_id = config('rules.id_rule');
                    //phone_code
                    $phone_code = config('rules.id_rule');
                    //currency
                    $currency = config('rules.currency');
                    //code
                    $code = config('rules.alnum5_rule');
                    //symbol
                    $symbol = config('rules.alnum10_rule');
                    //thousand_separator
                    $thousand_separator = config('rules.alnum2_rule');
                    //decimal_separator
                    $decimal_separator = config('rules.alnum2_rule');

                    $id = array();
                    return [
                        'id' => $id,
                        'country_id' => $country_id,
                        'phone_code' => $phone_code,
                        'currency' => $currency,
                        'code' => $code,
                        'symbol' => $symbol,
                        'thousand_separator' => $thousand_separator,
                        'decimal_separator' => $decimal_separator,
        //                        'id' => $id,
        //                        'country_id' => '',
        //                        'sort_name' => 'required',
        //                        'phone_code' => 'required',
        //                        'currency' => 'required',
        //                        'code' => 'required',
        //                        'symbol' => 'required',
        //                        'thousand_separator' => 'required',
        //                        'decimal_separator' => 'required',
                    ];
                }
            default:break;
        }
    }

    public function messages()
    {
        return [
/*            'sort_name.required' => 'Sort Name required',
            'phone_code.required' => 'Text Name required',
            'currency.required' => 'Currency required',
            'code.required' => 'Code required',
            'symbol.required' => 'Symbol required',
            'thousand_separator.required' => 'Thousand separator required',
            'decimal_separator.required' => 'Decimal separator required',*/
        ];
    }

}
