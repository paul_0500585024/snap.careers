<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TestimonialFormRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
            case 'POST': {
                    $id = (int) $this->input('id', 0);
                    $testimonial_unique = '';
                    if ($id > 0) {
                        $testimonial_unique = ',id,' . $id;
                    }

            //testimonial_by
            $testimonial_by = config('rules.alnum150_rule');
            //testimonial
            $testimonial = config('rules.alnum300_rule');
            //company
            $company = config('rules.alnum150_rule');
            //testimonial_id
            $testimonial_id = array();
            $is_default = (int) $this->input('is_default', 0);
            if ($is_default == 0) {
                $testimonial_id = config('rules.id_rule');
            }
            //is_active
            $is_active = config('rules.bool_rule');
            //is_default
            $is_default = config('rules.bool_rule');
            //lang
            $lang = config('rules.lang_rule');

            $id = array();

                    return [
                        "id" => $id,
                        "testimonial_by" => $testimonial_by,
                        "testimonial" => $testimonial,
                        "company" => $company,
                        "is_default" => $is_default,
                        "testimonial_id" => $testimonial_id,
                        "is_active" => $is_active,
                        "lang" => $lang,


//                        "id" => "",
//                        "lang" => "required|max:10",
//                        "testimonial_by" => "required|max:100",
//                        "testimonial" => "required|max:600",
//                        "company" => "required|max:150",
//                        "is_default" => "required|boolean",
//                        "testimonial_id" => "required_if:is_default,0",
//                        "is_active" => "required|boolean",
                    ];
                }
            default:break;
        }
    }

    public function messages()
    {
        return [
/*            'lang.required' => 'Please select language.',
            'testimonial_by.required' => 'Testimonial by required.',
            'testimonial.required' => 'Testimonial required.',
            'company.required' => 'Company required.',
            'is_default.required' => 'Is this Testimonial default?',
            'testimonial_id.required_if' => 'Please select default/fallback Testimonial.',
            'is_active.required' => 'Is this Testimonial active?',*/
        ];
    }

}
