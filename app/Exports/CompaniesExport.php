<?php

namespace App\Exports;

use App\Company;
use Illuminate\Support\Collection;
//use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromQuery;

class CompaniesExport implements FromQuery, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function query()
    {

$query = Company::query()->select('companies.id','companies.name','companies.email','companies.ceo','companies.pic_name','companies.country_phone_pic_phone AS cppp','companies.pic_phone','industries.industry',
    'ownership_types.ownership_type','companies.description','companies.no_of_offices','companies.website','companies.no_of_employees',
    'companies.established_in','companies.fax','companies.country_phone_phone AS cpp','companies.phone','countries.country','states.state',
    'cities.city','companies.slug','companies.is_active','companies.is_featured','companies.verified','companies.verification_token AS vt',
    'companies.remember_token AS rt','companies.created_at','companies.updated_at','companies.facebook','companies.twitter',
    'companies.linkedin','companies.google_plus','companies.pinterest','packages.package_title','companies.package_start_date',
    'companies.package_end_date','companies.jobs_quota','companies.availed_jobs_quota','companies.is_subscribed','companies.is_top_employer','companies.provider AS cp','companies.provider_id AS cpid')
    ->leftjoin('industries', 'companies.industry_id', '=', 'industries.id')
    ->leftjoin('ownership_types', 'companies.ownership_type_id', '=', 'ownership_types.id')
    ->leftjoin('countries', 'companies.country_id', '=', 'countries.id')
    ->leftjoin('states', 'companies.state_id', '=', 'states.id')
    ->leftjoin('cities', 'companies.city_id', '=', 'cities.id')
    ->leftjoin('packages', 'companies.package_id', '=', 'packages.id');

        return $query;
    }

    public function headings(): array
    {
        return [
            'ID','Name','Email','CEO','PIC Name','Country Code PIC Phone','PIC Phone','Industry',
            'Ownership Type','Description','No Of Offices','Website','No Of Employees',
            'Established in','Fax','Country Code Phone','Phone','Country','State',
            'City','Slug','Active','Featured','Verified','Verification Token',
            'Remember Token','Created At','Updated At','Facebook','Twitter',
            'Linkedin','Google Plus','Pinterest','Package','Package Start Date',
            'Package End Date','Jobs Quota','Available Jobs Quota','Subscribed','Top Employer','Provider','Provider ID'
        ];
    }


}
