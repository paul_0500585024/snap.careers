<style>
    .select2 {
        width:100%!important;
    }
    .select-single .select2-selection__rendered {
        line-height: 16px !important;
    }
    .select-single .select2-container .select2-selection--single {
        height: 20px !important;
    }
    .select-single .select2-selection__arrow {
        height: 19px !important;
    }
    #sidebar {
        min-width: 250px;
        max-width: 250px;
        background: #fefefe;
        color: #212529;
        transition: all 0.3s;
        padding: 5px;
        font-size:11px !important;
    }
    #sidebar .sidebar-header {
        padding: 5px;
        background: #6d7fcc;
    }
    #sidebar ul.components {
        padding: 5px 0;
        border-bottom: 1px solid #47748b;
    }
    #sidebar ul li.active>a, a[aria-expanded="true"] {
/*        color: #fff; */
/*        background: #6d7fcc; */
    }
    #sidebar ul li a {
        padding: 1px;
        font-size: 1.1em;
        display: block;
        font-weight: bold;
    }
    #sidebar ul li a {
        text-align: left;
        color: #212529;
    }
    #sidebar ul li ul{
        margin-left:10px;
    }

    #sidebar ul li ul li a{
        text-align: left;
        color: #212529;
        font-weight: normal;
    }
    a[data-toggle="collapse"] {
        position: relative;
    }
    a, a:hover, a:focus {
        color: inherit;
        text-decoration: none;
        transition: all 0.3s;
    }
    .dropdown-toggle::after {
        font-family: "FontAwesome";
        content: "\f13a";
        border-top: 0;
        vertical-align: 0;
    }
    .dropdown-toggle::after {
        position: absolute;
        top: 50%;
        right: 0;
        transform: translateY(-50%);
    }
    .label_chkbox {
        display: block;
        padding-left: 15px;
        text-indent: -15px;
    }
    .input_chkbox {
        width: 13px;
        height: 13px;
        padding: 0;
        margin:0;
        vertical-align: bottom;
        position: relative;
        top: -1px;
        *overflow: hidden;
    }
    .select_form{
        font-size:10px;
    }
    .select2-search input {
        background: transparent;
        font: 11px/11px "Lato", Liberation Sans, Arial, sans-serif;
        color: #34495e;
    }
</style>
<div class="col-12 d-xl-none">
    <div class="sidebar">
        <form action="{{route('job.list')}}" method="get">

            <!-- Search Criteria -->
            <div class="widget">
                <h4 class="widget-title">{{__('Search Criteria')}}</h4>
            </div>

        </form>
    </div>
</div>
<div class="col-md-3 col-sm-6 d-none d-xl-block" id="desktop-jobFilterSideBar">
    <!-- Side Bar start -->
    <div class="sidebar" id="sidebar">
        <ul class="list-unstyled components">
            <li>
                <a href="#countryofnationality" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">{{__('Country of Nationality')}}</a>
                <ul class="collapse list-unstyled" id="countryofnationality">
                    <li>
<?php /*                        <label class="label_chkbox">
                        <input class="input_chkbox" type="checkbox" name="country_id[]" id="country_{{$country_id}}" value="{{$country_id}}"
                               checked>
                        {{$country_name}}
                        </label> */ ?>
                        <div class="form-group select-single mb-1">
                            {!! Form::select('country_id', ['' => __('Select Country')]+$countries, Request::get('country_id',$country_id), array('class'=>'form-control select-multiple-country select_form mb-1', 'id'=>'country_id')) !!}
                        </div>
                        <div class="form-group select-single mb-1">
                            <span id="default_state_dd">
                                {!! Form::select('state_id', ['' => __('Select State')], Request::get('state_id', null), array('class'=>'form-control select-multiple-state select_form mb-1', 'id'=>'state_id')) !!}
                            </span>
                        </div>
                        <div class="form-group select-single mb-1">
                            <span id="default_cities_dd">
                                {!! Form::select('city_id', ['' => __('Select City')], Request::get('city_id', null), array('class'=>'form-control select-multiple-city select_form mb-1', 'id'=>'city_id')) !!}
                            </span>
                        </div>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#industry" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">{{__('Industry')}}</a>
                <ul class="collapse list-unstyled" id="industry">
                    @foreach($industryIdsArray as $key=>$industry_id)
                        @php
                            $industry = App\Industry::where('id','=',$industry_id)->lang()->active()->first();
                        @endphp
                        @if(null !== $industry)
                            @php
                                $checked = (in_array($industry->id, Request::get('industry_id', array())))? 'checked="checked"':'';
                            @endphp
                            <li>
                                <div clsss="d-flex">
                                    <label class="label_chkbox" for="industry_{{$industry->id}}">
                                        <input class="input_chkbox" type="checkbox" name="industry_id[]" id="industry_{{$industry->id}}" value="{{$industry->id}}" {{$checked}}>
                                        {{$industry->industry}}
                                        <span class="float-right">{{App\Job::countNumJobs('industry_id', $industry->id)}}</span>
                                    </label>
                                </div>
                            </li>
                        @endif
                    @endforeach
                </ul>
            </li>
            <li>
                <a href="#salary" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">{{__('Salary')}}</a>
                <ul class="collapse list-unstyled" id="salary">
                    <li>
                        <div class="form-group">
                            {!! Form::select('salary_currency', ['' =>__('Select Salary Currency')]+$currencies, Request::get('salary_currency', $currency_defoult), array('class'=>'form-control', 'id'=>'salary_currency')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::number('salary_from', Request::get('salary_from', null), array('class'=>'form-control', 'id'=>'salary_from', 'placeholder'=>__('Salary From'))) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::number('salary_to', Request::get('salary_to', null), array('class'=>'form-control', 'id'=>'salary_to', 'placeholder'=>__('Salary To'))) !!}
                        </div>
                        <!-- Salary end -->

                        <!-- button -->
                        <div class="searchnt">
                            <button type="submit" class="btn"><i class="fa fa-search" aria-hidden="true"></i> {{__('Search Jobs')}}</button>
                        </div>
                        <!-- button end-->
                    </li>
                </ul>
            </li>
            <li>
                <a href="#">Portfolio</a>
            </li>
            <li>
                <a href="#">Contact</a>
            </li>
        </ul>

        <ul class="list-unstyled CTAs">
            <li>
                <a href="https://bootstrapious.com/tutorial/files/sidebar.zip" class="download">Download source</a>
            </li>
            <li>
                <a href="https://bootstrapious.com/p/bootstrap-sidebar" class="article">Back to article</a>
            </li>
        </ul>

<?php /*        <div id="accordion">
            <div id="headingOne">
                <h5 class="mb-0 d-flex justify-content-between align-items-center">
                    <button class="btn btn-link btn-collapsible" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        {{__('Country of Nationality')}}
                    </button>
                    <i class="fa fa-arrow-down" aria-hidden="true" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"></i>
                </h5>
            </div>
            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                <input type="checkbox" name="country_id[]" id="country_{{$country_id}}" value="{{$country_id}}" checked>
                <label for="country_{{$country_id}}"></label>
                {{$country_name}}
            </div>

            <div id="headingTwo">
                <h5 class="mb-0 d-flex justify-content-between align-items-center">
                    <button class="btn btn-link btn-collapsible collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        {{__('Preferred Work Location')}}
                    </button>
                    <i class="fa fa-arrow-down" aria-hidden="true" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"></i>
                </h5>
            </div>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
            </div>

            <div id="headingThree">
                <h5 class="mb-0 d-flex justify-content-between align-items-center">
                    <button class="btn btn-link btn-collapsible collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        {{__('Industry')}}
                    </button>
                    <i class="fa fa-arrow-down" aria-hidden="true" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"></i>
                </h5>
            </div>
            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
            </div>

            <div id="headingFour">
                <h5 class="mb-0 d-flex justify-content-between align-items-center">
                    <button class="btn btn-link btn-collapsible collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                        {{__('Salary')}}
                    </button>
                    <i class="fa fa-arrow-down" aria-hidden="true" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"></i>
                </h5>
            </div>
            <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
            </div>

            <div id="headingFive">
                <h5 class="mb-0 d-flex justify-content-between align-items-center">
                    <button class="btn btn-link btn-collapsible collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                        {{__('Language')}}
                    </button>
                    <i class="fa fa-arrow-down" aria-hidden="true" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"></i>
                </h5>
            </div>
            <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
            </div>

            <div id="headingSix">
                <h5 class="mb-0 d-flex justify-content-between align-items-center">
                    <button class="btn btn-link btn-collapsible collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                        {{__('Years of Experience')}}
                    </button>
                    <i class="fa fa-arrow-down" aria-hidden="true" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"></i>
                </h5>
            </div>
            <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
            </div>

            <div id="headingSeven">
                <h5 class="mb-0 d-flex justify-content-between align-items-center">
                    <button class="btn btn-link btn-collapsible collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                        {{__('Specialization')}}
                    </button>
                    <i class="fa fa-arrow-down" aria-hidden="true" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"></i>
                </h5>
            </div>
            <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion">
                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
            </div>

            <div id="headingEight">
                <h5 class="mb-0 d-flex justify-content-between align-items-center">
                    <button class="btn btn-link btn-collapsible collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                        {{__('Education')}}
                    </button>
                    <i class="fa fa-arrow-down" aria-hidden="true" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"></i>
                </h5>
            </div>
            <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion">
                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
            </div>

            <div id="headingNine">
                <h5 class="mb-0 d-flex justify-content-between align-items-center">
                    <button class="btn btn-link btn-collapsible collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                        {{__('Education')}}
                    </button>
                    <i class="fa fa-arrow-down" aria-hidden="true" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"></i>
                </h5>
            </div>
            <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion">
                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
            </div>

            <div id="headingTen">
                <h5 class="mb-0 d-flex justify-content-between align-items-center">
                    <button class="btn btn-link btn-collapsible collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                        {{__('Field of study')}}
                    </button>
                    <i class="fa fa-arrow-down" aria-hidden="true" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"></i>
                </h5>
            </div>
            <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion">
                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
            </div>
*/?>
        </div>


        <!-- Location -->
<?php /*        <div class="widget">
            <h4 class="widget-title">{{__('Country of Nationality')}}</h4>
        </div>
        <div class="widget">
            <h4 class="widget-title">{{__('Preferred Work Location')}}<x/h4>
        </div>

        <!-- Search Criteria -->
        <div class="widget">
            <h4 class="widget-title">{{__('Industry')}}</h4>
        </div>
        <div class="widget">
            <h4 class="widget-title">{{__('Salary')}}</h4>
            <div class="form-group">
                {!! Form::number('salary_from', Request::get('salary_from', null), array('class'=>'form-control', 'id'=>'salary_from', 'placeholder'=>__('Salary From'))) !!}
            </div>
            <div class="form-group">
                {!! Form::number('salary_to', Request::get('salary_to', null), array('class'=>'form-control', 'id'=>'salary_to', 'placeholder'=>__('Salary To'))) !!}
            </div>
            <div class="form-group">
                {!! Form::select('salary_currency', ['' =>__('Select Salary Currency')]+$currencies, Request::get('salary_currency', $currency_defoult), array('class'=>'form-control', 'id'=>'salary_currency')) !!}
            </div>
            <!-- Salary end -->
        </div>
        <div class="widget">
            <h4 class="widget-title">{{__('Language')}}</h4>
        </div>
        <div class="widget">
            <h4 class="widget-title">{{__('Years of Experience')}}</h4>
        </div>
        <div class="widget">
            <h4 class="widget-title">{{__('Specialization')}}</h4>
        </div>
        <div class="widget">
            <h4 class="widget-title">{{__('Education')}}</h4>
        </div>
        <div class="widget">
            <h4 class="widget-title">{{__('Field of study')}}</h4>
        </div> */?>
    </div>
</div>
@push('scripts')
<script>
    $(document).ready(function ($) {
        $('#country_id').on('change', function (e) {
            e.preventDefault();
            filterStates(0);
        });
        $(document).on('change', '#state_id', function (e) {
            e.preventDefault();
            filterCities(0);
        });

        @php
            $state_id_array = Request::get('state_id', array(0 => 0));
        @endphp
        filterStates({{$state_id_array[0]}});
        $('.select-multiple-country').select2({
            placeholder: "{{__('Select Country')}}",
            dropdownCssClass: "select_form",
        });
        $('.select-multiple-state').select2({
            placeholder: "{{__('Select State')}}",
            dropdownCssClass: "select_form",
        });
        $('.select-multiple-city').select2({
            placeholder: "{{__('Select City')}}",
            dropdownCssClass: "select_form",
        });
    });
    function filterStates(state_id)
    {
        var country_id = $('#country_id').val();
        if (country_id != ''){
            $.post("{{ route('filter.states.dropdown') }}", {country_id: country_id, state_id: state_id, _method: 'POST', _token: '{{ csrf_token() }}'})
                .done(function (response) {
                    $('#default_state_dd').html(response);
                    @php
                        $city_id_array = Request::get('city_id', array(0 => 0));
                    @endphp
                    filterCities({{$city_id_array[0]}});
                    $('.select-multiple-state').select2({
                        placeholder: "{{__('Select State')}}",
                        dropdownCssClass: "select_form",
                        //            allowClear: true
                    });
                });
        }
    }

    function filterCities(city_id)
    {
        var state_id = $('#state_id').val();
        if (state_id != ''){
            $.post("{{ route('filter.cities.dropdown') }}", {state_id: state_id, city_id: city_id, _method: 'POST', _token: '{{ csrf_token() }}'})
                .done(function (response) {
                    $('#default_cities_dd').html(response);
                    $('.select-multiple-city').select2({
                        placeholder: "{{__('Select City')}}",
                        dropdownCssClass: "select_form",
//            allowClear: true
                    });
                });
        }
    }
</script>
@endpush