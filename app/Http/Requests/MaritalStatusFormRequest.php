<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class MaritalStatusFormRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
            case 'POST': {
                    $id = (int) $this->input('id', 0);
                    $marital_status_unique = '';
                    if ($id > 0) {
                        $marital_status_unique = ',id,' . $id;
                    }
            //lang
            $lang = config('rules.lang_rule');
            //marital_status
            $marital_status = config('rules.alnum30_rule');
            //is_active
            $is_active = config('rules.bool_rule');
            //major_subject_id
            $marital_status_id = array();
            $is_default = (int) $this->input('is_default', 0);
            if ($is_default == 0) {
                $marital_status_id = config('rules.id_rule');
            }
            //is_default
            $is_default = config('rules.bool_rule');

                    $id = array();
                    return [
                        "id" => $id,
                        "lang" => $lang,
                        "marital_status" => $marital_status,
                        "is_default" => $is_default,
                        "marital_status_id" => $marital_status_id,
                        "is_active" => $is_active,

//                        "id" => "",
//                        "lang" => "required|max:10",
//                        "marital_status" => "required|max:40",
//                        "is_default" => "required|boolean",
//                        "marital_status_id" => "required_if:is_default,0",
//                        "is_active" => "required|boolean",
                    ];
                }
            default:break;
        }
    }

    public function messages()
    {
        return [
//            'lang.required' => 'Please select language.', 'marital_status.required' => 'Please enter Marital Status.', 'is_default.required' => 'Is this Marital Status default/fallback ?.', 'marital_status_id.required_if' => 'Please select default/fallback Marital Status.', 'is_active.required' => 'Is this Marital Status active?',
        ];
    }

}
