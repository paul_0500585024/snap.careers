<div class="modal-body">
    <div class="form-body">
        <?php /*        <div class="formrow" id="div_job_skill_id">
            <label class="form-lable">Select skill</label>
            @php
            $job_skill_id = (isset($profileSkill) ? $profileSkill->job_skill_id : null);
            @endphp
            {!! Form::select('job_skill_id', [''=>__('Select skill')]+$jobSkills, $job_skill_id, array('class'=>'form-control', 'id'=>'job_skill_id')) !!} <span class="help-block job_skill_id-error"></span> </div>
        <div class="formrow" id="div_job_experience_id">
            <label class="form-lable">Select experience</label>
            @php
            $job_experience_id = (isset($profileSkill) ? $profileSkill->job_experience_id : null);
            @endphp
            {!! Form::select('job_experience_id', [''=>__('Select experience')]+$jobExperiences, $job_experience_id, array('class'=>'form-control', 'id'=>'job_experience_id')) !!} <span class="help-block job_experience_id-error"></span> </div> */ ?>
        <datalist id="listskill">
            @foreach($jobSkills as $jobskill)
                <option value="{{ $jobskill }}">
            @endforeach
        </datalist>
        <div class="formrow" id="div_name">
            <label class="form-lable">Project Name</label>
            <input class="form-control" id="name" placeholder="{{__('Project Name').' '.__('(required)')}}" name="name" type="text" value="{{(isset($profileProject)? $profileProject->name:'')}}">
            <span class="help-block name-error"></span>
        </div>
        @if (isset($user) && count($user->profileSkills))
            @foreach ($user->profileSkills as $key=>$skill)
                <div class="row mb-2 profileskills">
                    <div class="formrow col-sm-6" data-error="div_job_skill_id">
                        {!! Form::text('job_skill_id[]', $skill->getJobSkill('job_skill'), array('class'=>'form-control', 'list' => 'listskill')) !!}
                        <span class="help-block" data-error="job_skill_id"></span>
                    </div>
                    <div class="formrow col-sm-5 select-single" data-error="div_job_experience_id">
                        {!! Form::select('job_experience_id[]', [''=>__('Select experience')]+$jobExperiences, $skill->getJobExperience('job_experience'), array('class'=>'form-control select-job_experience_id')) !!}
                        <span class="help-block" data-error="job_experience_id"></span>
                    </div>
                    <div class="col-sm-1">
                        <a id="{{ $skill->id }}" class="text-danger delete_user_skill" style="cursor:pointer" title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
</div>

<div class="form-group">
    <div class="col-sm-12">
        <div class="row">
            <a class="fnt-theme" id="add_skill" onclick="javascript:addSkill();"><i class="fa fa-plus-circle" id="icon_plus_sign"></i>&nbsp;<span class="add-skill-label" id="add">Add Skill</span></a>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('.select-job_experience_id').select2({
        placeholder: "{{__('Select Job Experience').' '.__('(required)')}}",
//            allowClear: true
    });
    $(document).ready(function(){
        $('a.delete_user_skill').on('click', function(e){
            e.preventDefault();
            var $this = $(this);
            var id = $this.prop('id');
            var q = confirm("Are you sure want to disable all fields?");
            if(q == true) {
                $.post("{{ route('delete.front.profile.skill') }}", {id: id, _method: 'DELETE', _token: '{{ csrf_token() }}'})
                    .done(function (response) {
                        if (response == 'ok')
                        {
                            $('#skill_' + id).remove();
                            $this.parent().parent()
                                .fadeOut(300, function(){$(this).remove();});
                        } else
                        {
                            alert('Request Failed!');
                        }
                    });
            }
            return false;
        });
    });

    function deleteUserSkillInput(element) {
        var $this = element;
        $this.parent().parent()
            .fadeOut(300, function () {
                $(this).remove();
            });
        return false;
    }

    function addSkill(){
        var html = '<div class="row mb-2 profileskills">';
        html += '<div class="formrow col-sm-6" data-error="div_job_skill_id">';
        html += '{!! Form::text("job_skill_id[]", null, array("class"=>"form-control", "list" => "listskill")) !!}';
        html += '<span class="help-block" data-error="job_skill_id"></span>';
        html += '</div>';
        html += '<div class="col-sm-5 select-single" data-error="div_job_experience_id">';
        html += '{!! Form::select("job_experience_id[]", [""=>__("Select experience")]+$jobExperiences, null, array("class"=>"form-control select-job_experience_id")) !!}';
        html += '<span class="help-block" data-error="job_experience_id"></span>';
        html += '</div>';
        html += '<div class="col-sm-1">';
        html += '<a class="text-danger delete_user_skill" style="cursor:pointer" title="Delete" onclick="deleteUserSkillInput($(this))"><i class="fa fa-trash" aria-hidden="true"></i></a>';
        html += '</div>';
        html += '</div>';
        $('.profileskills:last').after(html);
        $('.select-job_experience_id').select2({
            placeholder: "{{__('Select Job Experience').' '.__('(required)')}}",
//            allowClear: true
        });
    }

</script>