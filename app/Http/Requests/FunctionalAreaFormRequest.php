<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class FunctionalAreaFormRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
            case 'POST': {
                    $id = (int) $this->input('id', 0);
                    $functional_area_unique = '';
                    if ($id > 0) {
                        $functional_area_unique = ',id,' . $id;
                    }
                    //functional_area
                    $functional_area = config('rules.alnum100_rule');
                    array_push($functional_area,
                        'unique:functional_areas' . $functional_area_unique
                    );
                    //functional_area_id
                    $functional_area_id = array();
                    $is_default = (int) $this->input('is_default', 0);
                    if ($is_default == 0) {
                        $functional_area_id = config('rules.id_rule');
                    }
                    //is_active
                    $is_active = config('rules.bool_rule');
                    //is_default
                    $is_default = config('rules.bool_rule');
                    //lang
                    $lang = config('rules.lang_rule');

                    return [
                        'functional_area' => $functional_area,
                        'functional_area_id' => $functional_area_id,
                        'is_active' => $is_active,
                        'is_default' => $is_default,
                        'lang' => $lang,

//                        'functional_area' => 'required|max:200|unique:functional_areas' . $functional_area_unique,
//                        'functional_area_id' => 'required_if:is_default,0',
//                        'is_active' => 'required|boolean',
//                        'is_default' => 'required|boolean',
//                        'lang' => 'required|max:10',
                    ];
                }
            default:break;
        }
    }

    public function messages()
    {
        return [
/*            'functional_area.required' => 'Please enter Functional Area.',
            'functional_area_id.required_if' => 'Please select default/fallback Functional Area.',
            'is_default.required' => 'Is this Functional Area default?',
            'is_active.required' => 'Please select status.',
            'lang.required' => 'Please select language.',*/
        ];
    }

}
