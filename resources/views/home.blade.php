@extends('layouts.app')
@section('content') 
<!-- Header start --> 
@include('includes.header') 
<!-- Header end --> 
<!-- Inner Page Title start --> 
@include('includes.inner_page_title', ['page_title'=>__('Dashboard')]) 
<!-- Inner Page Title end -->
<div class="listpgWraper">
    <div class="container">
        <div class="row d-flex justify-content-end">
            <div class="switchbox">
                @php
                    if(isset($active)){
                    }else{
                        $active='';
                    }
                @endphp
                <div class="txtlbl">{{__('Immediate Available')}} <i class="fa fa-info-circle" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="{{__('Are you immediate available')}}?" data-original-title="{{__('Are you immediate available')}}?" title="{{__('Are you immediate available')}}?"></i>
                </div>
                <div class="pull-right">
                    <label class="switch switch-green"> @php
                            $checked = ((bool)Auth::user()->is_immediate_available)? 'checked="checked"':'';
                        @endphp
                        <input type="checkbox" name="is_immediate_available" id="is_immediate_available" class="switch-input" {{$checked}} onchange="changeImmediateAvailableStatus({{Auth::user()->id}}, {{Auth::user()->is_immediate_available}});">
                        <span class="switch-label" data-on="Yes" data-off="No"></span> <span class="switch-handle"></span> </label>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="container-fluid">@include('flash::message')
        <div class="row">
            @include('includes.user_dashboard_menu')
            <div class="col-xl-8 col-sm-12 col-12">
                @include('includes.user_dashboard_stats')
                @if((bool)config('jobseeker.is_jobseeker_package_active'))
                @php        
                $packages = App\Package::where('package_for', 'like', 'job_seeker')->get();
                $package = Auth::user()->getPackage();
                if(null !== $package){
                $packages = App\Package::where('package_for', 'like', 'job_seeker')->where('id', '<>', $package->id)->where('package_price', '>=', $package->package_price)->get();
                }
                @endphp

                @if(null !== $package)
                @include('includes.user_package_msg')
                @include('includes.user_packages_upgrade')
                @else

                @if(null !== $packages)
                @include('includes.user_packages_new')
                @endif
                @endif
                @endif
            </div>
            @include('includes.user_dashboard_menu')
        </div>
    </div>
</div>
@include('includes.footer')
@endsection
@push('scripts')
@include('includes.immediate_available_btn')
@endpush