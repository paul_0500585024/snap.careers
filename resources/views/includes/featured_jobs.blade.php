<!-- Featured Grid -->
<section class="bg-featured page-section" id="portfolio">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading text-light">Featured Jobs</h2>
            </div>
        </div>
        <div class="row">
            @if(isset($featuredJobs) && count($featuredJobs))
                @foreach($featuredJobs as $featuredJob)
            <div class="col-md-6 col-sm-6 portfolio-item">
                <a class="portfolio-link" href="{{route('job.detail', [$featuredJob->slug])}}">
                    <div class="portfolio-hover"></div>
                    <div class="portfolio-caption text-left">
                        <div class="row">
                            <div class="col-md-6">
                                <span class="text-uppercase text-muted"><i class="fa fa-briefcase"></i> Jobs</span>
                            </div>
                            <div class="col-md-6">
                                <div class="view-circle pull-right"></div>
                            </div>
                        </div>
                        <h4 class="fnt-theme">{{$featuredJob->getJobType('job_type')}} : {{$featuredJob->title}}</h4>
<?php /*                        <h4 style="color:#0062cc">{{$featuredJob->getJobType('job_type')}} : {{$featuredJob->title}}</h4> */?>
                        <p class="text-muted">{{$featuredJob->getCity('city')}}</p>
                    </div>
                </a>
            </div>
                @endforeach
            @endif

        </div>
    </div>
</section>
