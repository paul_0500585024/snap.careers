<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

class AdminFormErrorHelper
{

    public static function hasError($errors, $field)
    {
        return ($errors->first($field) != '') ? 'has-error msg_cls_for_focus' : '';
    }

    public static function hasErrorArray($errors, $field)
    {
        $class = '';
        foreach($errors->getMessages() as $field_name => $error_message) {
            if (Str::startsWith($field_name, $field)) {
                $class = 'has-error msg_cls_for_focus';
            }
        }
        return $class;

//        return ($errors->first($field) != '') ? 'has-error msg_cls_for_focus' : '';
    }

    public static function showErrors($errors, $field)
    {
        $html = '';
        if ($errors->first($field) != ''):
            foreach ($errors->get($field) as $message):
                $html .= '<span id="name-error" class="help-block help-block-error">' . $message . '</span>';
            endforeach;
        endif;
        return $html;
    }

    public static function showErrorsNotice($errors)
    {
        $html = '';
        if (count($errors) > 0) {
            $html .= '<div class="alert alert-danger">You have some form errors. Please check below.<ul>';
            foreach ($errors->all() as $message):
                $html .= '<li><span id="name-error" class="help-block help-block-error">' . $message . '</span></li>';
            endforeach;
            $html .= '</ul></div>';
        }
        return $html;
    }

    public static function showOnlyErrorsNotice($errors)
    {
        $html = '';
        if (count($errors) > 0) {
            $html = '<div class="alert alert-danger">You have some form errors. Please check below.</div>';
        }
        return $html;
    }

    public static function showErrorArray($errors, $field)
    {
        $html = '';
        foreach($errors->getMessages() as $field_name => $error_message){
            if(Str::startsWith($field_name, $field.'.')){
                foreach($error_message as $message){
                    $html .= '<span id="name-error" class="help-block help-block-error">' . $message . '</span>';
                }
            }
        }
        /*        if($errors->has('*')){
                    $count = 0;
                    foreach ($errors->all() as $error){
                        if($errors->first($field.'.' . $count) != ''){
                            $html .= '<span id="name-error" class="help-block help-block-error">' . $error . '</span>';
                        }
                        $count++;
                    }
                }*/
        return $html;

    }

}
