<form class="form" id="add_edit_profile_project" method="PUT" action="{{ route('update.front.profile.project', [$profileProject->id, $user->id]) }}">{{ csrf_field() }}
	@include('user.forms.project.project_form_view')
	<div class="modal-footer">
		<button type="button" class="btn btn-large btn-theme" data-dismiss="modal">Close</button>
	</div>
</form>