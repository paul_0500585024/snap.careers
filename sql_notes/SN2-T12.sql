﻿DELETE FROM job_experiences WHERE id <=51;

INSERT  INTO `job_experiences`(`job_experience_id`,`job_experience`,`is_default`,`is_active`,`sort_order`,`lang`,`created_at`,`updated_at`) 
VALUES 
(1, '< 2 Years', 1, 1, 1, 'en', NOW(), NOW()),
(2, '2 - 5 Years', 1, 1, 2, 'en', NOW(), NOW()),
(3, '5 - 10 Years', 1, 1, 3, 'en', NOW(), NOW()),
(4, '> 10 Years', 1, 1, 4, 'en', NOW(), NOW()),
(1, '< 2 años', 1, 1, 1, 'es', NOW(), NOW()),
(2, '2 - 5 años', 1, 1, 2, 'es', NOW(), NOW()),
(3, '5 - 10 años', 1, 1, 3, 'es', NOW(), NOW()),
(4, '> 10 años', 1, 1, 4, 'es', NOW(), NOW()),
(1, '< 2 سنة', 1, 1, 1, 'ar', NOW(), NOW()),
(2, '2 - 5 سنوات', 1, 1, 2, 'ar', NOW(), NOW()),
(3, '5 - 10 سنوات', 1, 1, 3, 'ar', NOW(), NOW()),
(4, '> 10 سنوات', 1, 1, 4, 'ar', NOW(), NOW()),
(1, '< 2 سال', 1, 1, 1, 'ur', NOW(), NOW()),
(2, '2 - 5 سال', 1, 1, 2, 'ur', NOW(), NOW()),
(3, '5 - 10 سال', 1, 1, 3, 'ur', NOW(), NOW()),
(4, '> 10 سال', 1, 1, 4, 'ur', NOW(), NOW())