﻿DELETE FROM career_levels;

INSERT INTO `career_levels`(`career_level_id`, `career_level`, is_default, is_active, sort_order, lang, created_at, updated_at) VALUES
(1, 'Internship', 1, 1, 1, 'en', NOW(), NULL),
(2, 'Administration', 1, 1, 2, 'en', NOW(), NULL),
(3, 'Staff', 1, 1, 3, 'en', NOW(), NULL),
(4, 'Supervisor', 1, 1, 4, 'en', NOW(), NULL),
(5, 'Manager', 1, 1, 5, 'en', NOW(), NULL),
(6, 'General Manager', 1, 1, 6, 'en', NOW(), NULL),
(7, 'Director', 1, 1, 7, 'en', NOW(), NULL)

INSERT INTO `career_levels`(`career_level_id`, `career_level`, is_default, is_active, sort_order, lang, created_at, updated_at) VALUES
(1, 'انٹرنشپ', 0, 1, 1, 'ur', NOW(), NULL),
(2, 'انتظامیہ', 0, 1, 2, 'ur', NOW(), NULL),
(3, 'اسٹاف', 0, 1, 3, 'ur', NOW(), NULL),
(4, 'سپروائزر', 0, 1, 4, 'ur', NOW(), NULL),
(5, 'مینیجر', 0, 1, 5, 'ur', NOW(), NULL),
(6, 'جنرل مینیجر', 0, 1, 6, 'ur', NOW(), NULL),
(7, 'ڈائریکٹر', 0, 1, 7, 'ur', NOW(), NULL)

INSERT INTO `career_levels`(`career_level_id`, `career_level`, is_default, is_active, sort_order, lang, created_at, updated_at) VALUES
(1, 'Internado', 0, 1, 1, 'es', NOW(), NULL),
(2, 'Administración', 0, 1, 2, 'es', NOW(), NULL),
(3, 'Personal', 0, 1, 3, 'es', NOW(), NULL),
(4, 'Supervisor', 0, 1, 4, 'es', NOW(), NULL),
(5, 'Gerente', 0, 1, 5, 'es', NOW(), NULL),
(6, 'Gerente general', 0, 1, 6, 'es', NOW(), NULL),
(7, 'Director', 0, 1, 7, 'es', NOW(), NULL)

INSERT INTO `career_levels`(`career_level_id`, `career_level`, is_default, is_active, sort_order, lang, created_at, updated_at) VALUES
(1, 'فترة تدريب', 0, 1, 1, 'ar', NOW(), NULL),
(2, 'الادارة', 0, 1, 2, 'ar', NOW(), NULL),
(3, 'العاملين', 0, 1, 3, 'ar', NOW(), NULL),
(4, 'مشرف', 0, 1, 4, 'ar', NOW(), NULL),
(5, 'مدير', 0, 1, 5, 'ar', NOW(), NULL),
(6, 'مدير عام', 0, 1, 6, 'ar', NOW(), NULL),
(7, 'مدير', 0, 1, 7, 'ar', NOW(), NULL)
