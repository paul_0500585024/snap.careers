<!-- Story -->
<section class="page-section pt-0" id="story">
    <div class="container">
        <div class="col-lg-12 text-center">
            <h2 class="section-heading text-dark pb-4">{{__('Success Stories')}}</h2>
        </div>
        <div id="demo" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
                <li data-target="#demo" data-slide-to="0" class="active"></li>
            </ul>
            <!-- The slideshow -->
            <div class="carousel-inner">
                <div class="carousel-item active">
                    @if(isset($testimonials) && count($testimonials))
                        @foreach($testimonials as $testimonial)
                    <div class="col-xs-12 col-sm-4 col-md-4">
                        <p class="testi">{{$testimonial->testimonial}}</p>
                        <div class="text-center pt-3">
<?php /*                            <img src="img/testi1.png"> */ ?>
                            <h6 class="pt-3 pb-0">{{$testimonial->testimonial_by}}</h6>
                            <p class="text-muted">{{$testimonial->company}}</p>
                        </div>
                    </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
