<style>
    .select-single .select2-selection__rendered {
        line-height: 46px !important;
    }
    .select-single .select2-container .select2-selection--single {
        height: 50px !important;
    }
    .select-single .select2-selection__arrow {
        height: 49px !important;
    }
    .select2-container .select2-selection--single{
        background-color: transparent;
    }
    .select2-container--default .select2-selection--single .select2-selection__placeholder,
    .select2-container--default .select2-selection--single .select2-selection__rendered{
        color: #fefefe;
        font-weight:700;
    }
    .select2-container--default .select2-selection--single {
        border:0;
    }
    .select2-container--default .select2-selection--single .select2-selection__arrow b{
        border-color: #fefefe transparent;
    }
</style>

<!-- Services -->
<section class="page-section grad" id="services">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="section-heading">
                    <!-- Default inline form -->
@if(Auth::guard('company')->check())
<form action="{{route('job.seeker.list')}}">
    <div class="row">
        <div class="container text-center">
            <div class="radius-border row">
                <div class="col-md-{{((bool)$siteSetting->country_specific_site)? 6:3}}">
                    {!! Form::select('functional_area_id[]', ['' => __('Select Functional Area')]+$functionalAreas, Request::get('functional_area_id', null), array('class'=>'form-control select-multiple-functional-area', 'id'=>'functional_area_id')) !!}
                </div>

                @if((bool)$siteSetting->country_specific_site)
                    {!! Form::hidden('country_id[]', Request::get('country_id[]', $country_id), array('id'=>'country_id')) !!}
                @else
                    <div class="col-md-3">
                        {!! Form::select('country_id[]', ['' => __('Select Country')]+$countries, Request::get('country_id', $country_id), array('class'=>'form-control select-multiple-country', 'id'=>'country_id')) !!}
                    </div>
                @endif

                <div class="col-md-3">
                    <span id="default_state_dd">
                        {!! Form::select('state_id[]', ['' => __('Select State')], Request::get('state_id', null), array('class'=>'form-control select-multiple-state', 'id'=>'state_id')) !!}
                    </span>
                </div>
                <div class="col-md-3">
                    <span id="default_cities_dd">
                        {!! Form::select('city_id[]', ['' => __('Select City')], Request::get('city_id', null), array('class'=>'form-control select-multiple-city', 'id'=>'city_id')) !!}
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="container text-center">
            <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                <input type="text" name="search" id="jbsearch" aria-label="Search" placeholder="{{__('Search by Job title, Skills, Industries')}}" class="form-control input-search">
                <?php /*                <div class="btn-group" role="group">
                    <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Petaling Jaya
                    </button>
                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                        <a class="dropdown-item" href="#">Dropdown link</a>
                        <a class="dropdown-item" href="#">Dropdown link</a>
                    </div>
                </div> */ ?>
                <button type="submit" class="btn btn-search-blue"><i class="fa fa-search"></i></button>
            </div>
        </div>
    </div>
</form>
@else
<form action="{{route('job.list')}}" method="get">
    <div class="row">
        <div class="container text-center">
            <div class="radius-border row">
                <div class="col-md-{{((bool)$siteSetting->country_specific_site)? 6:3}}">
                    {!! Form::select('functional_area_id[]', ['' => __('Select Functional Area')]+$functionalAreas, Request::get('functional_area_id', null), array('class'=>'form-control select-multiple-functional-area', 'id'=>'functional_area_id')) !!}
                </div>

                @if((bool)$siteSetting->country_specific_site)
                    {!! Form::hidden('country_id[]', Request::get('country_id[]', $country_id), array('id'=>'country_id')) !!}
                @else
                    <div class="col-md-3">
                        {!! Form::select('country_id[]', ['' => __('Select Country')]+$countries, Request::get('country_id', $country_id), array('class'=>'form-control select-multiple-country', 'id'=>'country_id')) !!}
                    </div>
                @endif

                <div class="col-md-3">
                    <span id="default_state_dd">
                        {!! Form::select('state_id[]', ['' => __('Select State')], Request::get('state_id', null), array('class'=>'form-control select-multiple-state', 'id'=>'state_id')) !!}
                    </span>
                </div>
                <div class="col-md-3">
                    <span id="default_cities_dd">
                        {!! Form::select('city_id[]', ['' => __('Select City')], Request::get('city_id', null), array('class'=>'form-control select-multiple-city', 'id'=>'city_id')) !!}
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="container text-center">
            <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                <input type="text" name="search" id="jbsearch" aria-label="Search" placeholder="{{__('Search by Job title, Skills, Industries')}}" class="form-control input-search">
<?php /*                <div class="btn-group" role="group">
                    <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Petaling Jaya
                    </button>
                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                        <a class="dropdown-item" href="#">Dropdown link</a>
                        <a class="dropdown-item" href="#">Dropdown link</a>
                    </div>
                </div> */ ?>
                <button type="submit" class="btn btn-search-blue"><i class="fa fa-search"></i></button>
            </div>
        </div>
    </div>

</form>
@endif
                <!-- Default inline form -->
                </div>
            </div>
        </div>
    </div>
</section>
