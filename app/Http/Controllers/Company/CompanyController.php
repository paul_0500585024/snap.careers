<?php

namespace App\Http\Controllers\Company;

use Mail;
use Hash;
use File;
use ImgUploader;
use Auth;
use Validator;
use DB;
use Input;
use Redirect;
use App\Subscription;
use Newsletter;
use App\User;
use App\Company;
use App\CompanyMessage;
use App\ApplicantMessage;
use App\Country;
use App\CountryDetail;
use App\State;
use App\City;
use App\Industry;
use App\FavouriteCompany;
use App\FavouriteApplicant;
use App\OwnershipType;
use App\JobApply;
use Carbon\Carbon;
use App\Helpers\MiscHelper;
use App\Helpers\DataArrayHelper;
use App\Http\Requests;
use App\Mail\CompanyContactMail;
use App\Mail\ApplicantContactMail;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Requests\Front\CompanyFrontFormRequest;
use App\Http\Controllers\Controller;
use App\Traits\CompanyTrait;
use App\Traits\Cron;
use App\Traits\FetchJobSeekers;

class CompanyController extends Controller
{

    use CompanyTrait;
    use Cron;
    use FetchJobSeekers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('company', ['except' => ['companyDetail', 'sendContactForm']]);
        $this->middleware(['company', 'isCompanyVerified'], ['except' => ['companyDetail', 'sendContactForm','showContactCompanies']]);
        $this->runCheckPackageValidity();
    }

    public function index()
    {
        return view('company_home')
         ->with('active', 'index');
    }
    public function company_listing()
    {
        $data['companies']=Company::paginate(20);
        return view('company.listing')->with($data);
    }

    public function companyProfile()
    {
        $countries = DataArrayHelper::langCountriesArray();
        $industries = DataArrayHelper::langIndustriesArray();
        $ownershipTypes = DataArrayHelper::langOwnershipTypesArray();
        $countryPhones = DataArrayHelper::CountryDetailTelephone();
        $company = Company::findOrFail(Auth::guard('company')->user()->id);

        if(isset($company) && count($company->countryDetailPhone)){
            foreach($company->countryDetailPhone as $country_detail){
                $company->country_phone_phone =  $country_detail->id;
            }
        }

        if(isset($company) && count($company->countryDetailPICPhone)){
            foreach($company->countryDetailPICPhone as $country_detail){
                $company->country_phone_pic_phone =  $country_detail->id;
            }
        }

        if(isset($company->phone)){
            $text = $company->phone;
            $arr = preg_split('/\(.*?\)/',$text);
            $company->phone = isset($arr[1])?$arr[1]:null;
        }
        if(isset($company->pic_phone)){
            $text = $company->pic_phone;
            $arr = preg_split('/\(.*?\)/',$text);
            $company->pic_phone = isset($arr[1])?$arr[1]:null;
        }


        return view('company.edit_profile')
                        ->with('company', $company)
                        ->with('countries', $countries)
                        ->with('industries', $industries)
                        ->with('ownershipTypes', $ownershipTypes)
                        ->with('countryPhones', $countryPhones)
                        ->with('active', 'companyProfile');
    }

    public function companyCandidate(Request $request)
    {
        $countries = DataArrayHelper::langCountriesArray();
        $industries = DataArrayHelper::langIndustriesArray();
        $ownershipTypes = DataArrayHelper::langOwnershipTypesArray();
        $countryPhones = DataArrayHelper::CountryDetailTelephone();
        $company = Company::findOrFail(Auth::guard('company')->user()->id);

        $country_ids = $request->query('country_id', array());
        $ip=geoip()->getClientIP();
        $location=geoip($ip);
        if(empty($country_ids)){
            $country_code=$location->iso_code;
            $country_ids[0]="".Country::countryID($country_code)."";
            $country_id=Country::countryID($country_code);
            $country_name=Country::countryName($country_code);

        }else{
            $country_id=null;
            $country_name = null;
        }
        $currency_defoult=$location->currency;

        $search = $request->query('search', '');
        $functional_area_ids = $request->query('functional_area_id', array());
        $country_ids = $request->query('country_id', array());
        $state_ids = $request->query('state_id', array());
        $city_ids = $request->query('city_id', array());
        $career_level_ids = $request->query('career_level_id', array());
        $gender_ids = $request->query('gender_id', array());
        $industry_ids = $request->query('industry_ids', array());
        $job_experience_ids = $request->query('job_experience_id', array());
        $current_salary = $request->query('current_salary', '');
        $expected_salary = $request->query('expected_salary', '');
        $salary_currency = $request->query('salary_currency', '');
        $order_by = $request->query('order_by', 'id');
        $limit = 10;

        $jobSeekers = $this->fetchJobSeekers($search, $industry_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $career_level_ids, $gender_ids, $job_experience_ids, $current_salary, $expected_salary, $salary_currency, $order_by, $limit);


        /*         * ************************************************** */

        $jobSeekerIdsArray = $this->fetchIdsArray($search, $industry_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $career_level_ids, $gender_ids, $job_experience_ids, $current_salary, $expected_salary, $salary_currency, 'users.id');

        /*         * ************************************************** */

        $skillIdsArray = $this->fetchSkillIdsArray($jobSeekerIdsArray);

        /*         * ************************************************** */

        $countryIdsArray = $this->fetchIdsArray($search, $industry_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $career_level_ids, $gender_ids, $job_experience_ids, $current_salary, $expected_salary, $salary_currency, 'users.country_id');

        /*         * ************************************************** */

        $stateIdsArray = $this->fetchIdsArray($search, $industry_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $career_level_ids, $gender_ids, $job_experience_ids, $current_salary, $expected_salary, $salary_currency, 'users.state_id');

        /*         * ************************************************** */

        $cityIdsArray = $this->fetchIdsArray($search, $industry_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $career_level_ids, $gender_ids, $job_experience_ids, $current_salary, $expected_salary, $salary_currency, 'users.city_id');

        /*         * ************************************************** */

        $industryIdsArray = $this->fetchIdsArray($search, $industry_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $career_level_ids, $gender_ids, $job_experience_ids, $current_salary, $expected_salary, $salary_currency, 'users.industry_id');

        /*         * ************************************************** */


        /*         * ************************************************** */

        $functionalAreaIdsArray = $this->fetchIdsArray($search, $industry_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $career_level_ids, $gender_ids, $job_experience_ids, $current_salary, $expected_salary, $salary_currency, 'users.functional_area_id');

        /*         * ************************************************** */

        $careerLevelIdsArray = $this->fetchIdsArray($search, $industry_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $career_level_ids, $gender_ids, $job_experience_ids, $current_salary, $expected_salary, $salary_currency, 'users.career_level_id');

        /*         * ************************************************** */

        $genderIdsArray = $this->fetchIdsArray($search, $industry_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $career_level_ids, $gender_ids, $job_experience_ids, $current_salary, $expected_salary, $salary_currency, 'users.gender_id');

        /*         * ************************************************** */

        $jobExperienceIdsArray = $this->fetchIdsArray($search, $industry_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $career_level_ids, $gender_ids, $job_experience_ids, $current_salary, $expected_salary, $salary_currency, 'users.job_experience_id');

        /*         * ************************************************** */

        $seoArray = $this->getSEO($functional_area_ids, $country_ids, $state_ids, $city_ids, $career_level_ids, $gender_ids, $job_experience_ids);

        /*         * ************************************************** */

        $currencies = DataArrayHelper::currenciesArray();

        /*         * ************************************************** */




        if(isset($company) && count($company->countryDetailPhone)){
            foreach($company->countryDetailPhone as $country_detail){
                $company->country_phone_phone =  $country_detail->id;
            }
        }

        if(isset($company) && count($company->countryDetailPICPhone)){
            foreach($company->countryDetailPICPhone as $country_detail){
                $company->country_phone_pic_phone =  $country_detail->id;
            }
        }

        if(isset($company->phone)){
            $text = $company->phone;
            $arr = preg_split('/\(.*?\)/',$text);
            $company->phone = isset($arr[1])?$arr[1]:null;
        }
        if(isset($company->pic_phone)){
            $text = $company->pic_phone;
            $arr = preg_split('/\(.*?\)/',$text);
            $company->pic_phone = isset($arr[1])?$arr[1]:null;
        }

/*        $salary_from = $request->query('salary_from', '');
        $salary_to = $request->query('salary_to', '');
        $salary_currency = $request->query('salary_currency', '');*/


        $seo = (object) array(
            'seo_title' => $seoArray['title'],
            'seo_description' => $seoArray['description'],
            'seo_keywords' => $seoArray['keywords'],
            'seo_other' => ''
        );

        $this->functionalAreas = DataArrayHelper::langFunctionalAreasArray();
        return view('company.candidate')
            ->with('company', $company)
            ->with('countries', $countries)
            ->with('industries', $industries)
            ->with('ownershipTypes', $ownershipTypes)
            ->with('countryPhones', $countryPhones)
            ->with('active', 'companyCandidate')
            ->with('functionalAreas', $this->functionalAreas)
            ->with('currencies', array_unique($currencies))
            ->with('jobSeekers', $jobSeekers)
            ->with('skillIdsArray', $skillIdsArray)
            ->with('countryIdsArray', $countryIdsArray)
            ->with('stateIdsArray', $stateIdsArray)
            ->with('cityIdsArray', $cityIdsArray)
            ->with('industryIdsArray', $industryIdsArray)
            ->with('functionalAreaIdsArray', $functionalAreaIdsArray)
            ->with('careerLevelIdsArray', $careerLevelIdsArray)
            ->with('genderIdsArray', $genderIdsArray)
            ->with('jobExperienceIdsArray', $jobExperienceIdsArray)
            ->with('currency_defoult',$currency_defoult)
            ->with('country_id',$country_id)
            ->with('country_name',$country_name)
            ->with('seo', $seo);
    }


    public function showCompanyChangePassword()
    {
        $company = Company::findOrFail(Auth::guard('company')->user()->id);
        return view('company.changepassword')
            ->with('company', $company)
            ->with('active', 'companyChangePassword');
    }


    public function companyChangePassword(Request $request)
    {

        if (!(Hash::check($request->get('current-password'), Auth::guard('company')->user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }
        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }
        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|confirmed|min:6|max:50',
        ]);
        //Change Password
        $user = Auth::guard('company')->user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();
        return redirect()->back()->with("success","Password changed successfully !");
    }

    public function updateCompanyProfile(CompanyFrontFormRequest $request)
    {
        $company = Company::findOrFail(Auth::guard('company')->user()->id);
        /*         * **************************************** */
        if ($request->hasFile('logo')) {
            $is_deleted = $this->deleteCompanyLogo($company->id);
            $image = $request->file('logo');
            $fileName = ImgUploader::UploadImage('company_logos', $image, md5($request->input('name')), 300, 300, false);
            $company->logo = $fileName;
        }
        /*         * ************************************** */
        $company->name = $request->input('name');
        $company->email = $request->input('email');
/*        if (!empty($request->input('password'))) {
            $company->password = Hash::make($request->input('password'));
        }*/
        $company->ceo = $request->input('ceo');
        $company->pic_name = $request->input('pic_name');

        $company->country_phone_pic_phone = $request->input('country_phone_pic_phone');
        $country_detail_obj = CountryDetail::where('country_id','=',$company->country_phone_pic_phone)->first();
        $country_phone_pic_phone_code = $country_detail_obj->phone_code;
        $company->pic_phone = '+'.'('.$country_phone_pic_phone_code.')'.$request->input('pic_phone');

        $company->industry_id = $request->input('industry_id');
        $company->ownership_type_id = $request->input('ownership_type_id');
        $company->description = $request->input('description');
//        $company->location = $request->input('location');
//        $company->map = $request->input('map');
        $company->no_of_offices = $request->input('no_of_offices');
        $website = $request->input('website');
        $company->website = (false === strpos($website, 'http')) ? 'http://' . $website : $website;
        $company->no_of_employees = $request->input('no_of_employees');
        $company->established_in = $request->input('established_in');
        $company->fax = $request->input('fax');

        $company->country_phone_phone = $request->input('country_phone_phone');
        $country_detail_obj = CountryDetail::where('country_id','=',$company->country_phone_phone)->first();
        $country_phone_phone_code = $country_detail_obj->phone_code;
        $company->phone = '+'.'('.$country_phone_phone_code.')'.$request->input('phone');

        $company->facebook = $request->input('facebook');
        $company->twitter = $request->input('twitter');
        $company->linkedin = $request->input('linkedin');
        $company->google_plus = $request->input('google_plus');
        $company->pinterest = $request->input('pinterest');
        $company->country_id = $request->input('country_id');
//        $company->longitude = $request->input('longitude');
//        $company->latitude = $request->input('latitude');
        $company->state_id = $request->input('state_id');
        $company->city_id = $request->input('city_id');

		$company->is_subscribed = $request->input('is_subscribed', 0);
		
        $company->slug = str_slug($company->name, '-') . '-' . $company->id;
        $company->update();
		/*************************/
		Subscription::where('email', 'like', $company->email)->delete();
		if((bool)$company->is_subscribed)
		{			
			$subscription = new Subscription();
			$subscription->email = $company->email;
			$subscription->name = $company->name;
			$subscription->save();
			/*************************/
			Newsletter::subscribeOrUpdate($subscription->email, ['FNAME'=>$subscription->name]);
			/*************************/
		}
		else
		{
			/*************************/
			Newsletter::unsubscribe($company->email);
			/*************************/
		}

        $company_slug = Auth::guard('company')->user()->slug;

        flash(__('Company has been updated'))->success();
//        return \Redirect::route('company.profile');
        return \Redirect::route('company.detail',$company_slug);
    }

    public function addToFavouriteApplicant(Request $request, $application_id, $user_id, $job_id, $company_id)
    {
        $data['user_id'] = $user_id;
        $data['job_id'] = $job_id;
        $data['company_id'] = $company_id;

        $data_save = FavouriteApplicant::create($data);
        flash(__('Job seeker has been added in favorites list'))->success();
        return \Redirect::route('applicant.profile', $application_id);
    }

    public function removeFromFavouriteApplicant(Request $request, $application_id, $user_id, $job_id, $company_id)
    {
        $data['user_id'] = $user_id;
        $data['job_id'] = $job_id;
        $data['company_id'] = $company_id;
        FavouriteApplicant::where('user_id', $user_id)
                ->where('job_id', '=', $job_id)
                ->where('company_id', '=', $company_id)
                ->delete();

        flash(__('Job seeker has been removed from favorites list'))->success();
        return \Redirect::route('applicant.profile', $application_id);
    }

    public function companyDetail(Request $request, $company_slug)
    {
//        $company = $post = Company::where('slug', 'like', $company_slug)->firstOrFail();
        $company = $post = Company::where('verified',1)
            ->where('country_id','<>','0')->where('state_id','<>','0')->where('city_id','<>','0')
            ->where('country_id', '<>', '')->where('state_id', '<>', '')->where('city_id', '<>', '')
            ->where('slug', 'like', $company_slug)->firstOrFail();

//        if (!isset(Auth::guard('company')->user()->id) || Auth::guard('company')->user()->id != $post->id) {
//            return response(view('errors.404'), 404);
//        }

        /*         * ************************************************** */
        $seo = $this->getCompanySEO($company);
        /*         * ************************************************** */

        if(Auth::guard('company')->user()){
             return view('company.detail')
                        ->with('company', $company)
                        ->with('seo', $seo)
                        ->with('active', 'companyDetail');
        }else{
            return view('company.detail-public')
                        ->with('company', $company)
                        ->with('seo', $seo)
                        ->with('active', 'companyDetail'); 
        }
       
    }

    public function showContactCompanies(Request $request)
    {
        $company_slug = $request->input('slug');
//        $error_input = '{"subject":["The subject field must be true or false."],"message":["The message field must be true or false."]}';
//        $error_input = '{"alert_message":"<div role=\"alert\" class=\"alert alert-success\">Message sent successfully</div>"}';
//        $error_input = json_decode($request->input('errors'));
        $errors = null;
        if($request->input('data') != null){
            $data_input = json_decode($request->input('data'));
            $errors = $data_input->display;
        }
        $company = $post = Company::where('slug', 'like', $company_slug)->firstOrFail();

//        if (!isset(Auth::guard('company')->user()->id) || Auth::guard('company')->user()->id != $post->id) {
//            return response(view('errors.404'), 404);
//        }

        $seo = $this->getCompanySEO($company);

        $returnHTMLBody = view('company.forms.show_contact_companies_modal_body')
            ->with('company', $company)
            ->with('seo', $seo)
            ->with('active', 'companyDetail')
            ->with('errors', $errors)
            ->render();
        $returnHTMLFooter = view('company.forms.show_contact_companies_modal_footer')
            ->with('company', $company)
            ->with('seo', $seo)
            ->with('active', 'companyDetail')
            ->with('errors', $errors)
            ->render();
        $returnHTMLForm = view('company.forms.show_contact_companies_modal_form')
            ->with('company', $company)
            ->with('seo', $seo)
            ->with('active', 'companyDetail')
            ->with('errors', $errors)
            ->render();
        $title = __('Contact Company');
        return response()->json(array('success' => true,
            'html_body' => $returnHTMLBody, 'html_footer' => $returnHTMLFooter, 'html_form' => $returnHTMLForm,'title' => $title));

/*        return view('company.show_contact_companies')
                ->with('company', $company)
                ->with('seo', $seo)
                ->with('active', 'companyDetail')
                ->with('errors', $errors); */
    }

    public function sendContactForm(Request $request)
    {
        $msgresponse = Array();
        $rules = array(
            'from_name' => 'required|max:100|between:4,70',
            'from_email' => 'required|email|max:100',
            'from_phone' =>  config('rules.phone_num_rule'),
            'subject' => 'required|max:200',
            'message' => 'required',
            'to_id' => 'required',
            'g-recaptcha-response' => 'required|captcha',
        );
        $rules_messages = array(
            'from_name.required' => __('Name is required'),
            'from_email.required' => __('E-mail address is required'),
            'from_email.email' => __('Valid e-mail address is required'),
            'subject.required' => __('Subject is required'),
            'message.required' => __('Message is required'),
            'to_id.required' => __('Recieving Company details missing'),
            'g-recaptcha-response.required' => __('Please verify that you are not a robot'),
            'g-recaptcha-response.captcha' => __('Captcha error! try again'),
        );
        $validation = Validator::make($request->all(), $rules, $rules_messages);
        if ($validation->fails()) {
            $msgresponse = $validation->messages()->toJson();
            echo $msgresponse;
            exit;
        } else {
            $receiver_company = Company::findOrFail($request->input('to_id'));
            $data['company_id'] = $request->input('company_id');
            $data['company_name'] = $request->input('company_name');
            $data['from_id'] = $request->input('from_id');
            $data['to_id'] = $request->input('to_id');
            $data['from_name'] = $request->input('from_name');
            $data['from_email'] = $request->input('from_email');
            $data['from_phone'] = $request->input('from_phone');
            $data['subject'] = $request->input('subject');
            $data['message_txt'] = $request->input('message');
            $data['to_email'] = $receiver_company->email;
            $data['to_name'] = $receiver_company->name;
            $msg_save = CompanyMessage::create($data);
            $when = Carbon::now()->addMinutes(5);
            Mail::send(new CompanyContactMail($data));
            $msgresponse = ['success' => 'success', 'message' => __('Message sent successfully')];
            echo json_encode($msgresponse);
            exit;
        }
    }

    public function sendApplicantContactForm(Request $request)
    {
        $msgresponse = Array();
        $rules = array(
            'from_name' => 'required|max:100|between:4,70',
            'from_email' => 'required|email|max:100',
            'subject' => 'required|max:200',
            'message' => 'required',
            'to_id' => 'required',
        );
        $rules_messages = array(
            'from_name.required' => __('Name is required'),
            'from_email.required' => __('E-mail address is required'),
            'from_email.email' => __('Valid e-mail address is required'),
            'subject.required' => __('Subject is required'),
            'message.required' => __('Message is required'),
            'to_id.required' => __('Recieving applicant details missing'),
            'g-recaptcha-response.required' => __('Please verify that you are not a robot'),
            'g-recaptcha-response.captcha' => __('Captcha error! try again'),
        );
        $validation = Validator::make($request->all(), $rules, $rules_messages);
        if ($validation->fails()) {
            $msgresponse = $validation->messages()->toJson();
            echo $msgresponse;
            exit;
        } else {
            $receiver_user = User::findOrFail($request->input('to_id'));
            $data['user_id'] = $request->input('user_id');
            $data['user_name'] = $request->input('user_name');
            $data['from_id'] = $request->input('from_id');
            $data['to_id'] = $request->input('to_id');
            $data['from_name'] = $request->input('from_name');
            $data['from_email'] = $request->input('from_email');
            $data['from_phone'] = $request->input('from_phone');
            $data['subject'] = $request->input('subject');
            $data['message_txt'] = $request->input('message');
            $data['to_email'] = $receiver_user->email;
            $data['to_name'] = $receiver_user->getName();
            $msg_save = ApplicantMessage::create($data);
            $when = Carbon::now()->addMinutes(5);
            Mail::send(new ApplicantContactMail($data));
            $msgresponse = ['success' => 'success', 'message' => __('Message sent successfully')];
            echo json_encode($msgresponse);
            exit;
        }
    }

    public function postedJobs(Request $request)
    {
        $jobs = Auth::guard('company')->user()->jobs()->paginate(10);

        $jobs_id = array();
        foreach($jobs as $job){
            $jobs_id[] = $job->id;
        }
        $joblocation = DataArrayHelper::langMultiJobLocationArray(implode(',',$jobs_id));

        return view('job.company_posted_jobs')
                        ->with('jobs', $jobs)
                        ->with('joblocationmanager', $joblocation)
                        ->with('active', 'postedJobs');
    }

    public function listAppliedUsers(Request $request, $job_id)
    {
        $job_applications = JobApply::where('job_id', '=', $job_id)->get();

        return view('job.job_applications')
                        ->with('job_applications', $job_applications);
    }

    public function listFavouriteAppliedUsers(Request $request, $job_id)
    {
        $company_id = Auth::guard('company')->user()->id;
        $user_ids = FavouriteApplicant::where('job_id', '=', $job_id)->where('company_id', '=', $company_id)->pluck('user_id')->toArray();
        $job_applications = JobApply::where('job_id', '=', $job_id)->whereIn('user_id', $user_ids)->get();

        return view('job.job_applications')
                        ->with('job_applications', $job_applications);
    }

    public function applicantProfile($application_id)
    {

        $job_application = JobApply::findOrFail($application_id);
        $user = $job_application->getUser();
        $job = $job_application->getJob();
        $company = $job->getCompany();
        $profileCv = $job_application->getProfileCv();

        /*         * ********************************************** */
        $num_profile_views = $user->num_profile_views + 1;
        $user->num_profile_views = $num_profile_views;
        $user->update();
        /*         * ********************************************** */
        return view('user.applicant_profile')
                        ->with('job_application', $job_application)
                        ->with('user', $user)
                        ->with('job', $job)
                        ->with('company', $company)
                        ->with('profileCv', $profileCv)
                        ->with('page_title', 'Applicant Profile')
                        ->with('form_title', 'Contact Applicant');
    }

    public function userProfile($id)
    {

        $user = User::findOrFail($id);
        $profileCv = $user->getDefaultCv();

        /*         * ********************************************** */
        $num_profile_views = $user->num_profile_views + 1;
        $user->num_profile_views = $num_profile_views;
        $user->update();
        /*         * ********************************************** */
        return view('user.applicant_profile')
                        ->with('user', $user)
                        ->with('profileCv', $profileCv)
                        ->with('page_title', 'Job Seeker Profile')
                        ->with('form_title', 'Contact Job Seeker');
    }

    public function companyFollowers()
    {
        $company = Company::findOrFail(Auth::guard('company')->user()->id);
        $userIdsArray = $company->getFollowerIdsArray();
        $users = User::whereIn('id', $userIdsArray)->get();

        return view('company.follower_users')
                        ->with('users', $users)
                        ->with('company', $company)
                        ->with('active', 'companyFollowers');
    }

    public function companyMessages()
    {
        $company = Company::findOrFail(Auth::guard('company')->user()->id);
        $messages = CompanyMessage::where('company_id', '=', $company->id)
                ->orderBy('is_read', 'asc')
                ->orderBy('created_at', 'desc')
                ->get();

        return view('company.company_messages')
                        ->with('company', $company)
                        ->with('messages', $messages)
                        ->with('active', 'companyMessages');
    }

    public function companyMessageDetail($message_id)
    {
        $company = Company::findOrFail(Auth::guard('company')->user()->id);
        $message = CompanyMessage::findOrFail($message_id);
        $message->update(['is_read' => 1]);

        return view('company.company_message_detail')
                        ->with('company', $company)
                        ->with('message', $message);
    }

}
