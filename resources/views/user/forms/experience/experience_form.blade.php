<style>
    .select-single .select2-selection__rendered {
        line-height: 33px !important;
    }
    .select-single .select2-container .select2-selection--single {
        height: 37px !important;
    }
    .select-single .select2-selection__arrow {
        height: 36px !important;
    }
</style>
<div class="modal-body">
    <div class="form-body" id="experience">
        <div class="formrow" id="div_title">
            <label class="form-lable">Experience Title</label>
            <input class="form-control" id="title" placeholder="{{__('Experience Title').' '.__('(required)')}}" name="title" type="text" value="{{(isset($profileExperience)? $profileExperience->title:'')}}">
            <span class="help-block title-error"></span> </div>

        <div class="formrow" id="div_company">
            <label class="form-lable">Company</label>
            <input class="form-control" id="company" placeholder="{{__('Company').' '.__('(required)')}}" name="company" type="text" value="{{(isset($profileExperience)? $profileExperience->company:'')}}">
            <span class="help-block company-error"></span> </div>
        <div class="formrow select-single" id="div_career_levels_id">
            <label class="form-lable">Career Level</label>
            <?php
            $career_levels_id=(isset($profileExperience->career_levels_id)) ? $profileExperience->career_levels_id : '';
               ?>
           {!! Form::select('career_levels_id', [''=>__('Select Career Level')]+$careerLevels, $career_levels_id, array('class'=>'form-control select-multiple-career-level', 'id'=>'career_levels_id')) !!}
            <span class="help-block career_levels_id-error"></span> </div>

        <div class="formrow select-single" id="div_industry_id">
             <label class="form-lable">Industry</label>
            <?php
            $industry_id=(isset($profileExperience->industry_id)) ? $profileExperience->industry_id : '';
               ?>
           {!! Form::select('industry_id', [''=>__('Select Industry')]+$industries, $industry_id, array('class'=>'form-control select-multiple-industry', 'id'=>'industry_id')) !!}
            <span class="help-block industry_id-error"></span> </div>

        <div class="formrow select-single" id="div_functional_area_id">
            <label class="form-lable">Functional Area</label>
            <?php
            $functional_area_id=(isset($profileExperience->functional_area_id)) ? $profileExperience->functional_area_id : '';
               ?>
           {!! Form::select('functional_area_id', [''=>__('Select Functional Area')]+$functionalAreas, $functional_area_id, array('class'=>'form-control select-multiple-functional-area', 'id'=>'functional_area_id')) !!}
            <span class="help-block functional_area_id-error"></span> </div>


        <div class="formrow select-single" id="div_country_id">
            <label class="form-lable">Select Country</label>
            <?php
            $country_id = (isset($profileExperience) ? $profileExperience->country_id : null);
            ?>
            {!! Form::select('country_id', ['' => __('Select Country').' '.__('(required)')]+$countries, $country_id, array('class'=>'form-control select-multiple-country', 'id'=>'country_id')) !!}
            <span class="help-block country_id-error"></span> </div>


        <div class="formrow select-single" id="div_state_id">
            <label class="form-lable">Select State</label>
            <?php
            $state_id = (isset($profileExperience) ? $profileExperience->state_id: null);
            ?>
            <span id="default_state_dd">
                {!! Form::select('state_id', ['' => __('Select State').' '.__('(required)')], $state_id, array('class'=>'form-control select-multiple-state', 'id'=>'state_id')) !!}
            </span>
            <span class="help-block state_id-error"></span> </div>

        <div class="formrow select-single" id="div_city_id">
            <label class="form-lable">Select City</label>
            <?php
            $city_id = (isset($profileExperience) ? $profileExperience->city_id: null);
            ?>
            <span id="default_city_dd">
                {!! Form::select('city_id', ['' => __('Select City').' '.__('(required)')], $city_id, array('class'=>'form-control select-multiple-city', 'id'=>'city_id')) !!}
            </span>
            <span class="help-block city_id-error"></span> </div>

        <div class="formrow" id="div_date_start">
            <label class="form-lable">Experience Start Date</label>
            @php
                if(isset($profileExperience)){
                    $originalDate = $profileExperience->date_start;
                    $startDate = date("M j, Y", strtotime($originalDate));
                }
            @endphp
            <input class="form-control datepicker"  autocomplete="off" id="date_start" placeholder="{{__('Experience Start Date').' '.__('(required)')}}" name="date_start" type="text" value="{{(isset($profileExperience)? $startDate:'')}}">
            <span class="help-block date_start-error"></span> </div>
        <div class="formrow" id="div_date_end">
            <label class="form-lable">Experience End Date</label>
            @php
                $originalDate = NULL;
                $endDate = NULL;

                if(isset($profileExperience) && $profileExperience->date_end !== NULL){
                $originalDate = $profileExperience->date_end;
  $endDate = date("M j, Y", strtotime($originalDate));
}
            @endphp
            <input class="form-control datepicker" autocomplete="off" id="date_end" name="date_end" type="text" value="{{(isset($profileExperience)? $endDate :'')}}"
                   @php
                       if (isset($profileExperience) && $profileExperience->is_currently_working == 1) {
                        echo "disabled";
                       }
                   @endphp

                   @if (!isset($profileExperience) OR (isset($profileExperience) && $profileExperience->is_currently_working == 0))
                   placeholder="{{__('Experience End Date').' '.__('(required)')}}"
                   @endif

            />
            <span class="help-block date_end-error"></span> </div>
        <div class="formrow" id="div_is_currently_working">

            <label for="is_currently_working" class="bold">{{__('Is Currently Working?')}}</label>
            <div class="radio-list">
                <?php
                $val_1_checked = '';
                $val_2_checked = 'checked="checked"';

                if (isset($profileExperience) && $profileExperience->is_currently_working == 1) {
                    $val_1_checked = 'checked="checked"';
                    $val_2_checked = '';
                }
                ?>
                <label class="radio-inline"><input id="currently_working" name="is_currently_working" type="radio" value="1" {{$val_1_checked}}> {{__('Yes')}} </label>
                <label class="radio-inline"><input id="not_currently_working" name="is_currently_working" type="radio" value="0" {{$val_2_checked}}> {{__('No')}} </label>
            </div>
            <span class="help-block is_currently_working-error"></span>
        </div>
        <div class="formrow" id="div_description">
             <label class="form-lable">Experience description</label>
            <textarea name="description" class="form-control" id="description" placeholder="{{__('Experience description').' '.__('(required)')}}">{{(isset($profileExperience)? $profileExperience->description:'')}}</textarea>
            <span class="help-block description-error"></span> </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('div#experience #not_currently_working').click(function(){
            $('div#experience #date_end').removeAttr("disabled");
            $('div#experience #date_end').prop('placeholder',"{{__('Experience End Date').' '.__('(required)')}}");
        });
        $('div#experience #currently_working').click(function(){
            $('div#experience #date_end').val('');
            $('div#experience #date_end').removeAttr('placeholder');
            $('div#experience #date_end').prop('disabled','disabled');
        });
        $('.select-multiple-country').select2({
            placeholder: "{{__('Select Country').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-state').select2({
            placeholder: "{{__('Select State').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-city').select2({
            placeholder: "{{__('Select City').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-career-level').select2({
            placeholder: "{{__('Select Career Level').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-industry').select2({
            placeholder: "{{__('Select Industry').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-functional-area').select2({
            placeholder: "{{__('Select Functional Area').' '.__('(required)')}}",
//            allowClear: true
        });

        $('#country_id').on('change', function (e) {
            e.preventDefault();
            filterStates(0);
        });
        $(document).on('change', '#state_id', function (e) {
            e.preventDefault();
            filterCities(0);
        });
        filterStates(<?php echo old('state_id', $state_id); ?>);
    });
    function filterStates(state_id)
    {
        var country_id = $('#country_id').val();
        if (country_id != '') {
            $.post("{{ route('filter.lang.states.dropdown') }}", {country_id: country_id, state_id: state_id, _method: 'POST', _token: '{{ csrf_token() }}'})
                .done(function (response) {
                    $('#default_state_dd, .default_state_dd').html(response);
                    filterCities(<?php echo old('city_id', $city_id); ?>);
                    $('.select-multiple-state').select2({
                        placeholder: "{{__('Select State').' '.__('(required)')}}",
//            allowClear: true
                    });
                });
        }
    }
    function filterCities(city_id)
    {
        var state_id = $('#state_id').val();
        if (state_id != '') {
            $.post("{{ route('filter.lang.cities.dropdown') }}", {state_id: state_id, city_id: city_id, _method: 'POST', _token: '{{ csrf_token() }}'})
                .done(function (response) {
                    $('#default_city_dd').html(response);
                    $('.select-multiple-city').select2({
                        placeholder: "{{__('Select City').' '.__('(required)')}}",
//            allowClear: true
                    });
                });
        }
    }
</script>