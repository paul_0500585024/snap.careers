<?php

namespace App;

use App;
use App\Traits\Lang;
use App\Traits\IsDefault;
use App\Traits\Active;
use App\Traits\Sorted;
use Illuminate\Database\Eloquent\Model;
use App\Helpers\DataArrayHelper;

class Slider extends Model
{

    use Lang;
    use IsDefault;
    use Active;
    use Sorted;

    protected $table = 'sliders';
    public $timestamps = true;
    protected $guarded = ['id'];
    //protected $dateFormat = 'U';
    protected $dates = ['created_at', 'updated_at'];
	
	public static function defaultSliders()
    {
        $array = Slider::isDefault()->active()->sorted()->get();
        return $array;
    }

    public static function langSliders()
    {
        $jobs_country_id = DataArrayHelper::langMultiGetCountryId();
        $array = Slider::selectRaw('id,slider_id,slider_image,slider_heading,slider_description,
            if(slider_link_text=\'Explore Jobs\',\'jobs?country_id[]='.$jobs_country_id.'\',slider_link) AS slider_link,
            slider_link_text,lang,is_default,is_active,sort_order,created_at,updated_at')
            ->lang()->active()->sorted()->get();
//        exit();
//        $array = Slider::lang()->active()->sorted()->get();
        if ((int) count($array) === 0) {
            $array = self::defaultSliders();
        }
        return $array;
    }
}
