@extends('layouts.app')
@section('content') 
<!-- Header start --> 
@include('includes.header') 
<!-- Header end --> 
<!-- Inner Page Title start --> 
@include('includes.inner_page_title', ['page_title'=>__('Company Search Candidate')])
<!-- Inner Page Title end -->
<div class="listpgWraper">
    <div class="container">
        <div class="row">


            <div class="col-md-12">
                <div class="userccount" style="padding:5px">
                    <div class="formpanel"> @include('flash::message')
                            <div class="row">
                                <div class="col-md-12 d-flex flex-row">
                                    <div class="mt-3 col-md-2">Talent Search</div>
                                    <div class="formrow {!! APFrmErrHelp::hasError($errors, 'search') !!} pt-2 pr-2 col-md-8">
                                        <div class="input-group">
                                            {!! Form::text('search', null, array('class'=>'form-control py-2 border-right-0 border', 'id'=>'search', 'placeholder'=>__('Postition title, Skill, Keyword'))) !!}
                                            <span class="input-group-append">
                                                <div class="input-group-text bg-transparent"><i class="fa fa-search"></i></div>
                                            </span>
                                        </div>
                                        {!! APFrmErrHelp::showErrors($errors, 'search') !!}
                                    </div>
                                    <div class="col-md-2">
                                        <button class="btn btn-theme" type="submit" style="height:40px; margin-top:7px; padding:0px">Search</button>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="mt-3">
                @include('flash::message')
                <!-- Search Result and sidebar start -->
                    <div class="row">
                        @include('company.inc.candidate_list_side_bar')
                        <div class="col">
                            <!-- Search List -->

                        </div>

                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
@include('includes.footer')
@endsection
@push('styles')
<style type="text/css">
    .userccount p{ text-align:left !important;}
</style>
@endpush