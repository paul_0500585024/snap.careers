<!-- About -->
<section class="page-section" id="about">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-5">
                &nbsp;
            </div>
            <div class="col-lg-7" style="background: rgba(255,255,255,0.8); border-radius: 30px 0px 0px 30px;">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 text-left">
                            <h6 class="text-muted pt-5 text-dark">Making your job search easy</h6>
                            <h2 class="section-heading text-dark">How Does It Work?</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2 text-left p-3">
                            <img src="{{asset('/')}}img_new/createaccount.png" class="col-lg-12 img-responsive">
                        </div>
                        <div class="col-lg-10 text-left p-3">
                            <h6 class="orange-sm mt-3 mb-0">Create An Account</h6>
                            <p class="text-muted">Create Your Account. Take control of your job search today with FREE account!</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2 text-left p-3">
                            <img src="{{asset('/')}}img_new/searchjob_1.png" class="col-lg-12 img-responsive">
                        </div>
                        <div class="col-lg-10 text-left p-3">
                            <h6 class="orange-sm mt-3 mb-0">Search Desired Job</h6>
                            <p class="text-muted">Search, find and apply the latest jobs on the go! Find your perfect job instantly.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2 text-left p-3">
                            <img src="{{asset('/')}}img_new/sendresume.png" class="col-lg-12 img-responsive">
                        </div>
                        <div class="col-lg-10 text-left p-3">
                            <h6 class="orange-sm mt-3 mb-0">Send your resume</h6>
                            <p class="text-muted">Now, take your resume, and send them directly. Take a chance to direct contact with your new boss.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>