<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CareerLevelFormRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
            case 'POST': {
                    $id = (int) $this->input('id', 0);
                    $career_level_unique = '';
                    if ($id > 0) {
                        $career_level_unique = ',id,' . $id;
                    }

                    //career_level
                    $career_level = config('rules.alnum50_rule');
                    array_push($career_level,
                        'unique:career_levels' . $career_level_unique
                    );
                    //career_level_id
                    $career_level_id = array();
                    $is_default = (int) $this->input('is_default', 0);
                    if ($is_default == 0) {
                        $career_level_id = config('rules.id_rule');
                    }
                    //is_active
                    $is_active = config('rules.bool_rule');
                    //is_default
                    $is_default = config('rules.bool_rule');
                    //lang
                    $lang = config('rules.lang_rule');

                    return [
                        'career_level' => $career_level,
                        'career_level_id' => $career_level_id,
                        'is_active' => $is_active,
                        'is_default' => $is_default,
                        'lang' => $lang,
//                        'career_level' => "required|max:200|unique:career_levels$career_level_unique",
//                        'career_level_id' => 'required_if:is_default,0',
//                        'is_active' => 'required|boolean',
//                        'is_default' => 'required|boolean',
//                        'lang' => 'required|max:10',
                    ];
                }
            default:break;
        }
    }

    public function messages()
    {
        return [
/*            'career_level.required' => 'Please enter Career Level.',
            'career_level_id.required_if' => 'Please select default/fallback Career Level.',
            'is_default.required' => 'Is this Career Level default?',
            'is_active.required' => 'Please select status.',
            'lang.required' => 'Please select language.',*/
        ];
    }

}
