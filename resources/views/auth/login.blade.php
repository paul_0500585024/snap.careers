@extends('layouts.app')
@section('content')
    <!-- Header start -->
    @include('includes.header')
    <!-- Header end -->
    <!-- Inner Page Title start -->
    <?php /* @include('includes.inner_page_title', ['page_title'=>__('Login')]) */ ?>
    <!-- Inner Page Title end -->
    <section class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    @include('flash::message')
                    <div class="row">
                        <div class="col-md-6 offset-md-3">
                            <div class="userccount userbtns">
                                <ul class="nav nav-tabs" role="tablist">
                                    <?php
                                    $c_or_e = old('candidate_or_employer', 'candidate');
                                    ?>
                                    <li class="nav-item">
                                        <a class="btn btn-theme nav-link {{($c_or_e == 'candidate')? 'active':''}}" href="#candidate" role="tab" data-toggle="tab" aria-expanded="true">{{__('Candidate')}}</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="btn btn-theme nav-link {{($c_or_e == 'employer')? 'active':''}}" href="#employer" role="tab" data-toggle="tab" aria-expanded="false">{{__('Employer')}}</a>
                                    </li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade {{($c_or_e == 'candidate')? 'show active':''}}" id="candidate">
                                        <?php /*  hide for a moment                          <div class="socialLogin">
                                        <h5>{{__('Login with Social')}}</h5>
                                        <a href="{{ url('login/jobseeker/facebook')}}" class="fb"><i class="fa fa-facebook" aria-hidden="true"></i></a><a href="{{ url('login/jobseeker/twitter')}}" class="tw"><i class="fa fa-twitter" aria-hidden="true"></i></a> </div> */ ?>
                                        <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="candidate_or_employer" value="candidate" />
                                            <div class="formpanel">
                                                <div class="formrow{{ $errors->has('email') ? ' has-error' : '' }}">
                                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="{{__('Email Address')}}">
                                                    @if ($errors->has('email'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('email') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                                <div class="formrow{{ $errors->has('password') ? ' has-error' : '' }}">
                                                    <div class="input-group">
                                                        <input type="password" class="form-control" name="password" value="" required placeholder="{{__('Password')}}" data-toggle="password">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text"><i class="fa fa-eye-slash"></i></span>
                                                        </div>
                                                    </div>
                                                    @if ($errors->has('password'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('password') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                                <input type="submit" class="btn btn-theme" value="{{__('Login')}}">
                                            </div>
                                            <!-- login form  end-->
                                        </form>
                                        <div class="socialLogin mt-4">
                                            <a href="{{ url('login/jobseeker/facebook')}}" class="fb"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                            <a href="{{ url('login/jobseeker/twitter')}}" class="tw"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                            <a href="{{ url('login/jobseeker/google')}}" class="gp"><i class="fa fa-google" aria-hidden="true"></i></a>
                                        </div>
                                        <!-- sign up form -->
                                        <div class="newuser"><i class="fa fa-user" aria-hidden="true"></i> {{__('New User')}}? <a class="fnt-theme" href="{{route('register')}}">{{__('Register Here')}}</a></div>
                                        <div class="newuser"><i class="fa fa-user" aria-hidden="true"></i> {{__('Forgot Your Password')}}? <a class="fnt-theme" href="{{ route('password.request') }}">{{__('Click here')}}</a></div>
                                        <!-- sign up form end-->
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade {{($c_or_e == 'employer')? 'show active':''}}" id="employer">
<?php /*                            <div class="socialLogin">
                                        <h5>{{__('Login with Social')}}</h5>
                                        <a href="{{ url('login/employer/facebook')}}" class="fb"><i class="fa fa-facebook" aria-hidden="true"></i></a> <a href="{{ url('login/employer/twitter')}}" class="tw"><i class="fa fa-twitter" aria-hidden="true"></i></a> </div> */ ?>
                                        <form class="form-horizontal" method="POST" action="{{ route('company.login') }}">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="candidate_or_employer" value="employer" />
                                            <div class="formpanel">
                                                <div class="formrow{{ $errors->has('email') ? ' has-error' : '' }}">
                                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="{{__('Email Address')}}">
                                                    @if ($errors->has('email'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                                <div class="formrow{{ $errors->has('password') ? ' has-error' : '' }}">
                                                    <div class="input-group">
                                                        <input id="password" type="password" class="form-control" name="password" value="" required placeholder="{{__('Password')}}" data-toggle="password">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text"><i class="fa fa-eye-slash"></i></span>
                                                        </div>
                                                    </div>
                                                    @if ($errors->has('password'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                                <input type="submit" class="btn btn-theme" value="{{__('Login')}}">
                                            </div>
                                            <!-- login form  end-->
                                        </form>
                                        <div class="socialLogin mt-4">
                                            <a href="{{ url('login/employer/facebook')}}" class="fb"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                            <a href="{{ url('login/employer/twitter')}}" class="tw"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                            <a href="{{ url('login/employer/google')}}" class="gp"><i class="fa fa-google" aria-hidden="true"></i></a>
                                        </div>
                                        <!-- sign up form -->
                                        <div class="newuser"><i class="fa fa-user" aria-hidden="true"></i> {{__('New User')}}? <a class="fnt-theme" href="{{route('register')}}">{{__('Register Here')}}</a></div>
                                        <div class="newuser"><i class="fa fa-user" aria-hidden="true"></i> {{__('Forgot Your Password')}}? <a class="fnt-theme" href="{{ route('company.password.request') }}">{{__('Click here')}}</a></div>
                                        <!-- sign up form end-->
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('includes.footer')
@endsection
