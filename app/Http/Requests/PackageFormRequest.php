<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PackageFormRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
            case 'POST': {
                    $id = (int) $this->input('id', 0);
                    $unique_id = ($id > 0) ? ',' . $id : '';
            //package_title
            $package_title = config('rules.alnum50_rule');
            //package_price
            $package_price = config('rules.price_rule');
            //package_num_days
            $package_num_days = config('rules.id_rule');
            //package_num_listings
            $package_num_listings = config('rules.id_rule');
            //package_for
            $package_for = config('rules.package_enum_rule');

                    return [
                        "package_title" => $package_title,
                        'package_price' => $package_price,
                        "package_num_days" => $package_num_days,
                        "package_num_listings" => $package_num_listings,
                        "package_for" => $package_for,


//                        "package_title" => "required|max:150",
//                        'package_price' => array(
//                            'required',
//                            'regex:/^(?:0|([1-9]\d{0,2}(,\d{3}){0,12}))(.[0-9][0-9])+$/m',
//                        ),
//                        "package_num_days" => "required|integer",
//                        "package_num_listings" => "required|integer",
//                        "package_for" => 'in:job_seeker,employer',
                    ];
                }
            default:break;
        }
    }

    public function messages()
    {
        return [
/*            'package_title.required' => 'Package Title is required',
            'package_price.required' => 'Package price is required',
            'package_num_days.required' => 'Package num days required',
            'package_num_listings.required' => 'Package num listings required',
            'package_for.required' => 'Please select package for',*/
        ];
    }

}
