<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class LanguageLevelFormRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
            case 'POST': {
                    $id = (int) $this->input('id', 0);
                    $language_level_unique = '';
                    if ($id > 0) {
                        $language_level_unique = ',id,' . $id;
                    }
                    //lang
                    $lang = config('rules.lang_rule');
                    //language_level
                    $language_level = config('rules.alnum40_rule');
                    //is_active
                    $is_active = config('rules.bool_rule');
                    //language_level_id
                    $language_level_id = array();
                    $is_default = (int) $this->input('is_default', 0);
                    if ($is_default == 0) {
                        $language_level_id = config('rules.id_rule');
                    }
                    //is_default
                    $is_default = config('rules.bool_rule');

                    $id = array();
                    return [
                        "id" => $id,
                        "lang" => $lang,
                        "language_level" => $language_level,
                        "is_default" => $is_default,
                        "language_level_id" => $language_level_id,
                        "is_active" => $is_active,


//                        "id" => "",
//                        "lang" => "required|max:10",
//                        "language_level" => "required|max:40",
//                        "is_default" => "required|boolean",
//                        "language_level_id" => "required_if:is_default,0",
//                        "is_active" => "required|boolean",
                    ];
                }
            default:break;
        }
    }

    public function messages()
    {
        return [
//            'lang.required' => 'Please select language.', 'language_level.required' => 'Please enter Language Level.', 'is_default.required' => 'Is this Language Level default/fallback ?.', 'language_level_id.required_if' => 'Please select default/fallback Language Level.', 'is_active.required' => 'Is this Language Level active?',
        ];
    }

}
