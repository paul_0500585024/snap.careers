<?php
$lang = config('default_lang');
if (isset($cmsContent))
    $lang = $cmsContent->lang;
$lang = MiscHelper::getLang($lang);
$direction = MiscHelper::getLangDirection($lang);
$queryString = MiscHelper::getLangQueryStr();
?>
{!! APFrmErrHelp::showErrorsNotice($errors) !!}
<div class="form-body">	
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'lang') !!}">
        {!! Form::label('lang', __('Language'), ['class' => 'bold']) !!}
        {!! Form::select('lang', ['' => __('Select Language').' '.__('(required)')]+$languages, $lang, ['class' => 'form-control select-multiple-language','style' => 'width:100%','onchange'=>'setLang(this.value)']) !!}
        {!! APFrmErrHelp::showErrors($errors, 'lang') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'page_id') !!}">
        {!! Form::label('page_id', 'CMS Page', ['class' => 'bold']) !!}
        {!! Form::select('page_id', ['' => __('Select CMS Page').' '.__('(required)')]+$cmsPages, null, ['class' => 'form-control select-multiple-page','style' => 'width:100%']) !!}
        {!! APFrmErrHelp::showErrors($errors, 'page_id') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'page_title') !!}">
        {!! Form::label('page_title', __('Page Title'), ['class' => 'bold']) !!}
        {!! Form::text('page_title', null, array('class'=>'form-control', 'id'=>'page_title', 'placeholder'=>__('Page Title').' '.__('(required)'), 'dir'=>$direction)) !!}
        {!! APFrmErrHelp::showErrors($errors, 'page_title') !!}                                       
    </div>    
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'page_content') !!}">
        {!! Form::label('page_content', __('Page Content'), ['class' => 'bold']) !!}
        {!! Form::textarea('page_content', null, array('class'=>'form-control', 'id'=>'page_content', 'placeholder'=>__('Page Content').' '.__('(required)'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'page_content') !!}                                       
    </div>
    <input type="file" name="image" id="image" style="display:none;" accept="image/*"/>
</div>
@push('scripts')
<script type="text/javascript">
    function setLang(lang) {
        window.location.href = "<?php echo url(Request::url()) . $queryString; ?>" + lang;
    }
    $(document).ready(function () {
        $('.select-multiple-language').select2({
            placeholder: "{{__('Select Language').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-page').select2({
            placeholder: "{{__('Select Page').' '.__('(required)')}}",
//            allowClear: true
        });
    });
</script>
@include('admin.shared.cms_form_tinyMCE', array($lang, $direction))
@endpush