{!! APFrmErrHelp::showErrorsNotice($errors) !!}
@include('flash::message')
<div class="form-body">
    <h3>Drag and Drop to Sort States</h3>
    {!! Form::select('lang', ['' => __('Select Language')]+$languages, 'en', array('class'=>'form-control select-multiple-language','style' => 'width:100%', 'id'=>'lang', 'onchange'=>'refreshStateSortData();')) !!}
    <div id="stateSortDataDiv"></div>
</div>
@push('scripts') 
<script>
    $(document).ready(function () {
        refreshStateSortData();
        $('.select-multiple-language').select2({
            placeholder: "{{__('Select Language')}}",
//            allowClear: true
        });		
    });
    function refreshStateSortData() {
        var language = $('#lang').val();
        $.ajax({
            type: "GET",
            url: "{{ route('state.sort.data') }}",
            data: {lang: language},
            success: function (responseData) {
                $("#stateSortDataDiv").html('');
                $("#stateSortDataDiv").html(responseData);
                /**************************/
                $('#sortable').sortable({
                    update: function (event, ui) {
                        var stateOrder = $(this).sortable('toArray').toString();
                        $.post("{{ route('state.sort.update') }}", {stateOrder: stateOrder, _method: 'PUT', _token: '{{ csrf_token() }}'})
                    }
                });
                $("#sortable").disableSelection();
                /***************************/
            }
        });
    }
</script> 
@endpush