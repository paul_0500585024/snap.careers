<?php

namespace App;

use App;
use App\Traits\Lang;
use App\Traits\IsDefault;
use App\Traits\Active;
use App\Traits\Sorted;
use Illuminate\Database\Eloquent\Model;

class CareerLevel extends Model
{

    use Lang;
    use IsDefault;
    use Active;
    use Sorted;

    protected $table = 'career_levels';
    public $timestamps = true;
    protected $guarded = ['id'];
    //protected $dateFormat = 'U';
    protected $dates = ['created_at', 'updated_at'];

    public static function getcareer_level($id)
    {
        $career_level = self::where('career_levels.id', '=', $id)->lang()->active()->first();
        return (isset($career_level->career_level))?$career_level->career_level:'';
    }

}
