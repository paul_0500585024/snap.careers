<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class IndustryFormRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
            case 'POST': {
                    $id = (int) $this->input('id', 0);
                    $industry_unique = '';
                    if ($id > 0) {
                        $industry_unique = ',id,' . $id;
                    }

                    //industry
                    $industry = config('rules.alnum100_rule');
                    array_push($industry,
                        'unique:industries' . $industry_unique
                    );
                    //industry_id
                    $industry_id = array();
                    $is_default = (int) $this->input('is_default', 0);
                    if ($is_default == 0) {
                        $industry_id = config('rules.id_rule');
                    }
                    //is_active
                    $is_active = config('rules.bool_rule');
                    //is_default
                    $is_default = config('rules.bool_rule');
                    //lang
                    $lang = config('rules.lang_rule');

                    return [
                        'industry' => $industry,
                        'industry_id' => $industry_id,
                        'is_active' => $is_active,
                        'is_default' => $is_default,
                        'lang' => $lang,

//                        'industry' => 'required|max:150|unique:industries' . $industry_unique,
//                        'industry_id' => 'required_if:is_default,0',
//                        'is_active' => 'required|boolean',
//                        'is_default' => 'required|boolean',
//                        'lang' => 'required|max:10',
                    ];
                }
            default:break;
        }
    }

    public function messages()
    {
        return [
/*            'industry.required' => 'Please enter Industry.',
            'industry_id.required_if' => 'Please select default/fallback Industry.',
            'is_default.required' => 'Is this Industry default?',
            'is_active.required' => 'Please select status.',
            'lang.required' => 'Please select language.',*/
        ];
    }

}
