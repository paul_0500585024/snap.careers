{!! APFrmErrHelp::showErrorsNotice($errors) !!}
@include('flash::message')
<div class="form-body">
    <h3>Drag and Drop to Sort Salary Periods</h3>
    {!! Form::select('lang', ['' => __('Select Language')]+$languages, config('default_lang'), array('class'=>'form-control select-multiple-language','style' => 'width:100%', 'id'=>'lang', 'onchange'=>'refreshSalaryPeriodSortData();')) !!}
    <div id="salaryPeriodSortDataDiv"></div>
</div>
@push('scripts') 
<script>
    $(document).ready(function () {
        refreshSalaryPeriodSortData();
        $('.select-multiple-language').select2({
            placeholder: "{{__('Select Language')}}",
//            allowClear: true
        });
		
    });
    function refreshSalaryPeriodSortData() {
        var language = $('#lang').val();
        $.ajax({
            type: "GET",
            url: "{{ route('salary.period.sort.data') }}",
            data: {lang: language},
            success: function (responseData) {
                $("#salaryPeriodSortDataDiv").html('');
                $("#salaryPeriodSortDataDiv").html(responseData);
                /**************************/
                $('#sortable').sortable({
                    update: function (event, ui) {
                        var salaryPeriodOrder = $(this).sortable('toArray').toString();
                        $.post("{{ route('salary.period.sort.update') }}", {salaryPeriodOrder: salaryPeriodOrder, _method: 'PUT', _token: '{{ csrf_token() }}'})
                    }
                });
                $("#sortable").disableSelection();
                /***************************/
            }
        });
    }
</script> 
@endpush
