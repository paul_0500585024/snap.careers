<footer class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-1"></div>

            <div class="col-sm-12 col-md-4">
                <img src="{{asset('/')}}img_new/snap-career-logo-light.png" class="img-responsive col-lg-7">
                <p class="text-justify text-light pt-3"><i  class="fa fa-envelope"></i> <a href="mailto:info@snapcareers.com" class="text-light">info@snapcareers.com</a></p>
                <ul class="social-icons">
<?php /*                    <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a class="youtube" href="#"><i class="fa fa-youtube"></i></a></li>
                    <li><a class="instagram" href="#"><i class="fa fa-instagram"></i></a></li>
                    <li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li> */ ?>
                    <li><a href="https://www.facebook.com/snapinnovations/"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="https://twitter.com/snapinnovations"><i class="fa fa-twitter"></i></a></li>
<?php /*                    <li><a href="https://www.youtube.com/channel/UCPDkgLZG1vRXQrGx0W_pfqQ"><i class="fa fa-youtube"></i></a></li> */ ?>
                    <li><a href="https://www.instagram.com/explore/tags/snapinnovations/"><i class="fa fa-instagram"></i></a></li>
                    <li><a href="https://www.linkedin.com/company/snap-innovations/"><i class="fa fa-linkedin"></i></a></li>
                </ul>
            </div>

            <div class="col-12 col-md-6 d-flex justify-content-center">
                <ul class="footer-links">
                    <li><a href="{{url('/')}}">{{__('Home')}}</a></li>
                    @php
                        use App\Helpers\DataArrayHelper;
                        $jobs_country_id = DataArrayHelper::langMultiGetCountryId();
                    @endphp
                    <li><a href="{{url('/jobs')}}?country_id={{ $jobs_country_id }}">{{__('Jobs')}}</a></li>
                    <li><a href="{{url('/companies')}}">{{__('Companies')}}</a></li>
                    <li><a href="{{ route('contact.us') }}">{{__('Contact us')}}</a></li>
                    @if(!Auth::check() && !Auth::guard('company')->check())
                        <li><a href="{{route('login')}}">{{__('Sign in')}}</a></li>
                    @endif
                </ul>
            </div>

            <?php /* Hide for a while
            <div class="col-xs-6 col-md-3">
                <ul class="footer-links">
                    <li><a href="{{url('/')}}">{{__('Home')}}</a></li>
                    @php
                        use App\Helpers\DataArrayHelper;
                        $jobs_country_id = DataArrayHelper::langMultiGetCountryId();
                    @endphp
                    <li><a href="{{url('/jobs')}}?country_id[]={{ $jobs_country_id }}">{{__('Jobs')}}</a></li>
                    <li><a href="{{url('/companies')}}">{{__('Companies')}}</a></li>
                    <li><a href="{{ route('contact.us') }}">{{__('Contact us')}}</a></li>
                    <li><a href="{{route('login')}}">{{__('Sign in')}}</a></li>
                </ul>
            </div>

             <div class="col-xs-6 col-md-3">
                <ul class="footer-links">
                    <li><a href="#">Trending Searches</a></li>
                    <li><a href="#">How does it work</a></li>
                    <li><a href="#">Featured Jobs</a></li>
                    <li><a href="#">Top Hiring Companies</a></li>
                    <li><a href="#">Success Stories</a></li>
                </ul>
            </div> */ ?>

            <div class="col-sm-12 col-md-1"></div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <p class="copyright-text text-center text-light">Copyright &copy; 2019 - <?php echo date('Y')?> <a href="{{url('/')}}" class="text-light">SNAP CAREERS</a>. All Rights Reserved</p>
            </div>
        </div>
    </div>
</footer>