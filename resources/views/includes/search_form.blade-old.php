<style>
    .select-single .select2-selection__rendered {
        line-height: 46px !important;
    }
    .select-single .select2-container .select2-selection--single {
        height: 50px !important;
    }
    .select-single .select2-selection__arrow {
        height: 49px !important;
    }
</style>
@if(Auth::guard('company')->check())
<form action="{{route('job.seeker.list')}}" method="get">
    <div class="searchbar">
		<div class="srchbox">
		<div class="row srcsubfld additional_fields">
		<div class="col-md-{{((bool)$siteSetting->country_specific_site)? 6:3}} select-single">
            {!! Form::select('functional_area_id[]', ['' => __('Select Functional Area')]+$functionalAreas, Request::get('functional_area_id', null), array('class'=>'form-control select-multiple-functional-area', 'id'=>'functional_area_id')) !!}
        </div> 
        @if((bool)$siteSetting->country_specific_site)
        {!! Form::hidden('country_id[]', Request::get('country_id[]', $siteSetting->default_country_id), array('id'=>'country_id')) !!}
        @else
        <div class="col-md-3 select-single">
          {!! Form::select('country_id[]', ['' => __('Select Country')]+$countries, Request::get('country_id', $country_id), array('class'=>'form-control select-multiple-country', 'id'=>'country_id')) !!}
          
        </div>

        @endif

        <div class="col-md-3 select-single">
            <span id="default_state_dd">
                {!! Form::select('state_id[]', ['' => __('Select State')], Request::get('state_id', null), array('class'=>'form-control select-multiple-state', 'id'=>'state_id')) !!}
            </span>
        </div>
        <div class="col-md-3 select-single">
            <span id="default_cities_dd">
                {!! Form::select('city_id[]', ['' => __('Select City')], Request::get('city_id', null), array('class'=>'form-control select-multiple-city', 'id'=>'city_id')) !!}
            </span>
        </div>
		</div>
		
		
		<div class="input-group">
		  <input type="text"  name="search" id="empsearch" value="{{Request::get('search', '')}}" class="form-control" placeholder="{{__('Enter Skills or Job Seeker Details')}}" autocomplete="off" />
		  <span class="input-group-btn">
			<input type="submit" class="btn" value="{{__('Search Job Seeker')}}">
		  </span>
		</div>
		</div>
		
       
        
    </div>
</form>
@else
<form action="{{route('job.list')}}" method="get">
    <div class="searchbar">
		<div class="srchbox">
            <div class="row srcsubfld additional_fields">
                <div class="col-md-{{((bool)$siteSetting->country_specific_site)? 6:3}} select-single">
                    {!! Form::select('functional_area_id[]', ['' => __('Select Functional Area')]+$functionalAreas, Request::get('functional_area_id', null), array('class'=>'form-control select-multiple-functional-area', 'id'=>'functional_area_id')) !!}
                </div>

                @if((bool)$siteSetting->country_specific_site)
                {!! Form::hidden('country_id[]', Request::get('country_id[]', $country_id), array('id'=>'country_id')) !!}
                @else
                <div class="col-md-3 select-single">
                    {!! Form::select('country_id[]', ['' => __('Select Country')]+$countries, Request::get('country_id', $country_id), array('class'=>'form-control select-multiple-country', 'id'=>'country_id')) !!}
                </div>
                @endif

                <div class="col-md-3 select-single">
                    <span id="default_state_dd">
                        {!! Form::select('state_id[]', ['' => __('Select State')], Request::get('state_id', null), array('class'=>'form-control select-multiple-state', 'id'=>'state_id')) !!}
                    </span>
                </div>
                <div class="col-md-3 select-single">
                    <span id="default_cities_dd">
                        {!! Form::select('city_id[]', ['' => __('Select City')], Request::get('city_id', null), array('class'=>'form-control select-multiple-city', 'id'=>'city_id')) !!}
                    </span>
                </div>
            </div>
		    <br>
		
            <div class="input-group">
                <input type="text"  name="search" id="jbsearch" value="{{Request::get('search', '')}}" class="form-control" placeholder="{{__('Enter Skills or job title')}}" autocomplete="off" />
                <span class="input-group-btn">
                    <input type="submit" class="btn" value="{{__('Search Job')}}">
                </span>
            </div>
		</div>
      
		
		
		
		
    </div>
</form>
@endif