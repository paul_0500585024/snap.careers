<?php

namespace App\Traits;

use DB;
use File;
use ImgUploader;
use App\Company;

trait CompanyTrait
{

    private function deleteCompanyLogo($id)
    {
        try {
            $company = Company::findOrFail($id);
            $image = $company->logo;
            if (!empty($image)) {
                File::delete(ImgUploader::real_public_path() . 'company_logos/thumb/' . $image);
                File::delete(ImgUploader::real_public_path() . 'company_logos/mid/' . $image);
                File::delete(ImgUploader::real_public_path() . 'company_logos/' . $image);
            }
            return 'ok';
        } catch (ModelNotFoundException $e) {
            return 'notok';
        }
    }

    private function getCompanyIdsAndNumJobs($limit = 16, $countryid=101)
    {
        $ret_val = DB::table('jobs')
            ->join('companies','companies.id','=','jobs.company_id')
            ->join('manage_job_location','manage_job_location.job_id','=','jobs.id')
            ->select('company_id', DB::raw('COUNT(jobs.company_id) AS num_jobs'))
            ->where('companies.is_top_employer','=',1)
            ->where('companies.country_id','=',$countryid)
            ->orWhere('manage_job_location.country_id','=',$countryid)
            ->groupBy('company_id')
            ->orderBy('num_jobs', 'DESC')
            ->limit($limit)
            ->get();
        if(count($ret_val) < 1){
            $ret_val = DB::table('jobs')
                ->join('companies','companies.id','=','jobs.company_id')
                ->join('manage_job_location','manage_job_location.job_id','=','jobs.id')
                ->select('company_id', DB::raw('COUNT(jobs.company_id) AS num_jobs'))
                ->where('companies.is_top_employer','=',1)
                ->groupBy('company_id')
                ->orderBy('num_jobs', 'DESC')
                ->limit($limit)
                ->get();
        }
        return $ret_val;
    }

    private function getIndustryIdsFromCompanies($limit = 16)
    {
        $companies = Company::select('industry_id')->active()->whereHas('jobs', function ($query) {
                    $query->active()->notExpire();
                })->withCount(['jobs' => function ($query) {
                        $query->active()->notExpire();
                    }])->get();

        $industries_array = [];
        foreach ($companies as $company) {
            if (isset($industries_array[$company->industry_id])) {
                $industries_array[$company->industry_id] = $industries_array[$company->industry_id] + $company->jobs_count;
            } else {
                $industries_array[$company->industry_id] = $company->jobs_count;
            }
        }
        arsort($industries_array);
        return array_slice($industries_array, 0, $limit - 1, true);
    }
    private function getIndustryIdsFromCompaniesnew($limit = 16, $countryid=101)
    {
        $ret_val = Company::select('companies.industry_id', DB::raw('COUNT(companies.industry_id) AS num_jobs'))
                        ->groupBy('companies.industry_id')
                        ->notExpire() 
                        ->join('jobs','jobs.company_id','=','companies.id')
                        ->join('manage_job_location','manage_job_location.job_id','=','jobs.id')
                        ->where('manage_job_location.country_id','=',$countryid)
                        ->where('jobs.is_active','=',1)
                        ->orderBy('num_jobs', 'DESC')
                        ->limit($limit)
                        ->get();
        if(count($ret_val) < 1) {
            $ret_val = Company::select('companies.industry_id', DB::raw('COUNT(companies.industry_id) AS num_jobs'))
                ->groupBy('companies.industry_id')
                ->notExpire()
                ->join('jobs','jobs.company_id','=','companies.id')
                ->join('manage_job_location','manage_job_location.job_id','=','jobs.id')
                ->where('jobs.is_active','=',1)
                ->orderBy('num_jobs', 'DESC')
                ->limit($limit)
                ->get();
        }
        return $ret_val;
                        // join with company , job , manage_job_location
    }
    private function getCompanySEO($company)
    {
        $description = 'Company ';
        $keywords = '';

        $description .= ' ' . $company->name;
        $keywords .= $company->name . ',';

        $description .= ' ' . $company->getIndustry('industry');
        $keywords .= $company->getIndustry('industry') . ',';

        $description .= ' ' . $company->getOwnershipType('ownership_type');
        $keywords .= $company->getOwnershipType('ownership_type') . ',';

        $description .= ' ' . $company->location;
        $keywords .= $company->location . ',';

        $description .= ' ' . $company->description;
        $keywords .= $company->description . ',';

        $description .= ' ' . $company->getCountry('country');
        $keywords .= $company->getCountry('country') . ',';

        $description .= ' ' . $company->getState('state');
        $keywords .= $company->getState('state') . ',';

        $description .= ' ' . $company->getCity('city');
        $keywords .= $company->getCity('city') . ',';

        $seo = (object) array(
                    'seo_title' => $description,
                    'seo_description' => $description,
                    'seo_keywords' => $keywords,
                    'seo_other' => ''
        );
        return $seo;
    }

}
