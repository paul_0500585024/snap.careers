<form class="form" id="add_edit_profile_project" method="PUT" action="{{ route('update.front.profile.project', [$profileProject->id, $user->id]) }}">
	{{ csrf_field() }}
	@include('user.forms.project.project_form')
	<div class="modal-footer">
		<button type="button" class="btn btn-large btn-theme" onClick="submitProfileProjectForm();">{{__('Update Project')}} <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></button>
	</div>
</form>