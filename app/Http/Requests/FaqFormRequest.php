<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class FaqFormRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //faq_question
        $faq_question = config('rules.alnum200_rule');
        //faq_answer
        $faq_answer = config('rules.alnum200_rule');
        //lang
        $lang = config('rules.lang_rule');

        return [
            'faq_question' => $faq_question,
            'faq_answer' => $faq_answer,
            "lang" => $lang,

//            'faq_question' => 'required',
//            'faq_answer' => 'required'
        ];
    }

    public function messages()
    {
        return [
/*            'faq_question.required' => 'Please enter question.',
            'faq_answer.required' => 'Please enter answer.'*/
        ];
    }

}
