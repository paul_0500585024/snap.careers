<?php

namespace App\Exports;

use App\User;
use Illuminate\Support\Collection;
//use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromQuery;

class UsersExport implements FromQuery, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function query()
    {
//        return User::query();
/*        $query = User::query()->select('id,first_name,middle_name,last_name,name,email,father_name,date_of_birth,gender_id,marital_status_id,nationality_id,
        national_id_card_number,country_id,state_id,city_id,country_phone_phone,phone,country_phone_mobile_num,mobile_num,
        job_experience_id,career_level_id,industry_id,functional_area_id,current_salary,expected_salary,salary_currency,street_address,is_active,verified,verification_token,remember_token,created_at,updated_at,is_immediate_available,num_profile_views,package_id,package_start_date,package_end_date,jobs_quota,availed_jobs_quota,is_subscribed,country_phone_wa_id,whatsapp,telegram,skype');*/


$query = User::query()->select('users.id','users.first_name','users.middle_name','users.last_name','users.name','users.email','users.father_name','users.date_of_birth','genders.gender','marital_statuses.marital_status','users.nationality_id',
    'users.national_id_card_number','countries.country','states.state','cities.city','users.country_phone_phone','users.phone','users.country_phone_mobile_num','users.mobile_num',
    'job_experiences.job_experience','career_levels.career_level','industries.industry','functional_areas.functional_area','users.current_salary','users.expected_salary','users.salary_currency','users.street_address','users.is_active','users.verified','users.verification_token AS vt','users.remember_token AS rt','users.created_at','users.updated_at','users.is_immediate_available','users.num_profile_views','users.is_subscribed','users.country_phone_wa_id','users.whatsapp','users.telegram','users.skype','users.provider AS up','users.provider_id AS upi')
    ->leftjoin('genders', 'users.gender_id', '=', 'genders.id')
    ->leftjoin('marital_statuses', 'users.marital_status_id', '=', 'marital_statuses.id')
/*    ->leftjoin('countries', 'users.nationality_id', '=', 'countries.id')*/
    ->leftjoin('countries', 'users.country_id', '=', 'countries.id')
    ->leftjoin('states', 'users.state_id', '=', 'states.id')
    ->leftjoin('cities', 'users.city_id', '=', 'cities.id')
    ->leftjoin('job_experiences', 'users.job_experience_id', '=', 'job_experiences.job_experience_id')
    ->leftjoin('career_levels', 'users.career_level_id', '=', 'career_levels.id')
    ->leftjoin('industries', 'users.industry_id', '=', 'industries.id')
    ->leftjoin('functional_areas', 'users.functional_area_id', '=', 'functional_areas.id')
    ->where('job_experiences.lang','=',app()->getLocale());


        return $query;
    }

    public function headings(): array
    {
        return [
            'ID','First Name','Middle Name','Last Name','Name','Email','Father Name','Date Of Birth','Gender','Marital Status',
            'Nationality ID','National Id Card Number','Country','State','City','Country Code Phone',
            'Phone','Country Code Mobile Number','Mobile Number','Job Experience','Career Level','Industry','Functional Area Id',
            'Current Salary','Expected Salary','Salary Currency','Street Address','Active','Verified','Verification Token', 'Remember Token',
            'Created At','Updated At','Immediate Available',
            'Number Profile Views', 'Subscribed','Country Code WA','Whatsapp','Telegram','Skype','Provider','Provider ID'
        ];
    }


}
