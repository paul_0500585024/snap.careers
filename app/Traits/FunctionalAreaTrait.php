<?php

namespace App\Traits;

use DB;
use App\Job;

trait FunctionalAreaTrait
{

    private function getFunctionalAreaIdsAndNumJobs($limit = 16, $countryid=101)
    {
        $ret_val = Job::select('functional_area_id', DB::raw('COUNT(jobs.functional_area_id) AS num_jobs'))
                        ->groupBy('manage_job_location.country_id','jobs.functional_area_id')
                        ->notExpire()
                        ->join('manage_job_location','manage_job_location.job_id','=','jobs.id')
                        ->where('manage_job_location.country_id','=',$countryid)
                        ->active()
                        ->orderBy('num_jobs', 'DESC')
                        ->limit($limit)
                        ->get();
        if(count($ret_val) < 1){
            $ret_val = Job::select('functional_area_id', DB::raw('COUNT(jobs.functional_area_id) AS num_jobs'))
                ->groupBy('manage_job_location.country_id','jobs.functional_area_id')
                ->notExpire()
                ->join('manage_job_location','manage_job_location.job_id','=','jobs.id')
                ->active()
                ->orderBy('num_jobs', 'DESC')
                ->limit($limit)
                ->get();
        }
        return $ret_val;
    }

}
