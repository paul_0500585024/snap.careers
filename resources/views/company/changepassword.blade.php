@extends('layouts.app')
@section('content')
    <!-- Header start -->
    @include('includes.header')
    <!-- Header end -->
    <!-- Inner Page Title start -->
    @include('includes.inner_page_title', ['page_title'=>__('Company Profile')])
    <!-- Inner Page Title end -->
    <div class="listpgWraper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="userccount">
                        <div class="formpanel"> @include('flash::message')
                        <!-- Personal Information -->

                                <div class="card">
                                    <div class="card-header"><h4>Change password</h4></div>
                                    <hr />

                                    <div class="card-body">
                                        @if (session('error'))
                                            <div class="alert alert-danger">
                                                {{ session('error') }}
                                            </div>
                                        @endif
                                        @if (session('success'))
                                            <div class="alert alert-success">
                                                {{ session('success') }}
                                            </div>
                                        @endif
                                        <form class="form-horizontal" method="POST" action="{{ route('company.change.password') }}">
                                            {{ csrf_field() }}

                                            <div class="form-group{{ $errors->has('current-password') ? ' has-error' : '' }}">
                                                <label for="new-password" class="col-md-4 control-label">Current Password</label>

                                                <div class="col-md-12">
                                                    <div class="input-group">
                                                        <input id="current-password" type="password" class="form-control" name="current-password" data-toggle="password" required>
                                                        <div class="input-group-append">
                                                            <span class="input-group-text"><i class="fa fa-eye-slash"></i></span>
                                                        </div>
                                                    </div>
                                                    @if ($errors->has('current-password'))
                                                        <span class="help-block">
                                <strong>{{ $errors->first('current-password') }}</strong>
                            </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('new-password') ? ' has-error' : '' }}">
                                                <label for="new-password" class="col-md-4 control-label">New Password</label>

                                                <div class="col-md-12">
													<div class="input-group">
														<input id="new-password" type="password" class="form-control" name="new-password" data-toggle="password" required>
														<div class="input-group-append">
															<span class="input-group-text"><i class="fa fa-eye-slash"></i></span>
														</div>
													</div>
														

                                                    @if ($errors->has('new-password'))
                                                        <span class="help-block">
                                <strong>{{ $errors->first('new-password') }}</strong>
                            </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="new-password-confirm" class="col-md-4 control-label">Confirm New Password</label>

                                                <div class="col-md-6">
                                                    <input id="new-password-confirm" type="password" class="form-control" name="new-password_confirmation" required>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-6 col-md-offset-4">
                                                    <button type="submit" class="btn btn-theme">
                                                        Change Password
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('includes.footer')
@endsection
@push('styles')
    <style type="text/css">
        .userccount p{ text-align:left !important;}
    </style>
@endpush