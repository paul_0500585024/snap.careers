<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CityFormRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
            case 'POST': {
                    //id
                    $id = array();
                    //lang
                    $lang = config('rules.lang_rule');
                    //country_id
                    $country_id = config('rules.id_rule');
                    //state_id
                    $state_id = config('rules.id_rule');
                    //city
                    $city = config('rules.alnum50_rule');
                    //city_id
/*                    $city_id = array();
                    $is_default = (int) $this->input('is_default', 0);
                    if ($is_default == 0) {
                        $city_id = config('rules.id_rule');
                    }*/
                    //is_active
                    $is_active = config('rules.bool_rule');
                    //is_default
                    $is_default = config('rules.bool_rule');

                    return [
                        "id" => $id,
                        "country_id" => $country_id,
                        "state_id" => $state_id,
                        "city" => $city,
//                        "is_default" => $is_default,
//                        "city_id" => $city_id,
                        "is_active" => $is_active,
                        "lang" => $lang,

//                        "id" => "",
//                        "lang" => "required|max:10",
//                        "state_id" => "required|integer",
//                        "city" => "required|max:30",
//                        "is_default" => "required|boolean",
//                        "city_id" => "required_if:is_default,0",
//                        "is_active" => "required|boolean",
                    ];
                }
            default:break;
        }
    }

    public function messages()
    {
        return [
//            'lang.required' => 'Please select language.', 'state_id.required' => 'Please select State.', 'city.required' => 'Please enter City Name.', 'is_default.required' => 'Is this city default/fallback ?.', 'city_id.required_if' => 'Please select default/fallback City.', 'is_active.required' => 'Is this City active?',
        ];
    }

}
