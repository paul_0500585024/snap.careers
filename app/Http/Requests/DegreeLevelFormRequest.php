<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class DegreeLevelFormRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
            case 'POST': {
                    $id = (int) $this->input('id', 0);
                    $degree_level_unique = '';
                    if ($id > 0) {
                        $degree_level_unique = ',id,' . $id;
                    }
                    //lang
                    $lang = config('rules.lang_rule');
                    //degree_level
                    $degree_level = config('rules.alnum100_rule');
                    array_push($degree_level,
                        'unique:degree_levels' . $degree_level_unique
                    );
                    //degree_level_id
                    $degree_level_id = array();
                    $is_default = (int) $this->input('is_default', 0);
                    if ($is_default == 0) {
                        $degree_level_id = config('rules.id_rule');
                    }
                    //is_active
                    $is_active = config('rules.bool_rule');
                    //is_default
                    $is_default = config('rules.bool_rule');


                    $id = array();
                    return [
                        "id" => $id,
                        "lang" => $lang,
                        "degree_level" => $degree_level,
                        "is_default" => $is_default,
                        "degree_level_id" => $degree_level_id,
                        "is_active" => $is_active
//                        "id" => "",
//                        "lang" => "required|max:10",
//                        "degree_level" => "required|max:200|unique:degree_levels$degree_level_unique",
//                        "is_default" => "required|boolean",
//                        "degree_level_id" => "required_if:is_default,0",
//                        "is_active" => "required|boolean",
                    ];
                }
            default:break;
        }
    }

    public function messages()
    {
        return [
//            'lang.required' => 'Please select language.', 'degree_level.required' => 'Degree Level required.', 'is_default.required' => 'Is this Degree Level default?', 'degree_level_id.required_if' => 'Please select default/fallback Degree Level.', 'is_active.required' => 'Is this Degree Level active?',
        ];
    }

}
