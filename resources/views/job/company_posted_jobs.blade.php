<style>
    .box-job-hidden{
        height: 350px;
        overflow: hidden;
    }
    .box-job-container{
        position: relative;

    }
    .box-job-more-less{
        position: absolute;
        left: 50%;
        transform: translate(-50%, -50%);
        color:#142d99;
        cursor: pointer;
    }
    .box-job-top-class{
        top: 300px;
    }
    .box-button-job {
        display:inline-block;
        color:#444;
        border:1px solid #CCC;
        background:#DDD;
        box-shadow: 0 0 5px -1px rgba(0,0,0,0.2);
        cursor:pointer;
        vertical-align:middle;
        max-width: 100px;
        padding: 5px;
        text-align: center;
    }
</style>
@extends('layouts.app')
@section('content')
<!-- Header start -->
@include('includes.header')
<!-- Header end -->
<!-- Inner Page Title start -->
@include('includes.inner_page_title', ['page_title'=>__('Company Posted Jobs')])
<!-- Inner Page Title end -->
<div class="listpgWraper">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="myads">
                    <h3>{{__('Company Posted Jobs')}}</h3>
                    @include('flash::message')
                    <ul class="searchList">
                        <!-- job start -->
                        @if(isset($jobs) && count($jobs))
                        @foreach($jobs as $job)
                        @php $company = $job->getCompany(); @endphp
                        @if(null !== $company)
                        <li id="job_li_{{$job->id}}" class="box-job-hidden">
                            <div class="box-job-container">
                                <div class="row">
                                    <div class="col-md-8 col-sm-8">
                                        <div class="jobimg">{{$company->printCompanyImage()}}</div>
                                        <div class="jobinfo">
                                            <h3><a href="{{route('job.detail', [$job->slug])}}" title="{{$job->title}}">{{$job->title}}</a></h3>
                                            <div class="companyName"><a href="{{route('company.detail', $company->slug)}}" title="{{$company->name}}">{{$company->name}}</a></div>
                                            <div class="location">
    <?php /*                                            <label class="fulltime" title="{{$job->getJobShift('job_shift')}}">{{$job->getJobShift('job_shift')}}</label> */?>
                                                <label class="fulltime" title="{{$job->getJobType('job_type')}}">{{$job->getJobType('job_type')}}</label>
                                                <br><br>
                                                <span>
                                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
    <?php /*                                                @foreach($joblocationmanager[$job->id] as $each)
                                                    {{$each}}&nbsp;
                                                    @endforeach */?>
                                                    {{implode(', ',isset($joblocationmanager[$job->id])?$joblocationmanager[$job->id]:array())}}
                                                </span>
                                            </div>
                                            <br>
    <?php /*                                        <div class="module_posted_job line-clamp">{!! strip_tags($job->description) !!}</div> */?>
                                            <div class="job-posted-description">{!! $job->description !!}</div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 jobButtons">
                                        <div class="listbtn"><a class="btn" href="{{route('list.favourite.applied.users', [$job->id])}}">{{__('List Short Listed Candidates')}}</a></div>
                                        <div class="listbtn"><a class="btn" href="{{route('list.applied.users', [$job->id])}}">{{__('List Candidates')}}</a></div>
                                        <div class="listbtn"><a class="btn apply" href="{{route('edit.front.job', [$job->id])}}">{{__('Edit')}}</a></div>
                                        <div class="listbtn"><a class="btn report" href="javascript:;" onclick="deleteJob({{$job->id}});">{{__('Delete')}}</a></div>
                                    </div>
                                </div>
                                <div class="box-job-top-class box-button-job more-less-job"></div>
                            </div>
                        </li>
                        <!-- job end -->
                        @endif
                        @endforeach
                        @else
                            <li id="job_li_3">
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="jobinfo">
                                        <center><h3>No Data Found</h3></center>
                                        </div>
                                </div>
                            </div>
                        </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@include('includes.footer')
@endsection
@push('scripts')
<script type="text/javascript">
    function deleteJob(id) {
        var msg = 'Are you sure?';
        if (confirm(msg)) {
        $.post("{{ route('delete.front.job') }}", {id: id, _method: 'DELETE', _token: '{{ csrf_token() }}'})
                .done(function (response) {
                if (response == 'ok')
                {
                $('#job_li_' + id).remove();
                } else
                {
                alert('Request Failed!');
                }
                });
        }
    }
    $(document).ready(function(){
        $(document).on('click', '.box-job-more-less', function(e) {
            if($(this).data('val') == 'More'){
                $(this).data('val','Less');
                $(this).parent().parent().removeClass('box-job-hidden');
                $(this).html('Less');
                $(this).removeClass('box-job-top-class');
            }else{
                $(this).data('val','More');
                $(this).parent().parent().addClass('box-job-hidden');
                $(this).html('More');
                $(this).addClass('box-job-top-class');
            }
        });
        $('.more-less-job').each(function(index){
            if($(this).parent().find('div.job-posted-description').outerHeight()>96){
                $(this).addClass('box-job-more-less');
                $(this).html('More');
                $(this).data('val','More');
            }
        });
    });

</script>
@endpush