@php
if(!isset($active))$active='';
@endphp
<nav class="navbar navbar-expand-lg navbar-dark" style="position:sticky;top:0;background-color:#26272b; z-index:2000" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="{{url('/')}}"><img src="{{asset('/')}}img_new/snap-career-logo.png" class="img-responsive col-lg-7 p-0"></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="slide-collapse-left-right" data-target=".navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fa fa-bars"></i>
        </button>
		<?php /*************** web ***************/ ?>
			<?php /*************** main navigations ***************/ ?>
				<div class="collapse navbar-collapse navbarResponsive flex-column" id="navbarResponsive">
					<ul class="navbar-nav ml-auto">
						<?php /*************** Home ***************/ ?>
							<li class="nav-item d-none d-lg-block">
								<a class="nav-link js-scroll-trigger {{ Request::url() == route('index') ? 'active' : '' }}" href="{{url('/')}}">{{__('Home')}}</a>
							</li>
						<?php /*************** end Home ***************/ ?>
						<?php /*************** Jobs ***************/ ?>
							@php
								use App\Helpers\DataArrayHelper;
								$jobs_country_id = DataArrayHelper::langMultiGetCountryId();
							@endphp
							<li class="nav-item d-none d-lg-block">
								<a class="nav-link js-scroll-trigger {{ Request::url() == route('job.list') ? 'active' : '' }}" href="{{url('/jobs')}}?country_id={{ $jobs_country_id }}">{{__('Jobs')}}</a>
							</li>
						<?php /*************** end Jobs ***************/ ?>
						<?php /*************** Companies ***************/ ?>
							<li class="nav-item d-none d-lg-block">
								<a class="nav-link js-scroll-trigger {{ Request::url() == route('company.listing') ? 'active' : '' }}" href="{{url('/companies')}}">{{__('Companies')}}</a>
							</li>
						<?php /*************** end Companies ***************/ ?>
						<?php /*************** Page Slugs ***************/ ?>
							@foreach($show_in_top_menu as $top_menu) @php $cmsContent = App\CmsContent::getContentBySlug($top_menu->page_slug); @endphp
							<li class="nav-item d-none d-lg-block">
								<a class="nav-link js-scroll-trigger {{ Request::url() == route('cms', $top_menu->page_slug) ? 'active' : '' }}" href="{{ route('cms', $top_menu->page_slug) }}">{{ $cmsContent->page_title }}</a>
							</li>
							@endforeach
						<?php /*************** end Page Slugs ***************/ ?>
						<?php /*************** Contact Us ***************/ ?>
							<li class="nav-item d-none d-lg-block">
								<a class="nav-link js-scroll-trigger {{ Request::url() == route('contact.us') ? 'active' : '' }}" href="{{ route('contact.us') }}">{{__('Contact us')}}</a>
							</li>
						<?php /*************** end Contact Us ***************/ ?>
						<?php /*************** Logout ***************/?>
							@if(Auth::check())
							<li class="nav-item d-none d-lg-block">
								<a class="nav-link js-scroll-trigger" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form-header').submit();">{{__('Logout')}}</a>
							</li>
							@endif
							@if(Auth::guard('company')->check())
							<li class="nav-item d-lg-none d-lg-block">
								<a class="nav-link js-scroll-trigger" href="{{ route('company.logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form-header1').submit();">{{__('Logout')}}</a>
							</li>
							@endif
						<?php /*************** end Logout ***************/?>
						<?php /*************** Register or Login ***************/?>
							@if(!Auth::user() && !Auth::guard('company')->user())
							<li class="nav-item d-none d-lg-block">
								<div class="btn-group btn-block">
									<button type="button" class="btn btn-theme border-radius-0" onclick="location.href='{{route('register')}}'">{{__('Register')}}</button>
									&nbsp;
									<button type="button" class="btn btn-theme border-radius-0" onclick="location.href='{{route('login')}}'">{{__('Sign in')}}</button>
								</div>
							</li>
							@endif
						<?php /*************** end Register or Login ***************/?>
					</ul>
				</div>
			<?php /*************** end main navigations ***************/ ?>
		<?php /*************** end web ***************/ ?>


		<?php /*************** mobile ***************/ ?>
			<?php /*************** main navigations ***************/ ?>
				<div class="collapse navbar-collapse navbarResponsive flex-column" id="navbarResponsive">
					<ul class="navbar-nav ml-auto d-lg-none ">
						<?php /*************** Home ***************/ ?>
							<li class="nav-item">
								<a class="nav-link js-scroll-trigger {{ Request::url() == route('index') ? 'active' : '' }}" href="{{url('/')}}"><i class="fa fa-home" aria-hidden="true"></i> {{__('Home')}}</i></a>
							</li>
						<?php /*************** end Home ***************/ ?>
						<?php /*************** Jobs ***************/ ?>
							<li class="nav-item">
								<a class="nav-link js-scroll-trigger {{ Request::url() == route('job.list') ? 'active' : '' }}" href="{{url('/jobs')}}?country_id={{ $jobs_country_id }}"><i class="fa fa-briefcase" aria-hidden="true"></i> {{__('Jobs')}}</a>
							</li>
						<?php /*************** end Jobs ***************/ ?>
						<?php /*************** Companies ***************/ ?>
							<li class="nav-item">
								<a class="nav-link js-scroll-trigger {{ Request::url() == route('company.listing') ? 'active' : '' }}" href="{{url('/companies')}}"><i class="fa fa-building" aria-hidden="true"></i> {{__('Companies')}}</a>
							</li>
						<?php /*************** end Companies ***************/ ?>
						<?php /*************** Page Slugs ***************/ ?>
							@foreach($show_in_top_menu as $top_menu) @php $cmsContent = App\CmsContent::getContentBySlug($top_menu->page_slug); @endphp
							<li class="nav-item">
								<a class="nav-link js-scroll-trigger {{ Request::url() == route('cms', $top_menu->page_slug) ? 'active' : '' }}" href="{{ route('cms', $top_menu->page_slug) }}">{{ $cmsContent->page_title }}</a>
							</li>
							@endforeach
						<?php /*************** end Page Slugs ***************/ ?>
						<?php /*************** Contact Us ***************/ ?>
							<li class="nav-item">
								<a class="nav-link js-scroll-trigger {{ Request::url() == route('contact.us') ? 'active' : '' }}" href="{{ route('contact.us') }}"><i class="fa fa-phone-square" aria-hidden="true"></i> {{__('Contact us')}}</a>
							</li>
						<?php /*************** end Contact Us ***************/ ?>
						<?php /*************** Logout ***************/?>
							@if(Auth::check() OR Auth::guard('company')->check())
							<li class="nav-item d-none d-lg-block">
								<a class="nav-link js-scroll-trigger" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form-header').submit();">{{__('Logout')}}</a>
							</li>
							@endif
						<?php /*************** end Logout ***************/?>
						<?php /*************** Register or Login ***************/?>
							@if(!Auth::user() && !Auth::guard('company')->user())
							<li class="nav-item">
								<a class="nav-link js-scroll-trigger" href="{{ route('register') }}"><i class="fa fa-id-card" aria-hidden="true"></i> {{__('Register')}}</a>
								<a class="nav-link js-scroll-trigger" href="{{ route('login') }}"><i class="fa fa-sign-in" aria-hidden="true"></i> {{__('Sign in')}}</a>
							</li>
							@endif
						<?php /*************** end Register or Login ***************/?>
					</ul>
				<?php /*************** end main navigations ***************/ ?>
				<?php /*************** second navigations ***************/ ?>
					@if(Auth::check())
					<ul class="navbar-nav mx-auto d-lg-none" style="border-top: 1px solid #4F4F4F" id="secondNav">
						<li class="nav-item">
							<a class="nav-link js-scroll-trigger {{($active=="home")?'active':''}}" href="{{route('home')}}"><i class="fa fa-tachometer" aria-hidden="true"></i> {{__('Dashboard')}}</a>
						</li>
						<li class="nav-item">
							<a class="nav-link js-scroll-trigger {{($active=="myProfile")?'active':''}}" href="{{route('my.profile') }}"><i class="fa fa-user" aria-hidden="true"></i> {{__('My Profile')}}</a>
						</li>
						<li class="nav-item">
							<a class="nav-link js-scroll-trigger {{($active=="userChangePassword")?'active':''}}" href="{{ route('change.password') }}"><i class="fa fa-lock" aria-hidden="true"></i> {{__('Change Password')}}</a>
						</li>
						<li class="nav-item">
							<a class="nav-link js-scroll-trigger {{($active=="viewPublicProfile")?'active':''}}" href="{{ route('view.public.profile', Auth::user()->id) }}"><i class="fa fa-eye" aria-hidden="true"></i> {{__('View Public Profile')}}</a>
						</li>
						<li class="nav-item">
							<a class="nav-link js-scroll-trigger {{($active=="myJobApplications")?'active':''}}" href="{{ route('my.job.applications') }}"><i class="fa fa-desktop" aria-hidden="true"></i> {{__('My Job Applications')}}</a>
						</li>
						<li class="nav-item">
							<a class="nav-link js-scroll-trigger {{($active=="myFavouriteJobs")?'active':''}}" href="{{ route('my.favourite.jobs') }}"><i class="fa fa-heart" aria-hidden="true"></i> {{__('My Favourite Jobs')}}</a>
						</li>
						<li class="nav-item">
							<a class="nav-link js-scroll-trigger {{($active=="home2")?'active':''}}" href="{{route('my.profile.cv')}}"><i class="fa fa-file-text" aria-hidden="true"></i> {{__('Manage Resume')}}</a>
						</li>
						<li class="nav-item">
							<a class="nav-link js-scroll-trigger {{($active=="myMessages")?'active':''}}" href="{{route('my.messages')}}"><i class="fa fa-envelope-o" aria-hidden="true"></i> {{__('My Messages')}}</a>
						</li>
						<li class="nav-item">
							<a class="nav-link js-scroll-trigger {{($active=="myFollowings")?'active':''}}" href="{{route('my.followings')}}"><i class="fa fa-user-o" aria-hidden="true"></i> {{__('My Followings')}}</a>
						</li>
						<li class="nav-item d-lg-none">
							<a class="nav-link js-scroll-trigger" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form-header').submit();"><i class="fa fa-sign-out" aria-hidden="true"></i> {{__('Logout')}}</a>
						</li>
						<form id="logout-form-header" action="{{ route('logout') }}" method="POST" style="display: none;">
							{{ csrf_field() }}
						</form>
					</ul>
					@endif
					@if(Auth::guard('company')->check())
						<ul class="navbar-nav mx-auto d-lg-none" style="border-top: 1px solid #4F4F4F" id="secondNav">
							<li class="nav-item">
								<a class="nav-link js-scroll-trigger {{($active=="index")?'active':''}}" href="{{route('company.home')}}"><i class="fa fa-tachometer" aria-hidden="true"></i> {{__('Dashboard')}}</a>
							</li>
							<li class="nav-item">
								<a class="nav-link js-scroll-trigger {{($active=="companyProfile")?'active':''}}" href="{{route('company.profile') }}"><i class="fa fa-user" aria-hidden="true"></i> {{__('Company Profile')}}</a>
							</li>
							<li class="nav-item">
								<a class="nav-link js-scroll-trigger {{($active=="companyDetail")?'active':''}}" href="{{route('company.detail', Auth::guard('company')->user()->slug) }}"><i class="fa fa-user" aria-hidden="true"></i> {{__('View Company Public Profile')}}</a>
							</li>
							<li class="nav-item">
								<a class="nav-link js-scroll-trigger {{($active=="jobpost")?'active':''}}" href="{{route('post.job') }}"><i class="fa fa-user" aria-hidden="true"></i> {{__('Post Job')}}</a>
							</li>
							<li class="nav-item">
								<a class="nav-link js-scroll-trigger {{($active=="postedJobs")?'active':''}}" href="{{route('posted.jobs') }}"><i class="fa fa-user" aria-hidden="true"></i> {{__('Company Jobs')}}</a>
							</li>
							<li class="nav-item">
								<a class="nav-link js-scroll-trigger {{($active=="companyMessages")?'active':''}}" href="{{route('company.messages') }}"><i class="fa fa-user" aria-hidden="true"></i> {{__('Company Messages')}}</a>
							</li>
							<li class="nav-item">
								<a class="nav-link js-scroll-trigger {{($active=="companyFollowers")?'active':''}}" href="{{route('company.followers') }}"><i class="fa fa-user" aria-hidden="true"></i> {{__('Company Followers')}}</a>
							</li>
							<li class="nav-item">
								<a class="nav-link js-scroll-trigger {{($active=="companyChangePassword")?'active':''}}" href="{{ route('company.change.password') }}"><i class="fa fa-desktop" aria-hidden="true"></i> {{__('Change Password')}}</a>
							</li>
							<form id="logout-form-header1" action="{{ route('company.logout') }}" method="POST" style="display: none;">
								{{ csrf_field() }}
							</form>
						</ul>
					@endif
				</div>
			<?php /*************** end second navigations ***************/ ?>
		<?php /*************** end mobile ***************/ ?>
    </div>
    <!-- Header container end -->
</nav>

<?php /*************** web ***************/ ?>
	<?php /*************** second navigations ***************/ ?>
		@if(Auth::check())
			<nav class="navbar navbar-expand-lg navbar-dark d-none d-lg-block" id="secondmainNav" style="padding-left:0px; padding-right:0px">
				<div>
					<div class="collapse navbar-collapse navbarResponsive flex-column" id="secondnavbarResponsive">
						<ul class="navbar-nav" id="secondNav">
							<li class="nav-item">
								<a class="nav-link js-scroll-trigger {{($active=="home")?'active':''}}" href="{{route('home')}}">{{__('Dashboard')}}</a>
							</li>
							<li class="nav-item">
								<a class="nav-link js-scroll-trigger {{($active=="myProfile")?'active':''}}" href="{{route('my.profile') }}">{{__('My Profile')}}</a>
							</li>
							<li class="nav-item">
								<a class="nav-link js-scroll-trigger {{($active=="userChangePassword")?'active':''}}" href="{{ route('change.password') }}">{{__('Change Password')}}</a>
							</li>
							<li class="nav-item">
								<a class="nav-link js-scroll-trigger {{($active=="viewPublicProfile")?'active':''}}" href="{{ route('view.public.profile', Auth::user()->id) }}">{{__('View Public Profile')}}</a>
							</li>
							<li class="nav-item">
								<a class="nav-link js-scroll-trigger {{($active=="myJobApplications")?'active':''}}" href="{{ route('my.job.applications') }}">{{__('My Job Applications')}}</a>
							</li>
							<li class="nav-item">
								<a class="nav-link js-scroll-trigger {{($active=="myFavouriteJobs")?'active':''}}" href="{{ route('my.favourite.jobs') }}">{{__('My Favourite Jobs')}}</a>
							</li>
							<li class="nav-item">
								<a class="nav-link js-scroll-trigger {{($active=="home2")?'active':''}}" href="{{route('my.profile.cv')}}">{{__('Manage Resume')}}</a>
							</li>
							<li class="nav-item">
								<a class="nav-link js-scroll-trigger {{($active=="myMessages")?'active':''}}" href="{{route('my.messages')}}">{{__('My Messages')}}</a>
							</li>
							<li class="nav-item">
								<a class="nav-link js-scroll-trigger {{($active=="myFollowings")?'active':''}}" href="{{route('my.followings')}}">{{__('My Followings')}}</a>
							</li>
							<form id="logout-form-header" action="{{ route('logout') }}" method="POST" style="display: none;">
								{{ csrf_field() }}
							</form>
		<?php /*					<li class="nav-item d-lg-none">
								<a class="nav-link js-scroll-trigger" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form-header').submit();"><i class="fa fa-sign-out" aria-hidden="true"></i> {{__('Logout')}}</a>
							</li> */ ?>
						</ul>
					</div>
				</div>
				@if(Request::url() == route('my.profile') OR Request::url() == route('my.profile.experience') OR
				Request::url() == route('my.profile.education') OR Request::url() == route('my.profile.skills') OR Request::url() == route('get.addedit.front.profile.skills') OR
				Request::url() == route('my.profile.languages') OR Request::url() == route('my.profile.cv') OR
				Request::url() == route('my.profile.portfolios'))
					<div style="border-top:1px solid #4F4F4F !important">
						<div class="collapse navbar-collapse navbarResponsive flex-column" id="thirdnavbarResponsive">
							<ul class="navbar-nav" id="thirdNav">
								<li class="nav-item">
									<a class="nav-link js-scroll-trigger {{($active=="profileExperience")?'active':''}}" href="{{route('my.profile.experience')}}">{{__('Experience')}}</a>
								</li>
								<li class="nav-item">
									<a class="nav-link js-scroll-trigger {{($active=="profileEducation")?'active':''}}" href="{{route('my.profile.education') }}">{{__('Education')}}</a>
								</li>
								<li class="nav-item">
									<a class="nav-link js-scroll-trigger {{($active=="profileSkills")?'active':''}}" href="{{ route('my.profile.skills') }}">{{__('Skills')}}</a>
								</li>
								<li class="nav-item">
									<a class="nav-link js-scroll-trigger {{($active=="profileLanguages")?'active':''}}" href="{{ route('my.profile.languages') }}">{{__('Languages')}}</a>
								</li>
								<li class="nav-item">
									<a class="nav-link js-scroll-trigger {{($active=="profileCv")?'active':''}}" href="{{ route('my.profile.cv') }}">{{__('Curriculum Vitae')}}</a>
								</li>
								<li class="nav-item">
									<a class="nav-link js-scroll-trigger {{($active=="profilePortfolios")?'active':''}}" href="{{ route('my.profile.portfolios') }}">{{__('Portfolios')}}</a>
								</li>
							</ul>
						</div>
					</div>
				@endif
			</nav>
		@endif
		@if(Auth::guard('company')->check())
			<nav class="navbar navbar-expand-lg navbar-dark d-none d-lg-block" id="secondmainNav" style="padding-left:0px; padding-right:0px">
				<div>
					<div class="collapse navbar-collapse navbarResponsive flex-column" id="secondnavbarResponsive">
						<ul class="navbar-nav" id="secondNav">
							<li class="nav-item">
								<a class="nav-link js-scroll-trigger {{($active=="index")?'active':''}}" href="{{route('company.home')}}">{{__('Dashboard')}}</a>
							</li>
							<li class="nav-item">
								<a class="nav-link js-scroll-trigger {{($active=="companyProfile")?'active':''}}" href="{{route('company.profile') }}">{{__('Company Profile')}}</a>
							</li>
							<li class="nav-item">
								<a class="nav-link js-scroll-trigger {{($active=="companyDetail")?'active':''}}" href="{{route('company.detail', Auth::guard('company')->user()->slug) }}">{{__('View Company Public Profile')}}</a>
							</li>
							<li class="nav-item">
								<a class="nav-link js-scroll-trigger {{($active=="jobpost")?'active':''}}" href="{{route('post.job') }}">{{__('Post Job')}}</a>
							</li>
							<li class="nav-item">
								<a class="nav-link js-scroll-trigger {{($active=="postedJobs")?'active':''}}" href="{{route('posted.jobs') }}">{{__('Company Jobs')}}</a>
							</li>
							<li class="nav-item">
								<a class="nav-link js-scroll-trigger {{($active=="companyMessages")?'active':''}}" href="{{route('company.messages') }}">{{__('Company Messages')}}</a>
							</li>
							<li class="nav-item">
								<a class="nav-link js-scroll-trigger {{($active=="companyFollowers")?'active':''}}" href="{{route('company.followers') }}">{{__('Company Followers')}}</a>
							</li>
							<li class="nav-item">
								<a class="nav-link js-scroll-trigger {{($active=="companyChangePassword")?'active':''}}" href="{{ route('company.change.password') }}">{{__('Change Password')}}</a>
							</li>
							<form id="logout-form-header1" action="{{ route('company.logout') }}" method="POST" style="display: none;">
								{{ csrf_field() }}
							</form>
						</ul>
					</div>
				</div>
			</nav>
		@endif
	<?php /*************** end second navigations ***************/ ?>
<?php /*************** end web ***************/ ?>
