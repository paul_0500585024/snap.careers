<?php

namespace App\Http\Controllers;

use App\ProfileSummary;
use Auth;
use DB;
use Input;
use File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\UploadedFile;
use ImgUploader;
use Carbon\Carbon;
use Redirect;
use Newsletter;
use App\User;
use App\Subscription;
use App\ApplicantMessage;
use App\Company;
use App\FavouriteCompany;
use App\Gender;
use App\MaritalStatus;
use App\Country;
use App\State;
use App\City;
use App\JobExperience;
use App\JobApply;
use App\CareerLevel;
use App\Industry;
use App\FunctionalArea;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Controller;
use App\Traits\CommonUserFunctions;
use App\Traits\ProfileSummaryTrait;
use App\Traits\ProfileCvsTrait;
use App\Traits\ProfileProjectsTrait;
use App\Traits\ProfileExperienceTrait;
use App\Traits\ProfileEducationTrait;
use App\Traits\ProfileSkillTrait;
use App\Traits\ProfileLanguageTrait;
use App\Traits\Skills;
use App\Http\Requests\Front\UserFrontFormRequest;
use App\Helpers\DataArrayHelper;
use Illuminate\Support\Facades\Gate;
use App\CountryDetail;

class UserController extends Controller
{

    use CommonUserFunctions;
    use ProfileSummaryTrait;
    use ProfileCvsTrait;
    use ProfileProjectsTrait;
    use ProfileExperienceTrait;
    use ProfileEducationTrait;
    use ProfileSkillTrait;
    use ProfileLanguageTrait;
    use Skills;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth', ['only' => ['myProfile', 'updateMyProfile', 'viewPublicProfile']]);
        $this->middleware('auth', ['except' => ['showApplicantProfileEducation', 'showApplicantProfileProjects', 'showApplicantProfileExperience', 'showApplicantProfileSkills', 'showApplicantProfileLanguages']]);
    }

    public function viewPublicProfile($id)
    {
        $user = $post = User::findOrFail($id);

        if (!Gate::allows('check-user', $post)) {
            return response(view('errors.404'), 404);
        }
        if (Gate::allows('check-user', $post)) {
            $profileCv = $user->getDefaultCv();
        }

        return view('user.applicant_profile')
                        ->with('user', $user)
                        ->with('profileCv', $profileCv)
                        ->with('page_title', $user->getName())
                        ->with('form_title', 'Contact ' . $user->getName())
                         ->with('active', 'viewPublicProfile');
    }

    public function showChangePassword()
    {
        $user = User::findOrFail(Auth::user()->id);
        return view('user.changepassword')
            ->with('user', $user)
            ->with('active', 'userChangePassword');
    }


    public function ChangePassword(Request $request)
    {

        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }
        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }
        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|confirmed|min:6|max:50',
        ]);
        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();
        return redirect()->back()->with("success","Password changed successfully !");
    }


    public function myProfile()
    {
        $genders = DataArrayHelper::langGendersArray();
        $maritalStatuses = DataArrayHelper::langMaritalStatusesArray();
        $nationalities = DataArrayHelper::langNationalitiesArray();
        $countries = DataArrayHelper::langCountriesArray();
        $jobExperiences = DataArrayHelper::langJobExperiencesArray();
        $careerLevels = DataArrayHelper::langCareerLevelsArray();
        $industries = DataArrayHelper::langIndustriesArray();
        $functionalAreas = DataArrayHelper::langFunctionalAreasArray();
        $currencies = DataArrayHelper::currenciesArray();
        $upload_max_filesize = UploadedFile::getMaxFilesize() / (1048576);
        $jobSkills = DataArrayHelper::langJobSkillsArray();
        $countryPhones = DataArrayHelper::CountryDetailTelephone();
        $user = User::findOrFail(Auth::user()->id);

        if(isset($user) && count($user->countryDetailWA)){
            foreach($user->countryDetailWA as $country_detail){
                $user->country_phone_wa_id =  $country_detail->id;
            }
        }
        if(isset($user) && count($user->countryDetailPhone)){
            foreach($user->countryDetailPhone as $country_detail){
                $user->country_phone_phone =  $country_detail->id;
            }
        }
        if(isset($user) && count($user->countryDetailMobilePhone)){
            foreach($user->countryDetailMobilePhone as $country_detail){
                $user->country_phone_mobile_num =  $country_detail->id;
            }
        }

        if(isset($user->phone)){
            $text = $user->phone;
            $arr = preg_split('/\(.*?\)/',$text);
            $user->phone = isset($arr[1])?$arr[1]:null;
        }

        if(isset($user->mobile_num)){
            $text = $user->mobile_num;
            $arr = preg_split('/\(.*?\)/',$text);
            $user->mobile_num = isset($arr[1])?$arr[1]:null;
        }

        if(isset($user->whatsapp)){
            $text = $user->whatsapp;
            $arr = preg_split('/\(.*?\)/',$text);
            $user->whatsapp = isset($arr[1])?$arr[1]:null;
        }

        $jobSkillTexts = array();
        if(isset($user) && count($user->profileSkills)){
            foreach($user->profileSkills as $skill){
                $jobSkillTexts[] = $skill->getJobSkill('job_skill');
            }
        }

        return view('user.edit_profile')
                        ->with('genders', $genders)
                        ->with('maritalStatuses', $maritalStatuses)
                        ->with('nationalities', $nationalities)
                        ->with('countries', $countries)
                        ->with('jobExperiences', $jobExperiences)
                        ->with('careerLevels', $careerLevels)
                        ->with('industries', $industries)
                        ->with('functionalAreas', $functionalAreas)
                        ->with('user', $user)
                        ->with('currencies',$currencies)
                        ->with('jobSkills', $jobSkills)
                        ->with('jobSkillTexts', $jobSkillTexts)
                        ->with('upload_max_filesize', $upload_max_filesize)
                        ->with('countryPhones', $countryPhones)
                        ->with('active', 'myProfile');
    }

    public function myProfileExperience()
    {
        $genders = DataArrayHelper::langGendersArray();
        $maritalStatuses = DataArrayHelper::langMaritalStatusesArray();
        $nationalities = DataArrayHelper::langNationalitiesArray();
        $countries = DataArrayHelper::langCountriesArray();
        $jobExperiences = DataArrayHelper::langJobExperiencesArray();
        $careerLevels = DataArrayHelper::langCareerLevelsArray();
        $industries = DataArrayHelper::langIndustriesArray();
        $functionalAreas = DataArrayHelper::langFunctionalAreasArray();
        $currencies = DataArrayHelper::currenciesArray();
        $upload_max_filesize = UploadedFile::getMaxFilesize() / (1048576);
        $jobSkills = DataArrayHelper::langJobSkillsArray();
        $user = User::findOrFail(Auth::user()->id);
        $jobSkillTexts = array();
        if(isset($user) && count($user->profileSkills)){
            foreach($user->profileSkills as $skill){
                $jobSkillTexts[] = $skill->getJobSkill('job_skill');
            }
        }

        return view('user.edit_profile_experience')
            ->with('genders', $genders)
            ->with('maritalStatuses', $maritalStatuses)
            ->with('nationalities', $nationalities)
            ->with('countries', $countries)
            ->with('jobExperiences', $jobExperiences)
            ->with('careerLevels', $careerLevels)
            ->with('industries', $industries)
            ->with('functionalAreas', $functionalAreas)
            ->with('user', $user)
            ->with('currencies',$currencies)
            ->with('jobSkills', $jobSkills)
            ->with('jobSkillTexts', $jobSkillTexts)
            ->with('upload_max_filesize', $upload_max_filesize)
            ->with('active', 'profileExperience');
    }

    public function myProfileEducation()
    {
        $genders = DataArrayHelper::langGendersArray();
        $maritalStatuses = DataArrayHelper::langMaritalStatusesArray();
        $nationalities = DataArrayHelper::langNationalitiesArray();
        $countries = DataArrayHelper::langCountriesArray();
        $jobExperiences = DataArrayHelper::langJobExperiencesArray();
        $careerLevels = DataArrayHelper::langCareerLevelsArray();
        $industries = DataArrayHelper::langIndustriesArray();
        $functionalAreas = DataArrayHelper::langFunctionalAreasArray();
        $currencies = DataArrayHelper::currenciesArray();
        $upload_max_filesize = UploadedFile::getMaxFilesize() / (1048576);
        $jobSkills = DataArrayHelper::langJobSkillsArray();
        $user = User::findOrFail(Auth::user()->id);
        $jobSkillTexts = array();
        if(isset($user) && count($user->profileSkills)){
            foreach($user->profileSkills as $skill){
                $jobSkillTexts[] = $skill->getJobSkill('job_skill');
            }
        }

        return view('user.edit_profile_education')
            ->with('genders', $genders)
            ->with('maritalStatuses', $maritalStatuses)
            ->with('nationalities', $nationalities)
            ->with('countries', $countries)
            ->with('jobExperiences', $jobExperiences)
            ->with('careerLevels', $careerLevels)
            ->with('industries', $industries)
            ->with('functionalAreas', $functionalAreas)
            ->with('user', $user)
            ->with('currencies',$currencies)
            ->with('jobSkills', $jobSkills)
            ->with('jobSkillTexts', $jobSkillTexts)
            ->with('upload_max_filesize', $upload_max_filesize)
            ->with('active', 'profileEducation');
    }

    public function myProfileSkills()
    {
        $genders = DataArrayHelper::langGendersArray();
        $maritalStatuses = DataArrayHelper::langMaritalStatusesArray();
        $nationalities = DataArrayHelper::langNationalitiesArray();
        $countries = DataArrayHelper::langCountriesArray();
        $jobExperiences = DataArrayHelper::langJobExperiencesArray();
        $careerLevels = DataArrayHelper::langCareerLevelsArray();
        $industries = DataArrayHelper::langIndustriesArray();
        $functionalAreas = DataArrayHelper::langFunctionalAreasArray();
        $currencies = DataArrayHelper::currenciesArray();
        $upload_max_filesize = UploadedFile::getMaxFilesize() / (1048576);
        $jobSkills = DataArrayHelper::langJobSkillsArray();
        $user = User::findOrFail(Auth::user()->id);
        $jobSkillTexts = array();
        if(isset($user) && count($user->profileSkills)){
            foreach($user->profileSkills as $skill){
                $jobSkillTexts[] = $skill->getJobSkill('job_skill');
            }
        }

        return view('user.edit_profile_skills')
            ->with('genders', $genders)
            ->with('maritalStatuses', $maritalStatuses)
            ->with('nationalities', $nationalities)
            ->with('countries', $countries)
            ->with('jobExperiences', $jobExperiences)
            ->with('careerLevels', $careerLevels)
            ->with('industries', $industries)
            ->with('functionalAreas', $functionalAreas)
            ->with('user', $user)
            ->with('currencies',$currencies)
            ->with('jobSkills', $jobSkills)
            ->with('jobSkillTexts', $jobSkillTexts)
            ->with('upload_max_filesize', $upload_max_filesize)
            ->with('active', 'profileSkills');
    }

    public function getaddEditProfileSkills(Request $request)
    {
        $user = $post = User::findOrFail(Auth::user()->id);
        if (!Gate::allows('check-user', $post) && !Auth::guard('admin')->user()) {
            echo view('errors.404')->render();
            return;
        }
        $jobSkills = DataArrayHelper::langJobSkillsArray();
        $jobExperiences = DataArrayHelper::langJobExperiencesArray();

        return view('user.forms.skill.skill_add_edit')
            ->with('user', $user)
            ->with('jobSkills', $jobSkills)
            ->with('jobExperiences', $jobExperiences);
    }

    public function myProfileLanguages()
    {
        $genders = DataArrayHelper::langGendersArray();
        $maritalStatuses = DataArrayHelper::langMaritalStatusesArray();
        $nationalities = DataArrayHelper::langNationalitiesArray();
        $countries = DataArrayHelper::langCountriesArray();
        $jobExperiences = DataArrayHelper::langJobExperiencesArray();
        $careerLevels = DataArrayHelper::langCareerLevelsArray();
        $industries = DataArrayHelper::langIndustriesArray();
        $functionalAreas = DataArrayHelper::langFunctionalAreasArray();
        $currencies = DataArrayHelper::currenciesArray();
        $upload_max_filesize = UploadedFile::getMaxFilesize() / (1048576);
        $jobSkills = DataArrayHelper::langJobSkillsArray();
        $user = User::findOrFail(Auth::user()->id);
        $jobSkillTexts = array();
        if(isset($user) && count($user->profileSkills)){
            foreach($user->profileSkills as $skill){
                $jobSkillTexts[] = $skill->getJobSkill('job_skill');
            }
        }

        return view('user.edit_profile_languages')
            ->with('genders', $genders)
            ->with('maritalStatuses', $maritalStatuses)
            ->with('nationalities', $nationalities)
            ->with('countries', $countries)
            ->with('jobExperiences', $jobExperiences)
            ->with('careerLevels', $careerLevels)
            ->with('industries', $industries)
            ->with('functionalAreas', $functionalAreas)
            ->with('user', $user)
            ->with('currencies',$currencies)
            ->with('jobSkills', $jobSkills)
            ->with('jobSkillTexts', $jobSkillTexts)
            ->with('upload_max_filesize', $upload_max_filesize)
            ->with('active', 'profileLanguages');
    }

    public function myProfileCv()
    {
        $genders = DataArrayHelper::langGendersArray();
        $maritalStatuses = DataArrayHelper::langMaritalStatusesArray();
        $nationalities = DataArrayHelper::langNationalitiesArray();
        $countries = DataArrayHelper::langCountriesArray();
        $jobExperiences = DataArrayHelper::langJobExperiencesArray();
        $careerLevels = DataArrayHelper::langCareerLevelsArray();
        $industries = DataArrayHelper::langIndustriesArray();
        $functionalAreas = DataArrayHelper::langFunctionalAreasArray();
        $currencies = DataArrayHelper::currenciesArray();
        $upload_max_filesize = UploadedFile::getMaxFilesize() / (1048576);
        $jobSkills = DataArrayHelper::langJobSkillsArray();
        $user = User::findOrFail(Auth::user()->id);
        $jobSkillTexts = array();
        if(isset($user) && count($user->profileSkills)){
            foreach($user->profileSkills as $skill){
                $jobSkillTexts[] = $skill->getJobSkill('job_skill');
            }
        }

        return view('user.edit_profile_cv')
            ->with('genders', $genders)
            ->with('maritalStatuses', $maritalStatuses)
            ->with('nationalities', $nationalities)
            ->with('countries', $countries)
            ->with('jobExperiences', $jobExperiences)
            ->with('careerLevels', $careerLevels)
            ->with('industries', $industries)
            ->with('functionalAreas', $functionalAreas)
            ->with('user', $user)
            ->with('currencies',$currencies)
            ->with('jobSkills', $jobSkills)
            ->with('jobSkillTexts', $jobSkillTexts)
            ->with('upload_max_filesize', $upload_max_filesize)
            ->with('active', 'profileCv');
    }

    public function myProfilePortfolios()
    {
        $genders = DataArrayHelper::langGendersArray();
        $maritalStatuses = DataArrayHelper::langMaritalStatusesArray();
        $nationalities = DataArrayHelper::langNationalitiesArray();
        $countries = DataArrayHelper::langCountriesArray();
        $jobExperiences = DataArrayHelper::langJobExperiencesArray();
        $careerLevels = DataArrayHelper::langCareerLevelsArray();
        $industries = DataArrayHelper::langIndustriesArray();
        $functionalAreas = DataArrayHelper::langFunctionalAreasArray();
        $currencies = DataArrayHelper::currenciesArray();
        $upload_max_filesize = UploadedFile::getMaxFilesize() / (1048576);
        $jobSkills = DataArrayHelper::langJobSkillsArray();
        $user = User::findOrFail(Auth::user()->id);
        $jobSkillTexts = array();
        if(isset($user) && count($user->profileSkills)){
            foreach($user->profileSkills as $skill){
                $jobSkillTexts[] = $skill->getJobSkill('job_skill');
            }
        }

        return view('user.edit_profile_portfolios')
            ->with('genders', $genders)
            ->with('maritalStatuses', $maritalStatuses)
            ->with('nationalities', $nationalities)
            ->with('countries', $countries)
            ->with('jobExperiences', $jobExperiences)
            ->with('careerLevels', $careerLevels)
            ->with('industries', $industries)
            ->with('functionalAreas', $functionalAreas)
            ->with('user', $user)
            ->with('currencies',$currencies)
            ->with('jobSkills', $jobSkills)
            ->with('jobSkillTexts', $jobSkillTexts)
            ->with('upload_max_filesize', $upload_max_filesize)
            ->with('active', 'profilePortfolios');
    }

    public function updateMyProfile(UserFrontFormRequest $request)
    {
        $request['current_salary'] = str_replace(',','',$request->input('current_salary'));
        $request['expected_salary'] = str_replace(',','',$request->input('expected_salary'));

        $request['date_of_birth'] =
            Carbon::createFromFormat('M j, Y', $request->input('date_of_birth'))->format('Y-m-d');

//        $request['date_of_birth'] = date_create_from_format('M j, Y', $request->input('date_of_birth'))->format('Y-m-d');
        $user = User::findOrFail(Auth::user()->id);

        /*         * **************************************** */
        if ($request->hasFile('image_new')) {
            $is_deleted = $this->deleteUserImage($user->id);
            $image = $request->file('image_new');
            $fileName = ImgUploader::UploadImage('user_images', $image, $request->input('name'), 300, 300, false);
            $user->image = $fileName;
        }
        /*         * ************************************** */

        $user->first_name = $request->input('first_name');
        $user->middle_name = $request->input('middle_name');
        $user->last_name = $request->input('last_name');
        /*         * *********************** */
        $user->name = $user->getName();
        /*         * *********************** */
        $user->email = $request->input('email');
/*        if (!empty($request->input('password'))) {
            $user->password = Hash::make($request->input('password'));
        }*/
//        $user->father_name = $request->input('father_name');
        $user->date_of_birth = $request->input('date_of_birth');
        $user->gender_id = $request->input('gender_id');
        $user->marital_status_id = $request->input('marital_status_id');
        $user->nationality_id = $request->input('nationality_id');
        $user->national_id_card_number = $request->input('national_id_card_number');
        $user->country_id = $request->input('country_id');
        $user->state_id = $request->input('state_id');
        $user->city_id = $request->input('city_id');

        $user->country_phone_phone = $request->input('country_phone_phone');
        $country_detail_obj = CountryDetail::where('country_id','=',$user->country_phone_phone)->first();
        $country_phone_phone_code = $country_detail_obj->phone_code;
        $user->phone = '+'.'('.$country_phone_phone_code.')'.$request->input('phone');

        $user->country_phone_mobile_num = $request->input('country_phone_mobile_num');
        $country_detail_obj = CountryDetail::where('country_id','=',$user->country_phone_mobile_num)->first();
        $country_phone_mobile_num_code = $country_detail_obj->phone_code;
        $user->mobile_num = '+'.'('.$country_phone_mobile_num_code.')'.$request->input('mobile_num');

        $user->job_experience_id = $request->input('job_experience_id');
        $user->career_level_id = $request->input('career_level_id');
        $user->industry_id = $request->input('industry_id');
        $user->functional_area_id = $request->input('functional_area_id');
        $user->current_salary = $request->input('current_salary');
        $user->expected_salary = $request->input('expected_salary');
        $user->salary_currency = $request->input('salary_currency');
        $user->street_address = $request->input('street_address'); 
//        ($request->input('skype'))?$user->skype=$request->input('skype'):'';
//        ($request->input('telegram'))?$user->telegram=$request->input('telegram'):'';
//        ($request->input('Imessage'))?$user->Imessage=$request->input('Imessage'):'';
//        ($request->input('whatsapp'))?$user->whatsapp=$request->input('whatsapp'):'';
        $user->skype=$request->input('skype');
        $user->telegram=$request->input('telegram');

        $user->country_phone_wa_id = $request->input('country_phone_wa_id');
        $country_detail_obj = CountryDetail::where('country_id','=',$user->country_phone_wa_id)->first();
        $country_phone_wa_id_code = $country_detail_obj->phone_code;
        $user->whatsapp = '+'.'('.$country_phone_wa_id_code.')'.$request->input('whatsapp');

		$user->is_subscribed = $request->input('is_subscribed', 0);
        $user->update();

        $this->updateUserFullTextSearch($user);
		/*************************/
		Subscription::where('email', 'like', $user->email)->delete();
		if((bool)$user->is_subscribed)
		{			
			$subscription = new Subscription();
			$subscription->email = $user->email;
			$subscription->name = $user->name;
			$subscription->save();
			
			/*************************/
			Newsletter::subscribeOrUpdate($subscription->email, ['FNAME'=>$subscription->name]);
			/*************************/
		}
		else
		{
			/*************************/
			Newsletter::unsubscribe($user->email);
			/*************************/
		}
        ProfileSummary::where('user_id', '=', Auth::user()->id)->delete();

        $summary = $request->input('summary');
        $ProfileSummary = new ProfileSummary();
        $ProfileSummary->user_id = Auth::user()->id;
        $ProfileSummary->summary = $summary;
        $ProfileSummary->save();

        flash(__('You have updated your profile successfully'))->success();
//        return \Redirect::route('my.profile');
        return \Redirect::route('view.public.profile', Auth::user()->id);
    }

    public function addToFavouriteCompany(Request $request, $company_slug)
    {
        $data['company_slug'] = $company_slug;
        $data['user_id'] = Auth::user()->id;
        $data_save = FavouriteCompany::create($data);
        flash(__('Company has been added in favorites list'))->success();
        return \Redirect::route('company.detail', $company_slug);
    }

    public function removeFromFavouriteCompany(Request $request, $company_slug)
    {
        $user_id = Auth::user()->id;
        FavouriteCompany::where('company_slug', 'like', $company_slug)->where('user_id', $user_id)->delete();

        flash(__('Company has been removed from favorites list'))->success();
        return \Redirect::route('company.detail', $company_slug);
    }

    public function myFollowings()
    {
        $user = User::findOrFail(Auth::user()->id);
        $companiesSlugArray = $user->getFollowingCompaniesSlugArray();
        $companies = Company::whereIn('slug', $companiesSlugArray)->get();

        return view('user.following_companies')
                        ->with('user', $user)
                        ->with('companies', $companies)
                        ->with('active', 'myFollowings');
    }

    public function myMessages()
    {
        $user = User::findOrFail(Auth::user()->id);
        $messages = ApplicantMessage::where('user_id', '=', $user->id)
                ->orderBy('is_read', 'asc')
                ->orderBy('created_at', 'desc')
                ->get();

        return view('user.applicant_messages')
                        ->with('user', $user)
                        ->with('messages', $messages)
                        ->with('active', 'myMessages');
    }

    public function applicantMessageDetail($message_id)
    {
        $user = User::findOrFail(Auth::user()->id);
        $message = ApplicantMessage::findOrFail($message_id);
        $message->update(['is_read' => 1]);

        return view('user.applicant_message_detail')
                        ->with('user', $user)
                        ->with('message', $message);
    }

}
