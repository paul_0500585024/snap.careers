<?php

namespace App\Helpers;

use Request;
use App\Language;
use App\DegreeLevel;
use App\DegreeType;
use App\User;
use App\Gender;
use App\Category;
use App\Country;
use App\CountryDetail;
use App\State;
use App\City;
use App\CareerLevel;
use App\Industry;
use App\FunctionalArea;
use App\MajorSubject;
use App\ResultType;
use App\LanguageLevel;
use App\JobSkill;
use App\JobExperience;
use App\JobType;
use App\JobShift;
use App\JobTitle;
use App\Company;
use App\MaritalStatus;
use App\OwnershipType;
use App\SalaryPeriod;
use App\Video;
use App\Testimonial;
use App\Slider;
use App\AddressMultiple;
use DB;
class FunctionHelper
{
 
    public static function langSlidersArray()
    {
        $array = Slider::select('sliders.slider_heading', 'sliders.slider_id')->lang()->active()->sorted()->pluck('sliders.slider_heading', 'sliders.slider_id')->toArray();
        if ((int) count($array) === 0) {
            $array = self::defaultSlidersArray();
        }
        return $array;
    }
    public static function disguise_curl($url){

    $curl = curl_init();

    // Setup headers - I used the same headers from Firefox version 2.0.0.6
    // below was split up because php.net said the line was too long. :/
    $header[0] = "Accept: text/xml,application/xml,application/xhtml+xml,";
    $header[0] .= "text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
    $header[] = "Cache-Control: max-age=0";
    $header[] = "Connection: keep-alive";
    $header[] = "Keep-Alive: 300";
    $header[] = "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
    $header[] = "Accept-Language: en-us,en;q=0.5";
    $header[] = "Pragma: "; // browsers keep this blank.

    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 compatible RSS Fetcher');
    curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
    curl_setopt($curl, CURLOPT_REFERER, '-');
    curl_setopt($curl, CURLOPT_ENCODING, 'gzip,deflate');
    curl_setopt($curl, CURLOPT_AUTOREFERER, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 120);
    curl_setopt($curl, CURLOPT_TIMEOUT, 120);
    curl_setopt($curl, CURLOPT_MAXREDIRS, 5);

    $html = curl_exec($curl); // execute the curl command
    curl_close($curl); // close the connection

    return $html; // and finally, return $html
}
    
 public static function get_live_feeds( $url ){
    global $paginator;
    if (!isset($paginator)){ 
    } 
    $items = array();
    $has_curl = function_exists('curl_init');
    $has_iconv = function_exists('iconv');
    if ( $has_curl ){ 
        echo "dddd";
        dd($url);
        $content = disguise_curl( $url ); 
       /* if ( $has_iconv ) {
            $content = iconv("UTF-8", "UTF-8//TRANSLIT", $content);
        }
        if ( strpos($content, '<?xml') !== false ){
            $content = substr($content, strpos($content, '<response'));
        }*/

        /*$feed = @simplexml_load_string($content);
        if ( isset($feed->results) && isset($feed->results->result) ){
            $count = count($feed->results->result);
            $paginator->items_total = isset($feed->totalresults) ? (string)$feed->totalresults : 0;
        } else {
            $count = 0;
            $paginator->items_total = 0;
        }*/
       /* if ( $count ){
            foreach ($feed->results->result as $item){
                $info = array();
                $info['title'] = isset($item->jobtitle) ? (string)$item->jobtitle : '';
                $info['company'] = isset($item->company) ? (string)$item->company : '';
                $info['city'] = isset($item->city) ? (string)$item->city : '';
                $info['state'] = isset($item->state) ? (string)$item->state : '';
                $info['country'] = isset($item->country) ? (string)$item->country : '';
                $info['formattedLocation'] = isset($item->formattedLocation) ? (string)$item->formattedLocation : '';
                $info['source'] = isset($item->source) ? (string)$item->source : '';
                $info['date'] = isset($item->date) ? (string)$item->date : '';
                $info['snippet'] = isset($item->snippet) ? (string)$item->snippet : '';
                $info['url'] = isset($item->url) ? (string)$item->url : '';
                $info['onmousedown'] = isset($item->onmousedown) ? (string)$item->onmousedown : '';
                $info['jobkey'] = isset($item->jobkey) ? (string)$item->jobkey : '';

                $items[] = $info;
            }
        }*/
        $data = array('total' => (string)$feed->totalresults , 'maxcount' => (string)$feed->end , 'data' => $items); 
    } 
    return $data;
}
    /*     * **************************** */
}
