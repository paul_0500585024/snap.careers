<!-- Subscribe -->
<section class="bg-featured page-section" id="subscribe" style="padding:30px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 vcenter">
                <h3 class="section-heading text-light">{{__('Free Job Alert')}}</h3>
                <p class="text-light">{{__('Get an Email on jobs matching your criteria. No registration Required.')}}</p>
            </div>
            <div class="col-lg-6 vcenter align-self-center">
                <form method="post" action="{{ route('subscribe.newsletter')}}" name="subscribe_newsletter_form" id="subscribe_newsletter_form">
                    {{ csrf_field() }}
                    <div class="input-group subscribe mb-1">
                        <input type="email" name="email" class="form-control border-0" placeholder="Your email address">
                        <span class="input-group-btn">
                         <button class="btn btn-theme" type="submit">{{__('Subscribe')}}</button>
                         </span>
                    </div>
                </form>
                <div id="alert_messages" class="fade"></div>
            </div>
        </div>
    </div>
</section>

<!--<div class="section greybg">
  <div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10 align-center">{!! $siteSetting->index_page_below_subscribe_ad !!}</div>
    <div class="col-md-1"></div>
  </div>
</div>-->


@push('scripts') 
<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('submit', '#subscribe_newsletter_form', function () {
            var postData = $('#subscribe_newsletter_form').serialize();
            $.ajax({
                type: 'POST',
                url: "{{ route('subscribe.newsletter') }}",
                data: postData,
                //dataType: 'json',
                success: function (data)
                {
                    response = JSON.parse(data);
                    var res = response.success;
                    if (res == 'success')
                    {
                        var errorString = '<div role="alert" class="alert alert-success">' + response.message + '</div>';
                        $('#alert_messages').addClass('show');
                        $('#alert_messages').html(errorString);
                        window.setTimeout(function () {
                            $('#alert_messages').removeClass('show');
                        }, 3000);
//                        $(document).scrollTo('.alert', 2000);
                    } else
                    {
                        var errorString = '<div class="alert alert-danger" role="alert"><ul>';
                        response = JSON.parse(data);
                        $.each(response, function (index, value)
                        {
                            errorString += '<li>' + value + '</li>';
                        });
                        errorString += '</ul></div>';
                        $('#alert_messages').addClass('show');
                        $('#alert_messages').html(errorString);
                        window.setTimeout(function () {
                            $('#alert_messages').removeClass('show');
                        }, 3000);
//                        $(document).scrollTo('.alert', 2000);
                    }
                },
            });
            return false;
        });
    });
</script> 
@endpush