<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
//use App\EmailTemplate;
use App\SiteSetting;

class SubscriptionMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The demo object instance.
     *
     * @var Demo
     */
    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
//        $emailTemplate = EmailTemplate::where('name', '=', 'mail_templates')->first();
        $data = $this->data;
//        $from=SiteSetting::from();
        $from = $data['email'];
//        $data["email_template"] = $emailTemplate->content;
        $data['from'] = $from;

        return $this->from(config('mail.recieve_to.address'), config('mail.recieve_to.name'))
            ->to($data['email'], $data['email'])
            ->subject($data['subject'])
            ->view('emails.subscription_message')
            ->with($data);

/*        return $this->from('sender@example.com')
            ->view('emails.subscription_message')
            ->with(
                [
                    'testVarOne' => '1',
                    'testVarTwo' => '2',
                ]);*/
    }
}
