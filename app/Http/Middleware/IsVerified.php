<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class IsVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function username()
    {
        return 'email';
    }

    public function handle($request, Closure $next)
    {
        if(!auth()->user()->verified){
            Session::flush();
            Session::flash('_old_input.candidate_or_employer', 'candidate');
            Session::flash('_old_input.email', auth()->user()->email);
            return redirect()->to('login')->withErrors(['email'=>'Please verify your email before login.']);
        }
        return $next($request);
    }
}
