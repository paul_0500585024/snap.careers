<?php

namespace App;

use App;
use Illuminate\Database\Eloquent\Model;

class JobBenefitManager extends Model
{

    protected $table = 'manage_job_benefits';
    public $timestamps = true;
    protected $guarded = ['id'];
    //protected $dateFormat = 'U';
    protected $dates = ['created_at', 'updated_at'];

    public function job()
    {
        return $this->belongsTo('App\Job', 'job_id', 'id');
    }

    public function getJob($field = '')
    {
        if (null !== $job = $this->job()->first()) {
            if (!empty($field)) {
                return $job->$field;
            } else {
                return $job;
            }
        }
    }

    public function jobBenefit()
    {
        return $this->belongsTo('App\JobBenefit', 'job_benefit_id', 'job_benefit_id');
    }

    public function getJobBenefit($field = '')
    {
        $jobBenefit = $this->jobBenefit()->lang()->first();
        if (null === $jobBenefit) {
            $jobBenefit = $this->jobBenefit()->first();
        }
        if (null !== $jobBenefit) {
            if (!empty($field)) {
                return $jobBenefit->$field;
            } else {
                return $jobBenefit;
            }
        }
    }

}
