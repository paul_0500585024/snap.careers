<form class="form" id="add_edit_profile_cv" method="POST" action="{{ route('store.front.profile.cv', [$user->id]) }}">{{csrf_field()}}
	<input type="hidden" name="id" id="id" value="0"/>
	@include('user.forms.cv.cv_form')
	<div class="modal-footer">
		<button type="button" class="btn btn-theme" onclick="submitProfileCvForm();">{{__('Add CV')}} <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></button>
	</div>
</form>
