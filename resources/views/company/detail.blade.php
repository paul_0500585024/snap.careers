<style>
    .box-job-hidden{
        height: 350px;
        overflow: hidden;
    }
    .box-job-container{
        position: relative;

    }
    .box-job-more-less{
        position: absolute;
        left: 50%;
        transform: translate(-50%, -50%);
        color:#142d99;
        cursor: pointer;
    }
    .box-job-top-class{
        top: 300px;
        width: 300px;
        border: 15px solid green;
    }
    .box-button-job {
        display:inline-block;
        color:#444;
        border:1px solid #CCC;
        background:#DDD;
        box-shadow: 0 0 5px -1px rgba(0,0,0,0.2);
        cursor:pointer;
        vertical-align:middle;
        max-width: 100px;
        padding: 5px;
        text-align: center;
    }
    .no-border{
        border: 1px solid #18a7ff !important;
    }
</style>
@extends('layouts.app')
@section('content')
<!-- Header start -->
@include('includes.header')
<!-- Header end -->
<!-- Inner Page Title start -->
@include('includes.inner_page_title', ['page_title'=>__('Company Detail')])
<!-- Inner Page Title end -->
<div class="listpgWraper">
    <div class="container">
        @include('flash::message')
        <!-- Job Header start -->
        <div class="job-header">
            <div class="jobinfo">
                <div class="row">
                    <div class="col-xl-8 col-12">
                        <!-- Candidate Info -->
                        <div class="candidateinfo">
                            <div class="userPic no-border"><a href="{{route('company.detail',$company->slug)}}">{{$company->printCompanyImage()}}</a></div>
                            <div class="title">{{$company->name}}</div>
                            <div class="desi">{{$company->getIndustry('industry')}}</div>
                            <div class="loctext"><i class="fa fa-history" aria-hidden="true"></i> {{__('Member Since')}}, {{$company->created_at->format('M j, Y')}}</div>
                            <div class="loctext"><i class="fa fa-map-marker" aria-hidden="true"></i> {{$company->getCompanyLocationList()}}</div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-12">
                        <!-- Candidate Contact -->
                        <div class="candidateinfo">
                            @if(!empty($company->phone))
                            <div class="loctext"><i class="fa fa-phone" aria-hidden="true"></i> <a href="tel:{{$company->phone}}">{{$company->phone}}</a></div>
                            @endif
                            @if(!empty($company->email))
                            <div class="loctext"><i class="fa fa-envelope" aria-hidden="true"></i> <a href="mailto:{{$company->email}}">{{$company->email}}</a></div>
                            @endif
                            @if(!empty($company->website))
                            <div class="loctext"><i class="fa fa-globe" aria-hidden="true"></i> <a href="{{$company->website}}" target="_blank">{{$company->website}}</a></div>
                            @endif
                            <div class="cadsocial"> {!!$company->getSocialNetworkHtml()!!} </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12">
                        <div class="jobButtons">
                            @if(Auth::check() && Auth::user()->isFavouriteCompany($company->slug))
                                <a href="{{route('remove.from.favourite.company', $company->slug)}}" class="btn">
                                    <i class="fa fa-floppy-o" aria-hidden="true"></i> {{__('Favourite Company')}} </a>
                            @elseif(!Auth::guard('company')->user())
                                <a href="{{route('add.to.favourite.company', $company->slug)}}" class="btn">
                                    <i class="fa fa-floppy-o" aria-hidden="true"></i> {{__('Add to Favourite')}}</a>
                            @endif
                            @if(!Auth::guard('company')->user())
                                <a href="{{route('report.abuse.company', $company->slug)}}" class="btn report"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{__('Report Abuse')}}</a>
                            @endif
<?php /*                            <a href="#contact_company" class="btn"><i class="fa fa-envelope" aria-hidden="true"></i> {{__('Send Message')}}</a> */?>
                        </div>
                        <div class="row">
            <div class="col-md-8">
                <!-- About Employee start -->
                 @if($company->description!='')
                <div class="job-header">
                    <div class="contentbox">
                        <h3>{{__('About Company')}}</h3>
                        <p>{!! $company->description !!}</p>
                    </div>
                </div>
                  @endif
                <!-- Opening Jobs start -->
                <div class="relatedJobs">
                     @if(isset($company->jobs) && count($company->jobs))
                     <h3>{{__('Job Openings')}}</h3>
                     @endif
                    <ul class="searchList">
                        @if(isset($company->jobs) && count($company->jobs))
                        @foreach($company->jobs as $companyJob)
                        <!--Job start-->
                        <li class="box-job-hidden">
                            <div class="box-job-container">
                                <div class="row">
                                    <div class="col-md-8 col-sm-8">
                                        <div class="jobimg"><a href="{{route('job.detail', [$companyJob->slug])}}" title="{{$companyJob->title}}"> {{$company->printCompanyImage()}} </a></div>
                                        <div class="jobinfo">
                                            <h3><a href="{{route('job.detail', [$companyJob->slug])}}" title="{{$companyJob->title}}">{{$companyJob->title}}</a></h3>
                                            <div class="companyName"><a href="{{route('company.detail', $company->slug)}}" title="{{$company->name}}">{{$company->name}}</a></div>
                                            <div class="location">
                                                <label class="fulltime" title="{{$companyJob->getJobType('job_type')}}">{{$companyJob->getJobType('job_type')}}</label>
                                                <label class="partTime" title="{{$companyJob->getJobShift('job_shift')}}">{{$companyJob->getJobShift('job_shift')}}</label>
                                                - <span>{{$companyJob->getCity('city')}}</span></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="col-md-4 col-sm-4">
                                        <div class="listbtn"><a class="btn btn-theme" href="{{route('job.detail', [$companyJob->slug])}}">{{__('View Detail')}}</a></div>
                                    </div>
                                    <br>
                                    <div class="job-posted-description">{!! $companyJob->description !!}</div>
                                </div>
                                <div class="box-job-top-class box-button-job more-less-job"></div>
                            </div>
                        </li>
                        <!--Job end-->
                        @endforeach
                        @endif

                        <!-- Job end -->
                    </ul>
                </div>
            </div>
            <div class="col-md-4">
                <!-- Company Detail start -->
                <div class="job-header">
                    <div class="jobdetail">
                        <h3>{{__('Company Detail')}}</h3>
<?php /*                        <ul class="jbdetail">
                            <li class="row">
                                <div class="col-md-6 col-6">{{__('Is Email Verified')}}</div>
                                <div class="col-md-6 col-6"><span>{{((bool)$company->verified)? 'Yes':'No'}}</span></div>
                            </li>
                            <li class="row">
                                <div class="col-md-6 col-6">{{__('Total Employees')}}</div>
                                <div class="col-md-6 col-6"><span>{{$company->no_of_employees}}</span></div>
                            </li>
                            <li class="row">
                                <div class="col-md-6 col-6">{{__('Established In')}}</div>
                                <div class="col-md-6 col-6"><span>{{$company->established_in}}</span></div>
                            </li>
                            <li class="row">
                                <div class="col-md-6 col-6">{{__('Current jobs')}}</div>
                                <div class="col-md-6 col-6"><span>{{$company->countNumJobs('company_id',$company->id)}}</span></div>
                            </li>
                        </ul> */ ?>
                        <div class="jbdetail">
                            <div class="row mb-3">
                                <div class="col-md-6 col-6">{{__('Is Email Verified')}}</div>
                                <div class="col-md-6 col-6"><span>{{((bool)$company->verified)? 'Yes':'No'}}</span></div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-md-6 col-6">{{__('Total Employees')}}</div>
                                <div class="col-md-6 col-6"><span>{{$company->no_of_employees}}</span></div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-md-6 col-6">{{__('Established In')}}</div>
                                <div class="col-md-6 col-6"><span>{{$company->established_in}}</span></div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-md-6 col-6">{{__('Current jobs')}}</div>
                                <div class="col-md-6 col-6"><span>{{$company->countNumJobs('company_id',$company->id)}}</span></div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Google Map start -->
                @if($company->latitude!=0 && $company->longitude!=0)
                <div class="job-header">
                    <div class="jobdetail">
                      @include('includes.map')
                       @php

                            $country =(isset($job))?$job->countyname($company->country_id):'';
                             $state=(isset($job))?$job->statename($company->state_id):'';
                              $city= (isset($job))?$job->cityname($company->city_id):'';
                              ($company->latitude!='' OR $company->longitude!='')?
                            drow_map($company->latitude,$company->longitude , $company->name,$company->location, $country , $state , $city):'';
                            @endphp </div>
                </div>
                @endif
                <!-- Contact Company start -->

            </div>
        </div>
                    </div>
                </div>
            </div>

            <!-- Buttons -->

        </div>

        <!-- Job Detail start -->

    </div>
</div>
@include('includes.footer')
@endsection
@push('styles')
<style type="text/css">
    .formrow iframe {
        height: 78px;
    }
</style>
@endpush
@push('scripts')
<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('click', '#send_company_message', function () {
            var postData = $('#send-company-message-form').serialize();
            $.ajax({
                type: 'POST',
                url: "{{ route('contact.company.message.send') }}",
                data: postData,
                //dataType: 'json',
                success: function (data)
                {
                    response = JSON.parse(data);
                    var res = response.success;
                    if (res == 'success')
                    {
                        var errorString = '<div role="alert" class="alert alert-success">' + response.message + '</div>';
                        $('#alert_messages').html(errorString);
                        $('#send-company-message-form').hide('slow');
                        $(document).scrollTo('.alert', 2000);
                    } else
                    {
                        var errorString = '<div class="alert alert-danger" role="alert"><ul>';
                        response = JSON.parse(data);
                        $.each(response, function (index, value)
                        {
                            errorString += '<li>' + value + '</li>';
                        });
                        errorString += '</ul></div>';
                        $('#alert_messages').html(errorString);
                        $(document).scrollTo('.alert', 2000);
                    }
                },
            });
        });
    });
    $(document).ready(function(){
        $(document).on('click', '.box-job-more-less', function(e) {
            if($(this).data('val') == 'More'){
                $(this).data('val','Less');
                $(this).parent().parent().removeClass('box-job-hidden');
                $(this).html('Less');
                $(this).removeClass('box-job-top-class');
            }else{
                $(this).data('val','More');
                $(this).parent().parent().addClass('box-job-hidden');
                $(this).html('More');
                $(this).addClass('box-job-top-class');
            }
        });
        $('.more-less-job').each(function(index){
            if($(this).parent().find('div.job-posted-description').outerHeight()>174){
                $(this).addClass('box-job-more-less');
                $(this).html('More');
                $(this).data('val','More');
            }
        });
    });
</script>
@endpush