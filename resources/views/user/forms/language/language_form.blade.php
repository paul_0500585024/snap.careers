<style>
    .select-single .select2-selection__rendered {
        line-height: 33px !important;
    }
    .select-single .select2-container .select2-selection--single {
        height: 37px !important;
    }
    .select-single .select2-selection__arrow {
        height: 36px !important;
    }
</style>
<div class="modal-body">
    <div class="form-body">
        <div class="formrow select-single" id="div_language_id">
            <label class="form-lable">Select language</label>
            <?php
            $language_id = (isset($profileLanguage) ? $profileLanguage->language_id : null);
            ?>
            {!! Form::select('language_id', [''=>__('Select language')]+$languages, $language_id, array('class'=>'form-control select-multiple-language', 'id'=>'language_id')) !!} <span class="help-block language_id-error"></span> </div>
        <div class="formrow select-single" id="div_language_level_id">
            <label class="form-lable">Select Language Level</label>
            <?php
            $language_level_id = (isset($profileLanguage) ? $profileLanguage->language_level_id : null);
            ?>
            {!! Form::select('language_level_id', [''=>__('Select Language Level')]+$languageLevels, $language_level_id, array('class'=>'form-control select-multiple-language-level', 'id'=>'language_level_id')) !!} <span class="help-block language_level_id-error"></span> </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('.select-multiple-language').select2({
            placeholder: "{{__('Select language').' '.__('(required)')}}",
        });
        $('.select-multiple-language-level').select2({
            placeholder: "{{__('Select Language Level').' '.__('(required)')}}",
        });
    });
</script>
