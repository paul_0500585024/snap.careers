<div class="d-flex justify-content-start">
    <h5 onclick="showSkills();">{{__('Skills')}}</h5> &nbsp;&nbsp;&nbsp;
    <a class="fnt-theme font-weight-bold" href="{{url('/addedit-front-profile-skills')}}" style="cursor:pointer"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="" id="skill_div"></div>
    </div>
</div>

<div class="modal fade" id="add_skill_modal" role="dialog">
    <div class="modal-dialog modal-lg" style="top:110px">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="add_skill_modal_title"></h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="add_skill_modal_body"></div>
            <div class="modal-footer" id="add_skill_modal_footer"></div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>
@push('scripts')
    <script type="text/javascript">
        /**************************************************/
        function deleteUserSkillInput(element) {
            var $this = element;
            $this.parent().parent()
                .fadeOut(300, function () {
                    $(this).remove();
                });
            return false;
        }


        function addSkill(){
            var html = '<div class="row mb-2 profileskills">';
            html += '<div class="col-sm-6">';
            html += '{!! Form::text("skill[]", null, array("class"=>"form-control", "list" => "listskill")) !!}';
            html += '</div>';
            html += '<div class="col-sm-5 select-single">';
            html += '{!! Form::select("job_experience_id[]", [""=>__("Select experience")]+$jobExperiences, null, array("class"=>"form-control select-job_experience_id","style" => "width:100%")) !!}';
            html += '</div>';
            html += '<div class="col-sm-1">';
            html += '<a class="text-danger delete_user_skill" style="cursor:pointer" title="Delete" onclick="deleteUserSkillInput($(this))"><i class="fa fa-trash" aria-hidden="true"></i></a>';
            html += '</div>';
            html += '</div>';
            $('.profileskills:last').after(html);
            $('.select-job_experience_id').select2({
                placeholder: "{{__('Select Job Experience').' '.__('(required)')}}",
//            allowClear: true
            });
        }

        function showProfileSkillModal(){
            $("#add_skill_modal").modal();
            loadProfileSkillForm();
        }
        function loadProfileSkillForm(){
            $.ajax({
                type: "POST",
                url: "{{ route('get.front.profile.skill.form', $user->id) }}",
                data: {"_token": "{{ csrf_token() }}"},
                datatype: 'json',
                success: function (json) {
                    $("#add_skill_modal_title").html(json.title);
                    $("#add_skill_modal_footer").html(json.html_footer);
                    $("#add_skill_modal_body").empty();
                    $(json.html_form).appendTo($("#add_skill_modal_body")).append(json.html_body);

                }
            });
        }
        function showProfileSkillEditModal(skill_id){
            $("#add_skill_modal").modal();
            loadProfileSkillEditForm(skill_id);
        }
        function loadProfileSkillEditForm(skill_id){
            $.ajax({
                type: "POST",
                url: "{{ route('get.front.profile.skill.edit.form', $user->id) }}",
                data: {"skill_id": skill_id, "_token": "{{ csrf_token() }}"},
                datatype: 'json',
                success: function (json) {
                    $("#add_skill_modal_title").html(json.title);
                    $("#add_skill_modal_footer").html(json.html_footer);
                    $("#add_skill_modal_body").empty();
                    $(json.html_form).appendTo($("#add_skill_modal_body")).append(json.html_body);
                }
            });
        }
        function change_data_error(){
            $("[data-error^='div_job_skill_id']").each(function(i) {
                $(this).attr('data-error','div_job_skill_id.'+i);
            });
            $("[data-error^='job_skill_id']").each(function(i) {
                $(this).attr('data-error','job_skill_id.'+i);
            });
            $("[data-error^='div_job_experience_id']").each(function(i) {
                $(this).attr('data-error','div_job_experience_id.'+i);
            });
            $("[data-error^='job_experience_id']").each(function(i) {
                $(this).attr('data-error','job_experience_id.'+i);
            });
        }
        function submitProfileSkillForm() {
            change_data_error();
            var form = $('#add_edit_profile_skill');
            $.ajax({
                url     : form.attr('action'),
                type    : form.attr('method'),
                data    : form.serialize(),
                dataType: 'json',
                success : function (json){
                    $("#add_skill_modal_title").html(json.title);
                    $("#add_skill_modal_footer").html(json.html_footer);
                    $("#add_skill_modal_body").html(json.html_body);
                    showSkills();
                },
                error: function(json){
                    if (json.status === 422) {
                        var resJSON = json.responseJSON;
                        $('.help-block').html('');
                        $.each(resJSON.errors, function (key, value) {
                            $("[data-error='"+key+"']").html('<strong>' + value + '</strong>');
                            $("[data-error='div_"+key+"']").addClass('has-error');
//                            $('.' + key + '-error').html('<strong>' + value + '</strong>');
//                            $('#div_' + key).addClass('has-error');
                        });
                    } else {
                        // Error
                        // Incorrect credentials
                        // alert('Incorrect credentials. Please try again.')
                    }
                }
            });
        }
        function delete_profile_skill(id) {
            var msg = "{{__('Are you sure you want to delete?')}}";
            if (confirm(msg)) {
                $.post("{{ route('delete.front.profile.skill') }}", {id: id, _method: 'DELETE', _token: '{{ csrf_token() }}'})
                    .done(function (response) {
                        if (response == 'ok')
                        {
                            $('#skill_' + id).remove();
                        } else
                        {
                            alert('Request Failed!');
                        }
                    });
            }
        }

        function showSkills()
        {
            $.post("{{ route('show.front.profile.skills', $user->id) }}", {user_id: {{$user->id}}, _method: 'POST', _token: '{{ csrf_token() }}'})
                .done(function (response) {
                    $('#skill_div').html(response);
                });
        }

        function addeditSkills()
        {
            $.post("{{ route('addedit.front.profile.skills', $user->id) }}", {user_id: {{$user->id}}, _method: 'POST', _token: '{{ csrf_token() }}'})
                .done(function (response) {
                    $('#skill_div').html(response);
                });
            return false;
        }

        $(document).ready(function(){
            $('a.delete_user_skill').on('click', function(e){
                e.preventDefault();
                var $this = $(this);
                var id = $this.prop('id');
                var q = confirm("Are you sure want to disable all fields?");
                if(q == true) {
                    $.post("{{ route('delete.front.profile.skill') }}", {id: id, _method: 'DELETE', _token: '{{ csrf_token() }}'})
                        .done(function (response) {
                            if (response == 'ok')
                            {
                                $('#skill_' + id).remove();
                                $this.parent().parent()
                                    .fadeOut(300, function(){$(this).remove();});
                            } else
                            {
                                alert('Request Failed!');
                            }
                        });
                }
                return false;
            });
        });

        $(document).ready(function(){
            showSkills();
        });


        $('.select-job_experience_id').select2({
            placeholder: "{{__('Select Job Experience').' '.__('(required)')}}",
//            allowClear: true
        });

    </script>
@endpush