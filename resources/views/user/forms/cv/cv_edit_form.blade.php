<div class="form-body">
    <div class="formrow" id="div_title">
        <label class="form-lable">Title
            <i id="document_information"
               class="fa fa-info-circle"
               rel="popover"
               data-container="#div_title"
               data-placement="right"
               data-toggle="popover"
               data-trigger="hover"
               data-html="true"
               data-content="
               Format : <span style='font-weight:bold'>.doc</span> or <span style='font-weight:bold'>.docx</span> or <span style='font-weight:bold'>.pdf</span><br>
               Max Size :  <span style='font-weight:bold'>1MB</span>
               "></i>
        </label>
        <input class="form-control" placeholder="{{__('CV title')}}" name="title" id="title" type="text" value="{{(isset($profileCv)? $profileCv->title:'')}}">
        <span class="help-block title-error"></span> </div>

    @if(isset($profileCv))
        <div class="formrow" id="div_cv_file">
            <label class="form-lable">{{__('CV File')}} : </label> &nbsp;&nbsp;{{ImgUploader::print_doc("cvs/$profileCv->cv_file", $profileCv->title, $profileCv->title)}}
<?php /*            <span id="name-error-cv" class="help-block cv_file-error">asdf</span>
            <label for="cv_file" class="btn btn-theme" style="cursor:pointer">Replace CV</label>
            <input name="cv_file" id="cv_file" type="file" style="visibility:hidden; cursor:pointer"> */ ?>
            <div class="w-33">
                <label for="cv_file" class="btn btn-info" style="cursor:pointer;width:33%;" >Replace CV</label>
                <input name="cv_file" id="cv_file" type="file" style="visibility:hidden; cursor:pointer">
            </div>
            <span id="cv_file_name"></span>
            <span id="name-error-cv" class="help-block cv_file-error"></span>
        </div>
    @endif

    @if(!isset($profileCv))
        <div class="formrow">
            <input name="cv_file" id="cv_file" type="file" />
        </div>
    @endif

    <div class="formrow" id="div_is_default">
        <label for="is_default" class="bold">{{__('Is Default?')}}</label>
        <div class="radio-list">
            <?php
            $val_1_checked = '';
            $val_2_checked = 'checked="checked"';

            if (isset($profileCv) && $profileCv->is_default == 1) {
                $val_1_checked = 'checked="checked"';
                $val_2_checked = '';
            }
            ?>

            <label class="radio-inline"><input id="default" name="is_default" type="radio" value="1" {{$val_1_checked}}> {{__('Yes')}} </label>
            <label class="radio-inline"><input id="not_default" name="is_default" type="radio" value="0" {{$val_2_checked}}> {{__('No')}} </label>
        </div>
        <span class="help-block is_default-error"></span>
    </div>
</div>
<script>
    $(function () {
        $('[data-toggle="popover"]').popover()
    });
    $(document).ready(function () {
//        $("#cv_file").change(function() {
///*            filename = this.files[0].name
//            console.log(filename);*/
//            var files = this.files;
//            console.log(files);
//        });

        var fileInputCV = document.getElementById("cv_file");
        fileInputCV.addEventListener("change", function (e) {
            var files = this.files
            checkfile(files)
        }, false)
        function checkfile(files) {
            for (var i = 0; i < files.length; i++) {
                var file = files[i]
                $('#cv_file_name').html(file.name);
                var err_message = ''
                if(file.size >  1000000){
                    err_message = "The cv file may not be greater than 1 MB";
                }
                if (!file.type.match('application/pdf') &&
                    !file.type.match('application/msword') &&
                    !file.type.match('application/vnd.openxmlformats-officedocument.wordprocessingml.document')
                ) {
                    err_message = "Only PDF and DOC and DOCX files can be uploaded.";
                }
                if(err_message != ''){
                    $('.cv_file-error').html('<strong>' + err_message + '</strong>');
                    $('#cv_file_name').empty();
                    $('#div_cv_file').addClass('has-error');
                    return false;
                }
                $('#div_cv_file').removeClass('has-error');
                $('#div_cv_file span.cv_file-error').html('');


            }
        }


<?php /*        var fileInputCV = document.getElementById("cv_file");
        fileInputCV.addEventListener("change", function (e) {
            var files = this.files
            checkfile(files)
        }, false)
        function checkfile(files) {
            for (var i = 0; i < files.length; i++) {
                var file = files[i]
                    var err_message = ''
                    if(file.size >  1000000){
                        err_message = "The cv file may not be greater than 1 MB";
                    }
                    if (!file.type.match('application/pdf') &&
                        !file.type.match('application/msword') &&
                        !file.type.match('application/vnd.openxmlformats-officedocument.wordprocessingml.document')
                    ) {
                        err_message = "Only PDF and DOC and DOCX files can be uploaded.";
                    }
                    if(err_message != ''){
                        $('.cv_file-error').html('<strong>' + err_message + '</strong>');
                        $('#div_cv_file').addClass('has-error');
                        return false;
                    }
                    $('#div_cv_file').removeClass('has-error');
                    $('#div_cv_file span.cv_file-error').html('');
            }
        } */ ?>
    });
    function remove_cv(){
        alert();
    }
</script>