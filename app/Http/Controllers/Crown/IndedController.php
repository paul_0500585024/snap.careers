<?php

namespace App\Http\Controllers\Crown;

use Auth;
use DB;
use Input;
use Redirect;
use App\Country;
use App\State;
use App\City;
use App\Job;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Helpers\FunctionHelper;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Controller;
use App\Traits\JobTrait;

class IndedController extends Controller
{

    use JobTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    { 
       // error_reporting(0);
       // $this->middleware('company');
    }
    public function createIndedJob()
    {  
       $items = $this->get_live_feeds('http://api.indeed.com/ads/apisearch?sort=date&publisher=17083005191573&q=all&l=');        
             $a=($items['maxcount']);
             $b=($items['total']); 
             $i=0;
            while($a<=$b){
                $this->insertdata($items,1);
                $items = $this->get_live_feeds('http://api.indeed.com/ads/apisearch?sort=date&publisher=17083005191573&q=all&l=&start='.$a);
                $a=($items['maxcount']);
                $b=($items['total']);
                $i++;
                if($i>=0){
                    break;
               } 
           }
    }
    public function updateIndedJob()
    {  
        $this->searchbySkill();
       /*$items = $this->get_live_feeds('http://api.indeed.com/ads/apisearch?sort=date&publisher=17083005191573&q=all&l=');        
             $a=($items['maxcount']);
             $b=($items['total']); 
             $i=0;
             while($a<=$b){
                $this->insertdata($items,1);
                $items = $this->get_live_feeds('http://api.indeed.com/ads/apisearch?sort=date&publisher=17083005191573&q=all&l=&start='.$a);
                $a=($items['maxcount']);
                $b=($items['total']);
                $i++;
                if($i>=0){
                    break;
               } 
           }*/
    }
    public function searchbySkill($value='')
    {
        
    }
    public function get_live_feeds($url='')
    { 
        $has_curl = function_exists('curl_init');
        $has_iconv = function_exists('iconv');
        $content=$this->getData($url);
        if ( $has_iconv ) {
            $content = iconv("UTF-8", "UTF-8//TRANSLIT", $content);
        }
        if ( strpos($content, '<?xml') !== false ){
            $content = substr($content, strpos($content, '<response'));
        }

        $feed = @simplexml_load_string($content); 
        if ( isset($feed->results) && isset($feed->results->result) ){
            $count = count($feed->results->result); 
        } else {
            $count = 0; 
        }
        if ( $count ){
            foreach ($feed->results->result as $item){
                $info = array();
                $info['title'] = isset($item->jobtitle) ? (string)$item->jobtitle : '';
                $info['company'] = isset($item->company) ? (string)$item->company : '';
                $info['city'] = isset($item->city) ? (string)$item->city : '';
                $info['state'] = isset($item->state) ? (string)$item->state : '';
                $info['country'] = isset($item->country) ? (string)$item->country : '';
                $info['formattedLocation'] = isset($item->formattedLocation) ? (string)$item->formattedLocation : '';
                $info['source'] = isset($item->source) ? (string)$item->source : '';
                $info['date'] = isset($item->date) ? (string)$item->date : '';
                $info['snippet'] = isset($item->snippet) ? (string)$item->snippet : '';
                $info['url'] = isset($item->url) ? (string)$item->url : '';
                $info['onmousedown'] = isset($item->onmousedown) ? (string)$item->onmousedown : '';
                $info['jobkey'] = isset($item->jobkey) ? (string)$item->jobkey : '';

                $items[] = $info;
            }
        }
        $data = array('total' => (string)$feed->totalresults , 'maxcount' => (string)$feed->end , 'data' => $items);
        return ($data); 
    }
    public function getData($url='')
    {
        $curl = curl_init();
        $header[0] = "Accept: text/xml,application/xml,application/xhtml+xml,";
        $header[0] .= "text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
        $header[] = "Cache-Control: max-age=0";
        $header[] = "Connection: keep-alive";
        $header[] = "Keep-Alive: 300";
        $header[] = "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
        $header[] = "Accept-Language: en-us,en;q=0.5";
        $header[] = "Pragma: "; // browsers keep this blank.

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 compatible RSS Fetcher');
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_REFERER, '-');
        curl_setopt($curl, CURLOPT_ENCODING, 'gzip,deflate');
        curl_setopt($curl, CURLOPT_AUTOREFERER, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 120);
        curl_setopt($curl, CURLOPT_TIMEOUT, 120);
        curl_setopt($curl, CURLOPT_MAXREDIRS, 5);

        $html = curl_exec($curl); // execute the curl command
        curl_close($curl); // close the connection
        return $html; // and finally, return $html
    }
    public function insertdata($data='', $companyId='1')
    {
        $job = new Job();
        echo "<pre>"; 
        foreach ($data['data'] as $key => $value) { 
           $countryID=Country::countryID('IN'); 
           $StateID=State::StateID('IN'); 
           $cityId=City::getCityByName('Alachua');
           $this->assignJobValues($job,$value , $companyId);
           $job->save();
            $job->slug = str_slug($job->title, '-') . '-' . $job->id;
             $d=strtotime($value['date']); 
             $job->created_at=date("Y-m-d h:i:s", $d);
        /*         * ******************************* */
        $job->update();

       /* $this->storeJobSkills($request, $job->id); 
        $this->storeJobLlanguage($request, $job->id);
        $this->storeJobLocation($request, $job->id , $Joblocations);*/

        $this->updateFullTextSearch($job);
           dd();
        }
    }
    public function assignJobValues($job , $value, $companyId)
    { 
        $d=strtotime($value['date']); 
        $job->title = $value['title'];
        $job->description = $value['snippet'];
        $job->country_id =Country::countryID($value['country']);
        $job->state_id = State::StateID('state'); 
        $job->city_id = City::getCityByName('city');
        $job->is_freelance = 0;
        $job->career_level_id = 3;//
        $job->salary_from = 1000;//
        $job->salary_to = 2000;//
        $job->salary_currency = 'INR';//
        $job->hide_salary = 0;//
        $job->functional_area_id = 32;//
        $job->job_type_id = 3;//
        $job->job_shift_id = 1; //
        $job->num_of_positions = 1;//
        $job->gender_id = 1;//
        $job->expiry_date =date("Y-m-d h:i:s", strtotime("+1 month", $d));
        $job->degree_level_id =4;//
        $job->job_experience_id = 3;//
        $job->salary_period_id =2;//
        $job->multi_location = '1'; 
        $job->company_id = $companyId;
        $job->is_active = 1;
        $job->is_featured = 1;
        $job->post_type = 1; 
        $job->source_company = $value['source'];
        $job->url = $value['url'];
        $job->job_key = $value['jobkey'];
        return $job;
    }
    private function updateFullTextSearch($job)
    {
        $str = '';
        $str .= $job->getCompany('name');
        $str .= ' ' . $job->getCountry('country');
        $str .= ' ' . $job->getState('state');
        $str .= ' ' . $job->getCity('city');
        $str .= ' ' . $job->title;
        $str .= ' ' . $job->description;
        $str .= $job->getJobSkillsStr();
        $str .= ((bool) $job->is_freelance) ? ' freelance remote work from home multiple cities' : '';
        $str .= ' ' . $job->getCareerLevel('career_level');
        $str .= ((bool) $job->hide_salary === false) ? ' ' . $job->salary_from . ' ' . $job->salary_to : '';
        $str .= $job->getSalaryPeriod('salary_period');
        $str .= ' ' . $job->getFunctionalArea('functional_area');
        $str .= ' ' . $job->getJobType('job_type');
        $str .= ' ' . $job->getJobShift('job_shift');
        $str .= ' ' . $job->getGender('gender');
        $str .= ' ' . $job->getDegreeLevel('degree_level');
        $str .= ' ' . $job->getJobExperience('job_experience'); 
        $job->search = $str;
        $job->update();
    }

}
