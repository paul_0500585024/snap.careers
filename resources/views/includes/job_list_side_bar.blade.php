<style>
    .select-single .select2-selection__rendered {
        line-height: 46px !important;
    }
    .select-single .select2-container .select2-selection--single {
        height: 50px !important;
    }
    .select-single .select2-selection__arrow {
        height: 49px !important;
    }
</style>
<div class="col-12 d-xl-none">
    <div class="sidebar">
        <form action="{{route('job.list')}}" method="get">

            <!-- Search Criteria -->
            <div class="widget">
                <h4 class="widget-title">{{__('Search Criteria')}}</h4>
                <div class="form-group select-single">
                    {!! Form::select('country_id', ['' => __('Select Country')]+$countries, Request::get('country_id',$country_id), array('class'=>'form-control select-multiple-country', 'id'=>'country_id_mobile')) !!}
                </div>
                <div class="form-group select-single">
                    <span class="default_state_dd">
                        {!! Form::select('state_id', ['' => __('Select State')], Request::get('state_id', null), array('class'=>'form-control select-multiple-state', 'id'=>'state_id_mobile')) !!}
                    </span>
                </div>
                <div class="form-group">
                    <input type="text" name="search" value="{{Request::get('search', '')}}" class="form-control" placeholder="{{__('Job Title or Keywords')}}" id="search_mobile" />
                </div>
                <div class="form-group select-single">
                    {!! Form::select('functional_area_id[]', ['' => __('Select Functional Area')]+$functionalAreas, Request::get('functional_area_id', null), array('class'=>'form-control select-multiple-functional-area', 'id'=>'functional_area_id_mobile')) !!}
                </div>
                <div class="form-group">
                    {!! Form::text('salary_from', Request::get('salary_from', null), array('class'=>'form-control', 'id'=>'salary_from_mobile', 'placeholder'=>__('Minimum Salary').' ('.$currency_default.')')) !!}
                </div>
                <div class="form-group select-single">
                    {!! Form::select('career_level_id', ['' => __('Position Levels')] + $careerLevels, Request::get('career_level_id', null), array('class'=>'form-control select-multiple-career-level', 'id'=>'career_level_id_mobile')) !!}
                </div>
                <div class="form-group select-single">
                    {!! Form::select('companies_id', ['' => __('Companie Names')] + $companies, Request::get('companies_id', null), array('class'=>'form-control select-multiple-companies', 'id'=>'companies_id_mobile')) !!}
                </div>
            </div>

            <!-- button -->
            <div class="searchnt">
                <button type="submit" class="btn btn-theme"><i class="fa fa-search" aria-hidden="true"></i> {{__('Search Jobs')}}</button>
            </div>
            <!-- button end-->
        </form>
    </div>
</div>
<div class="col-md-3 col-sm-6 d-none d-xl-block" id="desktop-jobFilterSideBar">
    <!-- Side Bar start -->
    <div class="sidebar">
        <form action="{{route('job.list')}}" method="get">
            <!-- Location -->
            <div class="widget">
                <h4 class="widget-title">{{__('Jobs Location')}}</h4>
                <div class="form-group select-single">
                    {!! Form::select('country_id', ['' => __('Select Country')]+$countries, Request::get('country_id',$country_id), array('class'=>'form-control select-multiple-country', 'id'=>'country_id_desktop')) !!}
                </div>
                <div class="form-group select-single">
                    <span class="default_state_dd">
                        {!! Form::select('state_id', ['' => __('Select State')], Request::get('state_id', null), array('class'=>'form-control select-multiple-state', 'id'=>'state_id_desktop')) !!}
                    </span>
                </div>
            </div>

            <!-- Search Criteria -->
            <div class="widget">
                <h4 class="widget-title">{{__('Search Criteria')}}</h4>
                <div class="form-group">
                    <input type="text" name="search" value="{{Request::get('search', '')}}" class="form-control" placeholder="{{__('Job Title or Keywords')}}" id="search_desktop" />
                </div>
                <div class="form-group select-single">
                    {!! Form::select('functional_area_id[]', ['' => __('Select Functional Area')]+$functionalAreas, Request::get('functional_area_id', null), array('class'=>'form-control select-multiple-functional-area', 'id'=>'functional_area_id_desktop')) !!}
                </div>
                <div class="form-group">
                    {!! Form::text('salary_from', Request::get('salary_from', null), array('class'=>'form-control', 'id'=>'salary_from_desktop', 'placeholder'=>__('Minimum Salary').' ('.$currency_default.')')) !!}
                </div>
            </div>
            <div class="widget-end">
                <div class="form-group select-single">
                    <?php // $career_level_id = old('career_level_id', null); ?>
                    {!! Form::select('career_level_id', ['' => __('Position Levels')] + $careerLevels, Request::get('career_level_id', null), array('class'=>'form-control select-multiple-career-level', 'id'=>'career_level_id_desktop')) !!}
                </div>
                <div class="form-group select-single">
                    <?php // $companies_id = old('companies_id', null); ?>
                    {!! Form::select('companies_id', ['' => __('Companie Names')] + $companies, Request::get('companies_id', null), array('class'=>'form-control select-multiple-companies', 'id'=>'companies_id_desktop')) !!}
                </div>
            </div>

            <!-- button -->
            <div class="searchnt">
                <button type="submit" class="btn btn-theme"><i class="fa fa-search" aria-hidden="true"></i> {{__('Search Jobs')}}</button>
            </div>
            <!-- button end-->
        </form>
    </div>

</div>

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function ($){
            $('#salary_from_desktop, #salary_from_mobile').priceFormat({prefix:'',thousandsSeparator:',',limit:21,centsLimit:0,clearOnEmpty:true});
            $('.select-multiple-country').select2({
                placeholder: "{{__('Select Country')}}",
                //            allowClear: true
            });
            $('.select-multiple-state').select2({
                placeholder: "{{__('Select State')}}",
                //            allowClear: true
            });
            $('.select-multiple-city').select2({
                placeholder: "{{__('Select City')}}",
                //            allowClear: true
            });
            $('.select-multiple-functional-area').select2({
                placeholder: "{{__('All Specializations')}}",
            });


            $('#country_id_desktop').change(function(){
                value_desktop = $(this).val();
                value_mobile = $('#country_id_mobile').val();
                if(value_desktop != value_mobile){
                    filterStates_desktop(0);
                    currency_desktop();
                    $('#country_id_mobile').val(value_desktop).change();
                }
//                $('#country_id_mobile').val(value).change();
            });
            $('#country_id_mobile').change(function(){
                value_mobile = $(this).val();
                value_desktop = $('#country_id_dektop').val();
                if(value_desktop != value_mobile){
                    filterStates_mobile(0);
                    currency_mobile();
                    $('#country_id_desktop').val(value_mobile).change();
                }
//                $('#country_id_desktop').val(value).change();
            });

            $('#search_mobile').keyup(function(){
                var search = $('#search_mobile').val();
                $('#search_desktop').val(search);
            });

            $('#search_desktop').keyup(function(){
                var search = $('#search_desktop').val();
                $('#search_mobile').val(search);
            });

            $('#salary_from_mobile').keyup(function(){
                var salary = $('#salary_from_mobile').val();
                $('#salary_from_desktop').val(salary);
            });

            $('#salary_from_desktop').keyup(function(){
                var salary = $('#salary_from_desktop').val();
                $('#salary_from_mobile').val(salary);
            });

            $('.select-multiple-salary-currency').select2({
                placeholder: "{{__('Select Salary Currency')}}",
//            allowClear: true
            });
        });
        $('.select-multiple-career-level').select2({
            placeholder: "{{__('Position Levels')}}",
        });
        $('.select-multiple-companies').select2({
            placeholder: "{{__('Company Names')}}",
        });
    </script>
@endpush