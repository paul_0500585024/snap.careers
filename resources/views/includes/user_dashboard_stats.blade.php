<ul class="row profilestat d-flex justify-content-center" style="list-style-type:none; padding:0px !important;">
    <li class="col-xl-3 col-sm-6 col-12">
        <div class="inbox"> <i class="fa fa-eye" aria-hidden="true"></i>
            <h6 class="active-font-standard">{{Auth::user()->num_profile_views}}</h6>
            <strong>{{__('Profile Views')}}</strong> </div>
    </li>
    <li class="col-xl-3 col-sm-6 col-12">
        <div class="inbox"> <i class="fa fa-user-o" aria-hidden="true"></i>
            <h6 class="font-standard"><a href="{{route('my.followings')}}">{{Auth::user()->countFollowings()}}</a></h6>
            <strong>{{__('Followings')}}</strong> </div>
    </li>
    <li class="col-xl-3 col-sm-6 col-12">
        <div class="inbox"> <i class="fa fa-briefcase" aria-hidden="true"></i>
            <h6 class="font-standard"><a href="{{route('my.profile.cv')}}">{{Auth::user()->countProfileCvs()}}</a></h6>
            <strong>{{__('My CV List')}}</strong> </div>
    </li>
    <li class="col-xl-3 col-sm-6 col-12">
        <div class="inbox"> <i class="fa fa-envelope-o" aria-hidden="true"></i>
            <h6 class="font-standard"><a href="{{route('my.messages')}}">{{Auth::user()->countApplicantMessages()}}</a></h6>
            <strong>{{__('Messages')}}</strong> </div>
    </li>
</ul>