<style>
    .box-job-hidden{
        height: 350px;
        overflow: hidden;
    }
    .box-job-container{
        position: relative;

    }
    .box-job-more-less{
        position: absolute;
        left: 50%;
        transform: translate(-50%, -50%);
        color:#142d99;
        cursor: pointer;
    }
    .box-job-top-class{
        top: 300px;
    }
    .box-button-job {
        display:inline-block;
        color:#444;
        border:1px solid #CCC;
        background:#DDD;
        box-shadow: 0 0 5px -1px rgba(0,0,0,0.2);
        cursor:pointer;
        vertical-align:middle;
        max-width: 100px;
        padding: 5px;
        text-align: center;
    }
</style>
@extends('layouts.app')
@section('content')
    <!-- Header start -->
    @include('includes.header')
    <!-- Header end -->
    <!-- Inner Page Title start -->
    <style>
        .select2 {
            width:100%!important;
        }
    </style>
    <?php /*@include('includes.inner_page_title', ['page_title'=>__('Job Listing')])  */ ?>
    <!-- Inner Page Title end -->
    <div class="listpgWraper" style="padding-top:0px !important;">
        <div class="container">
            <div class="row">
                <div class="container">
                    <div class="mt-2">
                        <div class="col text-right">{{ $jobs->firstItem() == '' ? 0 : $jobs->firstItem()}} - {{ $jobs->lastItem() == '' ? 0 : $jobs->lastItem() }} {{__('of')}} {{ $jobs->total() }} {{__('jobs')}}</div>
                    </div>
                    <div class="mt-3">
                    @include('flash::message')
                    <!-- Search Result and sidebar start -->
                        <div class="row">
                            @include('includes.job_list_side_bar')
                            <div class="col">
                                <!-- Search List -->
                                <ul class="searchList">
                                    <!-- job start -->
                                    @if(isset($jobs) && count($jobs))
                                        @foreach($jobs as $job)
                                            @php $company = $job->getCompany(); @endphp
                                            @if(null !== $company)
                                                <li class="box-job-hidden">
                                                    <div class="box-job-container">
                                                        <?php /*                                                <div class="row">
                                                        <div class="col-md-12 col-sm-12">
                                                            <div class="row">
                                                                <div class="col-md-8 col-sm-8">
                                                                    <div class="row">
                                                                        <h3><a href="{{route('job.detail', [$job->slug])}}" title="{{$job->title}}">{{$job->title}}</a></h3>
                                                                        <div class="companyName">
                                                                            <a href="{{route('company.detail', $company->slug)}}" title="{{$company->name}}">{{$company->name}}</a>
                                                                        </div>
                                                                        <div class="location">
                                                                            <label class="fulltime" title="{{$job->getJobType('job_type')}}">{{$job->getJobType('job_type')}}</label>
                                                                            <br><br>
                                                                            <span><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;{!! $job->getJobLocationList() !!}</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4 col-sm-4">
                                                                    <div class="row">
                                                                        {{$company->printCompanyImage()}}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div> */ ?>
                                                        <div id="company_logo">
                                                            <a href="{{route('company.detail', $company->slug)}}" title="{{$company->name}}">
                                                                <div class="pull-right">
                                                                    {{$company->printCompanyImage()}}
                                                                </div>
                                                            </a>
                                                        </div>
                                                        <div id="position-title" class="job-position-title">
                                                            <a class="fnt-theme" href="{{route('job.detail', [$job->slug])}}" title="{{$job->title}}">
                                                                <h3>{{$job->title}}</h3>
                                                            </a>
                                                        </div>
                                                        <div id="company-name" class="companyName">
                                                            <a class="fnt-theme" href="{{route('company.detail', $company->slug)}}" title="{{$company->name}}" style="color:#fed136">{{$company->name}}</a>
                                                        </div>
                                                        <div id="location" class="location ml-3">
                                                            <div><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;{!! $job->getJobLocationListNew() !!}</div>
                                                            @if(!Auth::check())
                                                                @if(Auth::guard('company')->check())
                                                                    @if($job->company_id == (int)Auth::guard('company')->user()->id)
                                                                        <div><i class="fa fa-usd" aria-hidden="true"></i>
                                                                            {{$job->salary_currency}} {{number_format($job->salary_from)}} - {{number_format($job->salary_to)}}
                                                                        </div>
                                                                    @endif
                                                                @else
                                                                    <div><i class="fa fa-usd" aria-hidden="true"></i>
                                                                        <a class="fnt-theme" href="{{route('login')}}">{{ __('Candidate must login to view salary') }}</a>
                                                                    </div>
                                                                @endif
                                                            @else
                                                                <div><i class="fa fa-usd" aria-hidden="true"></i>
                                                                    {{$job->salary_currency}} {{number_format($job->salary_from)}} - {{number_format($job->salary_to)}}
                                                                </div>
                                                            @endif

                                                        </div>
                                                        <br><br>
                                                        @php
                                                            $to = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s'));
                                                            $from = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $job->updated_at);
                                                            $diff = $to->diffInDays($from);
                                                            $difftext = $diff . ' days ago';
                                                            if($diff == 0){
                                                                $diff = $to->diffInHours($from);
                                                                $difftext = $diff . ' hours ago';
                                                            }
                                                        @endphp
                                                        <?php /*                                                <div class="module_posted_job line-clamp">{!! strip_tags($job->description) !!}</div> */ ?>
                                                        <div class="job-posted-description">{!! $job->description !!}</div>
                                                        <span class="posted_date" style="font-size:12px">{{$difftext}}</span>
                                                        <div class="box-job-top-class box-button-job more-less-job"></div>
                                                    </div>
                                                </li>
                                                <!-- job end -->
                                            @endif
                                        @endforeach
                                    @endif
                                </ul>

                                <!-- Pagination Start -->
                                <div class="pagiWrap">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="showreslt">
                                                {{__('Showing Jobs')}} : {{ $jobs->firstItem() }} - {{ $jobs->lastItem() }} {{__('Total')}}
                                            </div>
                                        </div>
                                        <div class="col-md-7 text-right">
                                            @if(isset($jobs) && count($jobs))
                                                {{ $jobs->appends(request()->query())->links() }}
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <!-- Pagination end -->
                                <div class=""><br />{!! $siteSetting->listing_page_horizontal_ad !!}</div>
                                <!-- Search List -->
                                <ul class="searchList">
                                    <div class="row">
                                        <div class="col-md-12">
                                            @include('includes.alligator')
                                        </div>
                                    </div>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('includes.footer')
@endsection
@push('styles')
    <style type="text/css">
        .searchList li .jobimg {
            min-height: 80px;
        }
        .hide_vm_ul{
            height:100px;
            overflow:hidden;
        }
        .hide_vm{
            display:none !important;
        }
        .view_more{
            cursor:pointer;
        }
    </style>
@endpush
@push('scripts')
    <script>
        (function ($) {
            $('#jobSalaryMobile').click(function(){
                $('#jobSalary').modal('show');
                $('#mainNav').hide();
            });
            $('#CloseModalJobSalary').click(function(){
                $('#jobSalary').modal('hide');
                $('#mainNav').show();
            });
            $('#jobLocationMobile').click(function(){
                $('#jobLocation').modal('show');
                $('#mainNav').hide();
            });
            $('#CloseModalJobLocation').click(function(){
                $('#jobLocation').modal('hide');
                $('#mainNav').show();
            });
            $('#jobFilterMobile').click(function(){
                $('#jobFilter').modal('show');
                $('#mainNav').hide();
            });
            $('#CloseModalJobFilter').click(function(){
                $('#jobFilter').modal('hide');
                $('#mainNav').show();
            });
            $( window ).resize(function() {
                if($('#desktop-jobSalary').is(':visible')){
                    $('#jobSalary').modal('hide');
                }
                if ($('#desktop-jobFilterSideBar').is(':visible')) {
                    $('#jobLocation').modal('hide');
                }
                if($('#desktop-jobFilterSideBar').is(':visible')){
                    $('#jobFilter').modal('hide');
                }
            });
        })(jQuery);
        $(document).ready(function ($) {
            $('.country_id').on('change', function (e) {
                e.preventDefault();
                filterStatesclasses(0);
            });

            function filterStatesclasses(state_id)
            {
                var country_id = $('.country_id').val();
                console.log(country_id);
                <?php /*            if (country_id != ''){
                $.post("{{ route('filter.states.dropdown') }}", {country_id: country_id, state_id: state_id, _method: 'POST', _token: '{{ csrf_token() }}'})
                    .done(function (response) {
                        $('#default_state_dd, .default_state_dd').html(response);
                        @php
                            $city_id_array = Request::get('city_id', array(0 => 0));
                        @endphp
                        filterCities({{$city_id_array[0]}});
                        $('.select-multiple-state').select2({
                            placeholder: "{{__('Select State')}}",
//            allowClear: true
                        });
                    });
            } */ ?>
            }


            $("form").submit(function () {
/*                $(this).find(":input").filter(function () {
                    return !this.value;
                }).attr("disabled", "disabled");
                $("#country_id_mobile, #state_id_mobile, #functional_area_id_mobile, #career_level_id_mobile, #companies_id_mobile").prop("disabled", true);
                $("#country_id_desktop, #state_id_desktop, #functional_area_id_desktop, #career_level_id_desktop, #companies_id_desktop").prop("disabled", true); */
                return true;
            });
/*            $("form").find(":input").prop("disabled", false);
            $("#country_id_mobile, #state_id_mobile, #functional_area_id_mobile, #career_level_id_mobile, #companies_id_mobile").prop("disabled", "disabled");
            $("#country_id_desktop, #state_id_desktop, #functional_area_id_desktop, #career_level_id_desktop, #companies_id_desktop").prop("disabled", "disabled"); */

            $(".view_more_ul").each(function () {
                if ($(this).height() > 100)
                {
                    $(this).addClass('hide_vm_ul');
                    $(this).next().removeClass('hide_vm');
                }
            });
            $('.view_more').on('click', function (e) {
                e.preventDefault();
                $(this).prev().removeClass('hide_vm_ul');
                $(this).addClass('hide_vm');
            });

        });
    </script>
    <?php /* @include('includes.country_state_city_js') */ ?>

    <script>
        $(document).ready(function ($) {
<?php /*            $('#country_id_mobile').on('change', function (e) {
                e.preventDefault();
                filterStates_mobile(0);
            });
            $('#country_id_desktop').on('change', function (e) {
                e.preventDefault();
                filterStates_desktop(0);
            }); */ ?>
            @php
                $state_id_array = Request::get('state_id', array(0 => 0));
            @endphp
            filterStates_mobile_desktop({{$state_id_array[0]}});
        });
        function currency_desktop()
        {
            var country_id = $('#country_id_desktop').val();
            if (country_id != ''){
                $.post("{{ route('filter.currency') }}", {country_id: country_id, _method: 'POST', _token: '{{ csrf_token() }}'})
                    .done(function (response) {
                        $('#salary_from_desktop').attr('placeholder','Minimum Salary ('+response+')');
                    });
            }
        }
        function currency_mobile()
        {
            var country_id = $('#country_id_mobile').val();
            console.log(country_id);
            if (country_id != ''){
                $.post("{{ route('filter.currency') }}", {country_id: country_id, _method: 'POST', _token: '{{ csrf_token() }}'})
                    .done(function (response) {
                        $('#salary_from_mobile').attr('placeholder','Minimum Salary ('+response+')');
                    });
            }
        }
        function filterStates_mobile_desktop(state_id)
        {
            var country_id_mobile = $('#country_id_mobile').val();
            var country_id_desktop = $('#country_id_desktop').val();
            var country_id = '';
            if(country_id_mobile != ''){
                country_id = country_id_mobile;
            }else if (country_id_desktop != ''){
                country_id = country_id_desktop;
            }else{
                country_id = '';
            }
            if (country_id != ''){
                $.post("{{ route('filter.states.dropdown') }}", {country_id: country_id, state_id: state_id, _method: 'POST', _token: '{{ csrf_token() }}'})
                    .done(function (response) {
                        $('#default_state_dd, .default_state_dd').html(response);
                        @php
                            $city_id_array = Request::get('city_id', array(0 => 0));
                        @endphp
                        $('.select-multiple-state').select2({
                            placeholder: "{{__('Select State')}}",
                            //            allowClear: true
                        });
                    });
            }
        }
        function filterStates_mobile(state_id) {
            var country_id = $('#country_id_mobile').val();
            if (country_id != ''){
                $.post("{{ route('filter.states.dropdown') }}", {country_id: country_id, state_id: state_id, _method: 'POST', _token: '{{ csrf_token() }}'})
                    .done(function (response) {
                        $('#default_state_dd, .default_state_dd').html(response);
                        @php
                            $city_id_array = Request::get('city_id', array(0 => 0));
                        @endphp
                        $('.select-multiple-state').select2({
                            placeholder: "{{__('Select State')}}",
                            //            allowClear: true
                        });
                    });
            }
        }
        function filterStates_desktop(state_id) {
            console.log('desktop');
            var country_id = $('#country_id_desktop').val();
            if (country_id != ''){
                $.post("{{ route('filter.states.dropdown') }}", {country_id: country_id, state_id: state_id, _method: 'POST', _token: '{{ csrf_token() }}'})
                    .done(function (response) {
                        $('#default_state_dd, .default_state_dd').html(response);
                        @php
                            $city_id_array = Request::get('city_id', array(0 => 0));
                        @endphp
                        $('.select-multiple-state').select2({
                            placeholder: "{{__('Select State')}}",
                            //            allowClear: true
                        });
                    });
            }
        }
        $(document).ready(function(){
            $(document).on('click', '.box-job-more-less', function(e) {
                if($(this).data('val') == 'More'){
                    $(this).data('val','Less');
                    $(this).parent().parent().removeClass('box-job-hidden');
                    $(this).html('Less');
                    $(this).removeClass('box-job-top-class');
                }else{
                    $(this).data('val','More');
                    $(this).parent().parent().addClass('box-job-hidden');
                    $(this).html('More');
                    $(this).addClass('box-job-top-class');
                }
            });
            $('.more-less-job').each(function(index){
                if($(this).parent().find('div.job-posted-description').outerHeight()>96){
                    $(this).addClass('box-job-more-less');
                    $(this).html('More');
                    $(this).data('val','More');
                }
            });

        });
    </script>
@endpush