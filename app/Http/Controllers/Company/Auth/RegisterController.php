<?php

namespace App\Http\Controllers\Company\Auth;

use Auth;
use App\User;
use App\Company;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Jrean\UserVerification\Traits\VerifiesUsers;
use Jrean\UserVerification\Facades\UserVerification;
use App\Http\Requests\Front\CompanyFrontRegisterFormRequest;
use Illuminate\Auth\Events\Registered;
use App\Events\CompanyRegistered;
use Carbon\Carbon;

use Jrean\UserVerification\Exceptions\UserNotFoundException;
use Jrean\UserVerification\Exceptions\UserIsVerifiedException;
use Jrean\UserVerification\Exceptions\TokenMismatchException;

class RegisterController extends Controller
{
    /*
      |--------------------------------------------------------------------------
      | Register Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles the registration of new users as well as their
      | validation and creation. By default this controller uses a trait to
      | provide this functionality without requiring any additional code.
      |
     */

    use RegistersUsers;
    use VerifiesUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/company-home';
    protected $userTable = 'companies';
    protected $redirectIfVerified = '/company-home';
    protected $redirectAfterVerification = '/company-home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('company.guest', ['except' => ['getVerification', 'getVerificationError']]);
    }

    public function getVerification(Request $request, $token)
    {
        if (! $this->validateRequest($request)) {
            return redirect($this->redirectIfVerificationFails());
        }
        $company = Company::select('verified')
            ->where('email', '=', $request->input('email'))
            ->first();
        if(!empty($company->verified) && $company->verified == 1){
            return redirect($this->redirectIfVerificationFails());
        }

        try {
            $company = UserVerification::process($request->input('email'), $token, $this->userTable());
        } catch (UserNotFoundException $e) {
            return redirect($this->redirectIfVerificationFails());
        } catch (UserIsVerifiedException $e) {
            return redirect($this->redirectIfVerified());
        } catch (TokenMismatchException $e) {
            return redirect($this->redirectIfVerificationFails());
        } catch (\Exception $e){
            return redirect($this->redirectIfVerificationFails());
        }


        if (config('user-verification.auto-login') === true) {
            auth()->loginUsingId($company->id);
        }

        return redirect($this->redirectAfterVerification());
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('company');
    }

    public function register(CompanyFrontRegisterFormRequest $request)
    {

        if(User::select('email')->where('email', '=', $request->input('email'))->where('verified', '=', 1)->where('is_active','=','1')->exists()){
            return redirect()
                ->back()
                ->withInput()
                ->withErrors(['email'=>'Email has already been registered for candidate.']);
        }

/*        if(Company::select('email')->where('email', '=', $request->input('email'))->where('verified', '=', 1)->where('is_active','=','0')->exists()){
            $authUser = Company::where('email', $request->input('email'))->first();
            $authUser->is_active = 1;
            $authUser->updated_at = Carbon::now();
            $authUser->name = $request->input('name');
            $authUser->password = bcrypt($request->input('password'));

            $authUser->save();

            Auth::guard('company')->login($authUser, true);
            return redirect('/company-home');
        }

        $id_str_rule = '';
        $email_rule = config('rules.email_rule');
                array_push($email,
                    'unique:companies,email' . $id_str_rule
                );
        Validator::make($request->all(), [
            'email' => $email_rule,
        ])->validate(); */

        $company = new Company();
        $company->name = $request->input('name');
        $company->email = $request->input('email');
        $company->password = bcrypt($request->input('password'));
        $company->is_active = 1;
        $company->verified = 1;
//        $company->longitude = 0.000000;
//        $company->latitude = 0.000000;
        $company->is_top_employer=0;
        $company->save();
        /*         * ******************** */
        $company->slug = str_slug($company->name, '-') . '-' . $company->id;
        $company->update();
        /*         * ******************** */

        event(new Registered($company));
        event(new CompanyRegistered($company));
        $this->guard()->login($company);
        UserVerification::generate($company);
        UserVerification::send($company, 'Company Verification', config('mail.recieve_to.address'), config('mail.recieve_to.name'));
        return $this->registered($request, $company) ?: redirect($this->redirectPath());
    }

}
