<div class="modal-body">
    <div class="form-body">
         @if(isset($profileProject))
        <div class="formrow">
            {{ImgUploader::print_image("project_images/thumb/$profileProject->image")}}
        </div>
        @endif
         <div class="formrow" id="div_name">
            <label class="form-lable"><a target="_blank" href="{{(isset($profileProject)? $profileProject->url:'')}}">View Portfolio</a></label>
            </div>
        <div class="formrow" id="div_name">
            <label class="form-lable">Project Name : </label>
            <label class="form-lable">{{(isset($profileProject)? $profileProject->name:'')}}</label>
            </div>   
        <div class="formrow" id="div_image">
            <div class="uploadphotobx dropzone needsclick dz-clickable"  id="dropzone"> 
            </div>
            <span class="help-block image-error"></span> </div>
        <div class="formrow" id="div_url">
            <label class="form-lable">Project URL : </label>
            <label class="form-lable">{{(isset($profileProject)? $profileProject->url:'')}}</label>
              </div>
        <div class="formrow" id="div_date_start">
            <label class="form-lable">Project Start Date : </label>
             @php
                if(isset($profileProject)){
                $originalDate = $profileProject->date_start;
  $startDate = date("Y-m-d", strtotime($originalDate));
} 
                    @endphp 
            <label class="form-lable">{{(isset($profileProject)? $startDate:'')}}</label>
            </div>
        <div class="formrow" id="div_date_end">
            <label class="form-lable">Project End Date : </label>
              @php
                if(isset($profileProject)){
                $originalDate = $profileProject->date_end;
  $endDate = date("Y-m-d", strtotime($originalDate));
} 
                    @endphp 
                    <label class="form-lable">{{(isset($profileProject)? $endDate:'')}}</label>
           </div>  

        <div class="formrow" id="div_is_on_going">
            <label for="is_on_going" class="bold">{{__('Is Currently Ongoing?')}}</label>
            <label for="is_on_going" class="bold"><?php
                if (isset($profileProject) && $profileProject->is_on_going == 1) {
                   echo "Yes";
                }else{
                    echo "No";
                }
                ?></label>
            <div class="radio-list">
            </div>
            <span class="help-block is_on_going-error"></span>
        </div>

        <div class="formrow" id="div_description">
            <label class="form-lable">Project Description : </label>
            
       <label class="form-lable">{{(isset($profileProject)? $profileProject->description:'')}}</label>
     
            
            </div>
    </div>
</div>