<section class="py-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <p class="section-subheading text-muted"><b>Over 1 million jobs</b></p>
                <h2 class="section-heading">Top Employers</h2>
            </div>
        </div>
        <div class="container">
            @if(isset($topCompanyIds) && count($topCompanyIds))
                @php $numOfCols = 5; $rowCount = 0; $close_tag = true @endphp
                @foreach($topCompanyIds as $company_id_num_jobs)
                    @php $company = App\Company::where('id', '=', $company_id_num_jobs->company_id)->active()->first(); @endphp
                    @if (null !== $company)
                        @php $close_tag = true; @endphp
                        @if($rowCount % $numOfCols == 0)
                        <div class="row">
                        @endif
                            <div class="col mb-5 fnt-theme font-weight-bold">
                                <center>
                                <a href="{{route('company.detail', $company->slug)}}">
                                    <img class="img-fluid d-block mx-auto img-responsive col-md-12 rounded-circle" src="{{$company->printCompanyImageSrc()}}" alt="{{$company->name}}">
                                <div class="col-md-12 mt-2">
                                    {{$company->name}}
                                </div>
                                </a>
                                </center>
                            </div>
                        @if(($rowCount+1) % $numOfCols == 0)
                        @php $close_tag = false; @endphp
                        </div>
                        @endif
                        @php $rowCount++ @endphp
                    @endif
                @endforeach
                @if($close_tag == true)
                    @while($rowCount % $numOfCols != 0)
                        <div class="col mb-5 fnt-theme font-weight-bold"></div>
                        @php $rowCount++; @endphp
                    @endwhile
                    </div>
                @endif
            @endif
        </div>
        <div class="row">
            <div class="col-lg-10 mx-auto text-center">
                <p class="large text-muted p-5">{!! $siteSetting->index_page_below_top_employes_ad !!}</p>
            </div>
        </div>
        <?php /*
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading row">
                            <div class="col-lg-4">
                                <h3 class="panel-title">Browse Jobs</h3>
                            </div>
                            <div class="col-lg-8">
                                <!-- Tabs -->
                                <ul class="nav panel-tabs">
                                    <li><a href="#tab1" data-toggle="tab" class="text-dark"><img src="{{asset('/')}}img_new/icon1-ii.png" class="p-2 tab-icon">Browse by Functional Area</a></li>
                                    <li><a href="#tab2" data-toggle="tab" class="text-dark"><img src="{{asset('/')}}img_new/icon2.png" class="p-2 tab-icon">Browse by Cities</a></li>
                                    <li><a href="#tab3" data-toggle="tab" class="text-dark"><img src="{{asset('/')}}img_new/icon3.png" class="p-2 tab-icon">Browse by Industries</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab1">
                                    <div class="row">
                                        @if(isset($topFunctionalAreaIds) && count($topFunctionalAreaIds))
                                            @foreach($topFunctionalAreaIds as $functional_area_id_num_jobs)
                                            <?php
                                            $functionalArea = App\ FunctionalArea::where('functional_area_id', '=', $functional_area_id_num_jobs->functional_area_id)->lang()->active()->first();
                                            ?>
                                            @if(null !== $functionalArea)
                                                <div class="col-lg-3 text-center"><div class="circle-job-bg"><span class="centered-element">
                                                            <h4 class="pt-4">{{$functionalArea->functional_area}}</h4><p class="orange-sm">{{$functional_area_id_num_jobs->num_jobs}} Position</p></span></div></div>
                                        @endif @endforeach @endif
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab2">
                                    <div class="row">
                                        @if(isset($topCityIds) && count($topCityIds))
                                            @foreach($topCityIds as $city_id_num_jobs)
                                            <?php
                                            $city = App\ City::getCityById($city_id_num_jobs->city_id);
                                            ?>
                                            @if(null !== $city)
                                                <div class="col-lg-3 text-center"><div class="circle-job-bg"><span class="centered-element">
                                                            <h4 class="pt-3">{{$city->city}}</h4><p class="orange-sm">{{$city_id_num_jobs->num_jobs}} Position</p></span></div></div>
                                            @endif @endforeach @endif
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab3">
                                    <div class="row">
                                        @if(isset($topIndustryIds) && count($topIndustryIds)) @foreach($topIndustryIds as $industry_id => $num_jobs)
                                            <?php

                                            $industry = App\ Industry::where('industry_id', '=', $topIndustryIds[$industry_id]['industry_id'])->lang()->active()->first();
                                            ?>
                                            @if(null !== $industry)
                                                    <div class="col-lg-3 text-center"><div class="circle-job-bg"><span class="centered-element">
                                                                <h4 class="pt-3">{{$industry->industry}}</h4><p class="orange-sm">{{$topIndustryIds[$industry_id]['num_jobs']}} Position</p></span></div></div>
                                            @endif @endforeach @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> */ ?>
    </div>
</section>

