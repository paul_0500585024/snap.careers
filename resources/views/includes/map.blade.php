  @php
  function drow_map($lat='' , $long='', $name='' , $address='' , $country='' , $state='' , $city='')
  {
  	@endphp
  	  <style>          
          #map { 
            height: 300px;    
            width: 100%;            
          }          
        </style>        
    </head>    
    <body>        
        <div style="padding:10px">
            <div id="map"></div>
        </div> 
        <script type="text/javascript">
        var map; 
        function initMap() {                            
            var latitude = {{$lat}}; // YOUR LATITUDE VALUE
            var longitude = {{$long}}; // YOUR LONGITUDE VALUE 
            var myLatLng = {lat: latitude, lng: longitude}; 
            var infowindow = new google.maps.InfoWindow();
            map = new google.maps.Map(document.getElementById('map'), {
              center: myLatLng,
              zoom: 14                    
            });
            var marker = new google.maps.Marker({
              position: myLatLng,
              map: map
            }); 
             google.maps.event.addListener(marker, 'click', function() {
              infowindow.setContent('<div><strong>{{$name}} </strong><br>' +
                'City: {{$city}}<br>State: {{$state}}<br>Country: {{$country}}<br>Address {{$address}}</div>');
              infowindow.open(map, this);
            });            
        }
        </script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA1WFxIuSK08ewI1YkwJcXqL6MTkORoO-Q&callback=initMap"
        async defer></script>

        @php
  }
  @endphp

 {{--  infowindow.setContent('<div><strong><b>{{$name}}</b> </strong><br>' +
                'City: {{$city}}<br>City: {{$city}}<br>Address {{$address}}</div>');
                'City: {{$city}}<br>Country: {{$country}}<br>Address {{$address}}</div>'); --}}
  