<?php
if (!isset($seo)) {
    $seo = (object) array('seo_title' => $siteSetting->site_name, 'seo_description' => $siteSetting->site_name, 'seo_keywords' => $siteSetting->site_name, 'seo_other' => '');
}
?>
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" class="{{ (session('localeDir', 'ltr'))}}" dir="{{ (session('localeDir', 'ltr'))}}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>{{ htmlentities(__($seo->seo_title)) }}</title>
        <meta name="Description" content="{!! htmlentities($seo->seo_description) !!}">
        <meta name="Keywords" content="{!! htmlentities($seo->seo_keywords) !!}">
        <meta name="author" content="">
        {!! $seo->seo_other !!}
        <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Lato" />
        <!-- Fav Icon -->
        <link href="{{asset('/')}}build/css/intlTelInput.css" rel="stylesheet">


        <link rel="shortcut icon" href="{{asset('/')}}favicon.ico">
        <!-- Slider -->
        <link href="{{asset('/')}}js/revolution-slider/css/settings.css" rel="stylesheet">
        <!-- Owl carousel -->
        <link href="{{asset('/')}}css/owl.carousel.css" rel="stylesheet">
        <!-- Bootstrap core CSS -->
        <link href="{{asset('/')}}vendor_new/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom fonts for this template -->
        <link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">
        <!-- Font Awesome -->
<?php /*        <link href="{{asset('/')}}css_new/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous"> */ ?>
        <link href="{{asset('/')}}css_new/font-awesome.min.css" rel="stylesheet">
        <!-- Custom Style -->
        <link href="{{asset('/')}}css_new/careerv1-1-0.css?v={{time()}}" rel="stylesheet">
        @if((session('localeDir', 'ltr') == 'rtl'))
        <!-- Rtl Style -->
        <link href="{{asset('/')}}css/rtl-style.css" rel="stylesheet">
        @endif
        <link href="{{ asset('/') }}admin_assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ asset('/') }}admin_assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ asset('/') }}admin_assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
         <link href="{{ asset('/') }}admin_assets/custom.css?v={{time()}}" rel="stylesheet" type="text/css" />
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="{{asset('/')}}js/html5shiv.min.js"></script>
          <script src="{{asset('/')}}js/respond.min.js"></script>
        <![endif]-->
        @stack('styles')
        <script src="{{asset('/')}}js_new/jquery.min.js"></script>
    </head>
    <body id="page-top">
        @yield('content')
        <!-- Bootstrap's JavaScript -->
        <script src="{{asset('/')}}vendor_new/jquery/jquery.min.js"></script>
        <script src="{{asset('/')}}vendor_new/bootstrap/js/bootstrap.bundle.js"></script>
        <script src="{{asset('/')}}js/bootstrap-show-password.js"></script>
        <!-- Owl carousel -->
        <script src="{{asset('/')}}js/owl.carousel.js"></script>
        <script src="{{ asset('/') }}admin_assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="{{ asset('/') }}admin_assets/global/plugins/Bootstrap-3-Typeahead/bootstrap3-typeahead.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <script src="{{ asset('/') }}admin_assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
        <script src="{{ asset('/') }}admin_assets/global/plugins/jquery.scrollTo.min.js" type="text/javascript"></script>
        <script src="{{ asset('/') }}admin_assets/global/plugins/jquery-price-format/jquery.priceformat.min.js" type="text/javascript"></script>
        <!-- Revolution Slider -->
        <script type="text/javascript" src="{{ asset('/') }}js/revolution-slider/js/jquery.themepunch.tools.min.js"></script>
        <script type="text/javascript" src="{{ asset('/') }}build/js/intlTelInput.js"></script>
        <script type="text/javascript" src="{{ asset('/') }}js/revolution-slider/js/jquery.themepunch.revolution.min.js"></script>
        {!! NoCaptcha::renderJs() !!}
        @stack('scripts')
        <!-- Custom js -->
        <script type="text/JavaScript">
            $(document).ready(function(){
            $(document).scrollTo('.has-error', 2000);
            });
            function showProcessingForm(btn_id){
            $("#"+btn_id).val( 'Processing .....' );
            $("#"+btn_id).attr('disabled','disabled');
            }
        </script>
        <!-- Plugin JavaScript -->
        <script src="{{asset('/')}}vendor_new/jquery-easing/jquery.easing.min.js"></script>
        <!-- Custom scripts for this template -->
		@if( Request::segment(1) == '')
        <script src="{{asset('/')}}js_new/snap-home.min.js"></script>
		@else
        <script src="{{asset('/')}}js_new/snap-others.min.js"></script>
		@endif
		<script src="{{asset('/')}}js_new/snap.min.js"></script>
    </body>
</html>
