<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProfileSkillFormRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
            case 'POST': {
    //                    $id = (int) $this->input('id', 0);

    //            $name = config('rules.alnum150_rule');

                //job_skill
                $job_skill_id = config('rules.array_rule');
                //job_skill_star
                $job_skill_id_star = config('rules.array_alnum100_star_rule');

                //job_experience
                $job_experience_id = config('rules.array_rule');
                //job_skills_star
                $job_experience_id_star = config('rules.array_integer_star_rule');

                return ([
    //                    "name" => $name,
                    "job_skill_id" => $job_skill_id,
                    "job_skill_id.*" => $job_skill_id_star,
                    "job_experience_id" => $job_experience_id,
                    "job_experience_id.*" => $job_experience_id_star,

    //                        "job_skill_id" => "required|integer",
    //                        "job_experience_id" => "required|integer",
                ]);
            }
            default:break;
        }
    }

    public function messages()
    {
        return [
/*            'job_skill_id.required' => 'Please select skill.',
            'job_experience_id.required' => 'Please select experience.',*/
        ];
    }

}
