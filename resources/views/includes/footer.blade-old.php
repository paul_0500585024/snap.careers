<!--Footer-->
<div class="largebanner shadow3">
<div class="adin">
{!! $siteSetting->above_footer_ad !!}
</div>
<div class="clearfix"></div>
</div>

<style>
    .select-single .select2-selection__rendered {
        line-height: 46px !important;
    }
    .select-single .select2-container .select2-selection--single {
        height: 50px !important;
    }
    .select-single .select2-selection__arrow {
        height: 49px !important;
    }
</style>
<div class="footerWrap"> 
    <div class="container">
        <div class="row"> 

            <!--Quick Links-->
            <div class="col-md-3 col-sm-6">
                <h5>{{__('Quick Links')}}</h5>
                <!--Quick Links menu Start-->
                <ul class="quicklinks">
                    <li><a href="{{ route('index') }}">{{__('Home')}}</a></li>
                    <li><a href="{{ route('contact.us') }}">{{__('Contact Us')}}</a></li>
                    <li class="postad"><a href="{{ route('post.job') }}">{{__('Post a Job')}}</a></li>
                    <li><a href="{{ route('faq') }}">{{__('FAQs')}}</a></li>
                    @foreach($show_in_footer_menu as $footer_menu)
                    @php
                    $cmsContent = App\CmsContent::getContentBySlug($footer_menu->page_slug);
                    @endphp

                    <li class="{{ Request::url() == route('cms', $footer_menu->page_slug) ? 'active' : '' }}"><a href="{{ route('cms', $footer_menu->page_slug) }}">{{ $cmsContent->page_title }}</a></li>
                    @endforeach
                </ul>
            </div>
            <!--Quick Links menu end-->

            <div class="col-md-3 col-sm-6 select-single">
                @php
                    $functionalAreas = App\FunctionalArea::getUsingFunctionalAreas(10);
                    @endphp
                <h5>{{__('Jobs By Functional Area')}}</h5>
                <!--Quick Links menu Start-->
                 {!! Form::open(array('method' => 'get', 'id' => 'myForm', 'route' => ['job.list'])) !!} 
                <select name="functional_area_id[]" class="form-control select-multiple-functional-area">
                     @foreach($functionalAreas as $functionalArea) 
                    <option value="{{$functionalArea->functional_area_id}}">
                    {{$functionalArea->functional_area}}
                </option>
                @endforeach
            </select>
            <br><br>
            <input type="submit" name="submit" value="Search Job" class="btn btn-primary">
            </form>
                {{-- <ul class="quicklinks">
                    
                    @foreach($functionalAreas as $functionalArea)
                     <li><a href="{{ route('job.list', ['functional_area_id[]'=>$functionalArea->functional_area_id]) }}">{{$functionalArea->functional_area}}</a></li> 
                    @endforeach
                </ul> --}}
            </div>

            <!--Jobs By Industry-->
            <div class="col-md-3 col-sm-6 select-single">
                <h5>{{__('Jobs By Industry')}}</h5>
                @php
                    $industries = App\Industry::getUsingIndustries(10);
                    @endphp
                <!--Industry menu Start-->
                {!! Form::open(array('method' => 'get', 'route' => ['job.list'])) !!} 
                <select name="industry_id[]" class="form-control select-multiple-industry">
                     @foreach($industries as $industry) 
                    <option value="{{$industry->industry_id}}">
                    {{$industry->industry}}
                </option>
                @endforeach
            </select><br><br>
            <input type="submit" name="submit"  value="Search Job" class="btn btn-primary">
               {{--  <ul class="quicklinks"> 
                    @foreach($industries as $industry)
                    <li><a href="{{ route('job.list', ['industry_id[]'=>$industry->industry_id]) }}">{{$industry->industry}}</a></li>
                    @endforeach
                </ul> --}}
                <!--Industry menu End-->
                <div class="clear"></div>
            </div>

            <!--About Us-->
            <div class="col-md-3 col-sm-12">
                <h5>{{__('Contact Us')}}</h5>
                <div class="address">{{ $siteSetting->site_street_address }}</div>
                <div class="email"> <a href="mailto:{{ $siteSetting->mail_to_address }}">{{ $siteSetting->mail_to_address }}</a> </div>
                <div class="phone"> <a href="tel:{{ $siteSetting->site_phone_primary }}">{{ $siteSetting->site_phone_primary }}</a></div>
                <!-- Social Icons -->
                <div class="social">@include('includes.footer_social')</div>
                <!-- Social Icons end --> 

            </div>
            <!--About us End--> 


        </div>
    </div>
</div>
<!--Footer end--> 
<!--Copyright-->
<div class="copyright">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="bttxt">{{__('Copyright')}} &copy; {{date('Y')}} {{ $siteSetting->site_name }}. {{__('All Rights Reserved')}}.</div>
            </div>
            <div class="col-md-4">
                <div class="paylogos"><img src="{{asset('/')}}images/payment-icons.png" alt="" /></div>	
            </div>
        </div>

    </div>
</div>
