<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StateFormRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
            case 'POST': {
                    $id = (int) $this->input('id', 0);
                    $state_unique = '';
                    if ($id > 0) {
                        $state_unique = ',id,' . $id;
                    }

            //country_id
            $country_id = config('rules.id_rule');
            //state
            $state = config('rules.alnum50_rule');
            //code
            $code = config('rules.alnum2_rule');
            //state_id
/*            $state_id = array();
            $is_default = (int) $this->input('is_default', 0);
            if ($is_default == 0) {
                $state_id = config('rules.id_rule');
            }*/
            //is_active
            $is_active = config('rules.bool_rule');
            //is_default
//            $is_default = config('rules.bool_rule');
            //lang
            $lang = config('rules.lang_rule');

            $id = array();

                    return [
                        "id" => $id,
                        "country_id" => $country_id,
                        'code' => $code,
                        "state" => $state,
//                        "is_default" => $is_default,
//                        "state_id" => $state_id,
                        "is_active" => $is_active,
                        "lang" => $lang,

//                        "id" => "",
//                        "lang" => "required|max:10",
//                        "country_id" => "required|integer",
//                        'code' => 'required|max:3',
//                        "state" => "required|max:40",
//                        "is_default" => "required|boolean",
//                        "state_id" => "required_if:is_default,0",
//                        "is_active" => "required|boolean",
                    ];
                }
            default:break;
        }
    }

    public function messages()
    {
        return [
//            'lang.required' => 'Please select language.', 'country_id.required' => 'Please select Country.', 'state.required' => 'Please enter State Name.', 'is_default.required' => 'Is this state default/fallback ?.', 'state_id.required_if' => 'Please select default/fallback State.', 'is_active.required' => 'Is this State active?',
        ];
    }

}
