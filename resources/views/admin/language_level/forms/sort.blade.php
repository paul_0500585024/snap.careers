{!! APFrmErrHelp::showErrorsNotice($errors) !!}
@include('flash::message')
<div class="form-body">
    <h3>Drag and Drop to Sort Language Levels</h3>
    {!! Form::select('lang', ['' => __('Select Language')]+$languages, 'en', array('class'=>'form-control select-multiple-language','style' => 'width:100%', 'id'=>'lang', 'onchange'=>'refreshLanguageLevelSortData();')) !!}
    <div id="languageLevelSortDataDiv"></div>
</div>
@push('scripts') 
<script>
    $(document).ready(function () {
        refreshLanguageLevelSortData();
        $('.select-multiple-language').select2({
            placeholder: "{{__('Select Language')}}",
//            allowClear: true
        });
    });
    function refreshLanguageLevelSortData() {
        var language = $('#lang').val();
        $.ajax({
            type: "GET",
            url: "{{ route('language.level.sort.data') }}",
            data: {lang: language},
            success: function (responseData) {
                $("#languageLevelSortDataDiv").html('');
                $("#languageLevelSortDataDiv").html(responseData);
                /**************************/
                $('#sortable').sortable({
                    update: function (event, ui) {
                        var languageLevelOrder = $(this).sortable('toArray').toString();
                        $.post("{{ route('language.level.sort.update') }}", {languageLevelOrder: languageLevelOrder, _method: 'PUT', _token: '{{ csrf_token() }}'})
                    }
                });
                $("#sortable").disableSelection();
                /***************************/
            }
        });
    }
</script> 
@endpush