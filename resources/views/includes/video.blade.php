<!-- Team -->
<style>
    .videowraper {
        background: #233361;
    }

    .videowraper .titleTop{text-align: left; margin-top: 40px;}

    .videowraper .subtitle, .videowraper h3 {
        color: #fff;
    }
    .videowraper p {
        color: #fff;
        max-width: 800px;
        margin: 0 auto;
        font-size: 18px;
        line-height: 24px;
    }

    .videowraper .embed-responsive{
        -webkit-box-shadow: 0 10px 15px 0 rgba(38, 48, 77, 0.14);
        box-shadow: 0 10px 15px 0 rgba(38, 48, 77, 0.14);
        border: 7px solid #fff;
        border-radius: 3px;
    }
/*    iframe {
        height:calc(100vh - 4px);
        width:calc(100vw - 4px);
        box-sizing: border-box;
    }*/
</style>
@if(isset($video))
<section class="page-section" id="team">
    <div class="container">
        <div class="row">
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="{{$video->video_link}}" frameborder="0" allowfullscreen></iframe>
            </div>
            <div class="col-lg-12 text-center">
                <h2 class="section-heading text-dark pt-5">Watch Our Video</h2>
                <h3 class="section-subheading text-muted">
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-8 pb-5">
                            Snap Career is a global online employment solution for people seeking jobs and the employers who need great people.
                            We are changing the way people think about work, and we're helping them actively improve their lives and their workforce performance with new technology, tools and practices
                            We create and deliver the best recruiting media, technologies and platforms for connecting jobs and people; we strive every day to help our customers hire and help people find jobs.
                        </div>
                        <div class="col-lg-2"></div>
                    </div>
                </h3>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 p-5"></div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="team-member">
                    <h1 class="orange-lg">50,000</h1>
                    <h6 class="text-muted">Jobs available</h6>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="team-member">
                    <h1 class="orange-lg">Over 100</h1>
                    <h6 class="text-muted">Companies onboard</h6>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="team-member">
                    <h6 class="text-muted mb-0">Snap Academy is</h6>
                    <h1 class="orange-lg">Hiring</h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 mx-auto text-center">
                <p class="large text-muted"><button class="btn btn-large btn-theme" onclick="window.location='{{route('register')}}'">Sign up now</button></p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 mx-auto text-center">
                <p class="large text-muted">{!! $siteSetting->above_footer_ad !!}</p>
            </div>
        </div>
    </div>
</section>
@endif