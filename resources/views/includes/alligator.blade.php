<div class="section">
    <div  style="
    padding-left: 0px;
">
        <div class="topsearchwrap">
			<div class="tabswrap mb-3">
				<div class="row">
					<div class="col-md-12">

                        <div class="alligator list-group list-group-horizontal">
                            <?php
                            $c_or_e = old('candidate_or_employer', 'CareerJet');
                            ?>

                            <a class="btn btn-theme border-radius-0 nav-link {{($c_or_e == 'CareerJet')? 'show active':''}}" data-toggle="tab" href="#CareerJet">{{__('CareerJet')}}</a>
                            <a class="btn btn-theme border-radius-0 nav-link {{($c_or_e == 'Indeed')? 'show active':''}}" data-toggle="tab" href="#Indeed">{{__('Indeed')}}</a>
<?php /*                            <a class="btn btn-theme border-radius-0" data-toggle="tab" href="#CareerBuilder" class="list-group-item list-group-action">{{__('CareerBuilder')}}</a> */ ?>
                            <a class="btn btn-theme border-radius-0 nav-link {{($c_or_e == 'Monster')? 'show active':''}}" data-toggle="tab" href="#Monster">{{__('Monster')}}</a>
                        </div>

<?php /*						<ul class="nav nav-tabs" style="float: left">
						  <li class="active" style="
    padding-left: 0px;
    padding-right: 0px;
    padding-top: 0px;
    padding-bottom: 0px;
"><a data-toggle="tab" href="#CareerJet" aria-expanded="true">{{__('CareerJet')}}</a></li>
						  <li class="" style="
    padding-left: 0px;
    padding-right: 0px;
    padding-top: 0px;
    padding-bottom: 0px;
"><a data-toggle="tab" href="#Indeed" aria-expanded="false">{{__('Indeed')}}</a></li>
						  <li class="" style="
    padding-left: 0px;
    padding-right: 0px;
    padding-top: 0px;
    padding-bottom: 0px;
"><a data-toggle="tab" href="#CareerBuilder" aria-expanded="false">{{__('CareerBuilder')}}</a></li>
<li class="" style="
    padding-left: 0px;
    padding-right: 0px;
    padding-top: 0px;
    padding-bottom: 0px;
"><a data-toggle="tab" href="#Monster" aria-expanded="false">{{__('Monster')}}</a></li>
						</ul> */ ?>
					</div>
				</div>
			
			</div>
				
			<div class="tab-content">
				<div id="CareerJet" class="tab-pane fade {{($c_or_e == 'CareerJet')? 'show active':''}}">
					<div class="srchbx">				
                <!--Categories start-->
               
                <div class="srchint"   style="padding-left: 0;">
                    <ul class="row catelist" style="list-style-type:none; padding:0px !important;">
                        @if(isset($careerjet['data']) && count($careerjet['data']))
                        @foreach($careerjet['data'] as $job)
                     <li>
                            <div class="row">
                                <div class="col-md-8 col-sm-8">
                                    
                                    <div class="jobinfo">
                                        <h3><a  target="_blanck" href="{{$job['url']}}" title="{{$job['title']}}">{{$job['title']}}</a></h3>
                                        <div class="companyName"> </div>
                                        <div class="location">
                                            <label class="fulltime" title="Indeed">Career Jet</label>
                                            - <span>{{$job['date']}}</span></div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="listbtn"><a class="btn btn-theme" target="_blanck" href="{{$job['url']}}">View Details</a></div>
                                </div>
                            </div>
                            <p>{{str_limit(strip_tags($job['snippet']), 150, '...')}}</p>
                        </li> 
                         @endforeach
                        @endif
                    </ul>
                    <!--Categories end-->
                </div>
            </div>
				</div>
				<div id="Indeed" class="tab-pane fade {{($c_or_e == 'Indeed')? 'show active':''}}">
					<div class="srchbx">
                <!--Cities start-->
                
                <div class="srchint" style="padding-left: 0;">
                    <ul class="row catelist" style="list-style-type:none; padding:0px !important;">
                          @if(isset($aligator['data']) && count($aligator['data']))
                        @foreach($aligator['data'] as $job)
                     <li>
                            <div class="row">
                                <div class="col-md-8 col-sm-8">
                                    
                                    <div class="jobinfo">
                                        <h3><a  target="_blanck" href="{{$job['url']}}" title="{{$job['title']}}">{{$job['title']}}</a></h3>
                                        <div class="companyName"><a  target="_blanck" href="{{$job['url']}}" title="{{$job['company']}}">{{$job['company']}}</a></div>
                                        <div class="location">
                                            <label class="fulltime" title="Indeed">Indeed</label>
                                            - <span>{{$job['formattedLocation']}}, {{$job['country']}}</span></div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="listbtn"><a class="btn btn-theme" target="_blanck" href="{{$job['url']}}">View Details</a></div>
                                </div>
                            </div>
                            <p>{{str_limit(strip_tags($job['snippet']), 150, '...')}}</p>
                        </li> 
                         @endforeach
                        @endif
                    </ul>
                    <!--Cities end-->
                </div>
            </div>
				</div>
<?php /*				<div id="CareerBuilder" class="tab-pane fade">
					<div class="srchbx">
                <!--Categories start-->
               
                <div class="srchint" style="padding-left: 0;">
                    <ul class="row catelist" style="list-style-type:none; padding:0px !important;">
                        @if(isset($careerbuilder['data']) && count($careerbuilder['data']))
                        @foreach($careerbuilder['data'] as $job)
                     <li>

                            <div class="row"> 
                                <div class="col-md-8 col-sm-8">
                                    
                                    <div class="jobinfo">
                                        <h3><a  target="_blanck" href="{{$job->link}}" title="{{$job->title}}">{{$job->title}}</a></h3>
                                        <div class="companyName"> </div>
                                        <div class="location">
                                            <label class="fulltime" title="Indeed">Career Builder</label>
                                            - <span>{{$job->date}}</span></div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="listbtn"><a class="btn btn-theme" target="_blanck" href="{{$job->link}}">View Details</a></div>
                                </div>
                            </div>
                            <p>{{str_limit(strip_tags($job->desc), 150, '...')}}</p>
                        </li> 
                         @endforeach
                        @endif
                    </ul>
                    <!--Categories end-->
                </div>
            </div>				
				</div> */ ?>
                <div id="Monster" class="tab-pane fade {{($c_or_e == 'Monster')? 'show active':''}}">
                    <div class="srchbx">
                <!--Categories start-->
               
                <div class="srchint" style="padding-left: 0;">
                    <ul class="row catelist" style="list-style-type:none; padding:0px !important;">
                       @if(isset($monster['data']) && count($monster['data']))
                        @foreach($monster['data'] as $job)
                     <li>

                            <div class="row"> 
                                <div class="col-md-8 col-sm-8">
                                    
                                    <div class="jobinfo">
                                        <h3><a  target="_blanck" href="{{$job->link}}" title="{{$job->title}}">{{$job->title}}</a></h3>
                                        <div class="companyName"> </div>
                                        <div class="location">
                                            <label class="fulltime" title="Indeed">Monster</label>
                                            - <span>{{$job->date}}</span></div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="listbtn"><a class="btn btn-theme" target="_blanck" href="{{$job->link}}">View Details</a></div>
                                </div>
                            </div>
                            <p>{{str_limit(strip_tags($job->desc), 150, '...')}}</p>
                        </li> 
                         @endforeach
                        @endif
                    </ul>
                    <!--Categories end-->
                </div>
            </div>              
                </div>
			</div>
			

            

            

            
        </div>
    </div>
</div>