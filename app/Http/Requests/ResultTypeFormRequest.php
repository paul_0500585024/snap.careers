<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ResultTypeFormRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
            case 'POST': {
                    $id = (int) $this->input('id', 0);
                    $result_type_unique = '';
                    if ($id > 0) {
                        $result_type_unique = ',id,' . $id;
                    }
                    //result_type
                    $result_type = config('rules.alnum20_rule');
                    //result_type_id
                    $result_type_id = array();
                    $is_default = (int) $this->input('is_default', 0);
                    if ($is_default == 0) {
                        $result_type_id = config('rules.id_rule');
                    }
                    //is_active
                    $is_active = config('rules.bool_rule');
                    //is_default
                    $is_default = config('rules.bool_rule');
                    //lang
                    $lang = config('rules.lang_rule');

                    $id = array();
                    return [
                        "id" => $id,
                        "result_type" => $result_type,
                        "is_default" => $is_default,
                        "result_type_id" => $result_type_id,
                        "is_active" => $is_active,
                        "lang" => $lang,


//                        "id" => "",
//                        "lang" => "required|max:10",
//                        "result_type" => "required|max:40",
//                        "is_default" => "required|boolean",
//                        "result_type_id" => "required_if:is_default,0",
//                        "is_active" => "required|boolean",
                    ];
                }
            default:break;
        }
    }

    public function messages()
    {
        return [
//            'lang.required' => 'Please select language.', 'result_type.required' => 'Please enter Result Type.', 'is_default.required' => 'Is this Result Type default/fallback ?.', 'result_type_id.required_if' => 'Please select default/fallback Result Type.', 'is_active.required' => 'Is this Result Type active?',
        ];
    }

}
