<ul class="row profilestat d-flex justify-content-center" style="list-style-type:none; padding:0px !important;">
    <li class="col-xl-4 col-sm-4 col-12">
        <div class="inbox"> <i class="fa fa-clock-o" aria-hidden="true"></i>
            <h6><a class="fnt-theme" href="{{route('posted.jobs')}}">{{Auth::guard('company')->user()->countOpenJobs()}}</a></h6>
            <strong>{{__('Open Jobs').' '.'('.__('Not expired').')'}}</strong> </div>
    </li>
    <li class="col-xl-4 col-sm-4 col-12">
        <div class="inbox"> <i class="fa fa-user-o" aria-hidden="true"></i>
            <h6><a class="fnt-theme" href="{{route('company.followers')}}">{{Auth::guard('company')->user()->countFollowers()}}</a></h6>
            <strong>{{__('Followers')}}</strong> </div>
    </li>
    <li class="col-xl-4 col-sm-4 col-12">
        <div class="inbox"> <i class="fa fa-envelope-o" aria-hidden="true"></i>
            <h6><a class="fnt-theme" href="{{route('company.messages')}}">{{Auth::guard('company')->user()->countCompanyMessages()}}</a></h6>
            <strong>{{__('Messages')}}</strong> </div>
    </li>
</ul>