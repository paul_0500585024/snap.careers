@extends('layouts.app')
@section('content') 
<!-- Header start -->
@include('includes.header')
<!-- Header end --> 
<!-- Inner Page Title start --> 
<?php /* @include('includes.inner_page_title', ['page_title'=>__('All Companies')]) */ ?>
<!-- Inner Page Title end -->
<section class="py-4">
    <div class="container">
        <div class="row">
            @if($companies)
                @foreach($companies as $company)
                    <div class="col-md-2 col-sm-6 mt-4 fnt-theme">
                        <a href="{{route('company.detail',$company->slug)}}">
                            <img class="img-fluid d-block mx-auto img-responsive col-md-12 rounded-circle" src="{{$company->printCompanyImageSrc()}}" alt="{{$company->name}}">
                        </a>
                        <div class="col-md-12">
                            <div class="d-flex justify-content-center">
                                <a href="{{route('company.detail',$company->slug)}}">
                                {{$company->name}}
                                </a>
                            </div>
<?php /*                            {{$company->getCompanyLocationList()}}
                            <br>
                            {{__('Current jobs')}} : {{$company->countNumJobs('company_id',$company->id)}} */ ?>
                        </div>
                    </div>
                @endforeach
            @else
            @endif
        </div>
        <div class="d-flex justify-content-center fnt-theme mt-5">{{ $companies->links() }}</div>
    </div>
</section>


@include('includes.footer')
@endsection
@push('styles')
<style type="text/css">
    .formrow iframe {
        height: 78px;
    }
</style>
@endpush
@push('scripts') 
<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('click', '#send_company_message', function () {
            var postData = $('#send-company-message-form').serialize();
            $.ajax({
                type: 'POST',
                url: "{{ route('contact.company.message.send') }}",
                data: postData,
                //dataType: 'json',
                success: function (data)
                {
                    response = JSON.parse(data);
                    var res = response.success;
                    if (res == 'success')
                    {
                        var errorString = '<div role="alert" class="alert alert-success">' + response.message + '</div>';
                        $('#alert_messages').html(errorString);
                        $('#send-company-message-form').hide('slow');
                        $(document).scrollTo('.alert', 2000);
                    } else
                    {
                        var errorString = '<div class="alert alert-danger" role="alert"><ul>';
                        response = JSON.parse(data);
                        $.each(response, function (index, value)
                        {
                            errorString += '<li>' + value + '</li>';
                        });
                        errorString += '</ul></div>';
                        $('#alert_messages').html(errorString);
                        $(document).scrollTo('.alert', 2000);
                    }
                },
            });
        });
    });
</script> 
@endpush