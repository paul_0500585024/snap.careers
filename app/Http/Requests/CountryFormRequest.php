<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CountryFormRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
            case 'POST': {
                    $id = (int) $this->input('id', 0);
                    $country_unique = '';
                    if ($id > 0) {
                        $country_unique = ',id,' . $id;
                    }
                    //country
                    $country = config('rules.alnum100_rule');
                    array_push($country,
                        'unique:countries' . $country_unique
                    );

                    //country_id
/*                    $country_id = array();
                    $is_default = (int) $this->input('is_default', 0);
                    if ($is_default == 0) {
                        $country_id = config('rules.id_rule');
                    }*/
                    //code
                    $code = config('rules.alnum2_rule');
                    //nationality
                    $nationality = config('rules.alnum100_rule');
                    //is_active
                    $is_active = config('rules.bool_rule');
                    //is_default
//                    $is_default = config('rules.bool_rule');
                    //lang
                    $lang = config('rules.lang_rule');

                    return [
//                        'country_id' => $country_id,
                        'country' => $country,
                        'nationality' => $nationality,
                        'code' => $code,
//                        'is_default' => $is_default,
                        'is_active' => $is_active,
                        'lang' => $lang,

//                        'country' => 'required|max:150|unique:countries' . $country_unique,
//                        'country_id' => 'required_if:is_default,0',
//                        'code' => 'required|max:3',
//                        'is_active' => 'required|boolean',
//                        'is_default' => 'required|boolean',
//                        'lang' => 'required|max:10',
                    ];
                }
            default:break;
        }
    }

    public function messages()
    {
        return [
/*            'country.required' => 'Please enter Country.',
            'country_id.required_if' => 'Please select default/fallback Country.',
            'is_default.required' => 'Is this Country default?',
            'is_active.required' => 'Please select status.',
            'lang.required' => 'Please select language.',*/
        ];
    }

}
