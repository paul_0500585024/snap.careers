{!! APFrmErrHelp::showErrorsNotice($errors) !!}
@include('flash::message')
<div class="form-body">        
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'facebook_app_id') !!}">
        {!! Form::label('facebook_app_id', __('Facebook App ID').' '.__('(Candidate)'), ['class' => 'bold']) !!}
        {!! Form::text('facebook_app_id', null, array('class'=>'form-control', 'id'=>'facebook_app_id', 'placeholder'=>__('Facebook App ID').' '.__('(Candidate)'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'facebook_app_id') !!}                                       
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'facebook_app_secret') !!}">
        {!! Form::label('facebook_app_secret', __('Facebook App Secret').' '.__('(Candidate)'), ['class' => 'bold']) !!}
        {!! Form::text('facebook_app_secret', null, array('class'=>'form-control', 'id'=>'facebook_app_secret', 'placeholder'=>__('facebook App Secret').' '.__('(Candidate)'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'facebook_app_secret') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'facebook_company_app_id') !!}">
        {!! Form::label('facebook_company_app_id', __('Facebook App ID').' '.__('(Company)'), ['class' => 'bold']) !!}
        {!! Form::text('facebook_company_app_id', null, array('class'=>'form-control', 'id'=>'facebook_company_app_id', 'placeholder'=>__('Facebook App ID').' '.__('(Company)'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'facebook_company_app_id') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'facebook_company_app_secret') !!}">
        {!! Form::label('facebook_company_app_secret', __('Facebook App Secret').' '.__('(Company)'), ['class' => 'bold']) !!}
        {!! Form::text('facebook_company_app_secret', null, array('class'=>'form-control', 'id'=>'facebook_company_app_secret', 'placeholder'=>__('facebook App Secret').' '.__('(Company)'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'facebook_company_app_secret') !!}
    </div>

    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'twitter_app_id') !!}">
        {!! Form::label('twitter_app_id', __('Twitter App ID').' '.__('(Candidate)'), ['class' => 'bold']) !!}
        {!! Form::text('twitter_app_id', null, array('class'=>'form-control', 'id'=>'twitter_app_id', 'placeholder'=>__('Twitter App ID').' '.__('(Candidate)'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'twitter_app_id') !!}                                       
    </div>    
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'twitter_app_secret') !!}">
        {!! Form::label('twitter_app_secret', __('Twitter App Secret').' '.__('(Candidate)'), ['class' => 'bold']) !!}
        {!! Form::text('twitter_app_secret', null, array('class'=>'form-control', 'id'=>'twitter_app_secret', 'placeholder'=>__('Twitter App Secret').' '.__('(Candidate)'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'twitter_app_secret') !!}                                       
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'twitter_company_app_id') !!}">
        {!! Form::label('twitter_company_app_id', __('Twitter App ID').' '.__('(Company)'), ['class' => 'bold']) !!}
        {!! Form::text('twitter_company_app_id', null, array('class'=>'form-control', 'id'=>'twitter_company_app_id', 'placeholder'=>__('Twitter App ID').' '.__('(Company)'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'twitter_company_app_id') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'twitter_company_app_secret') !!}">
        {!! Form::label('twitter_company_app_secret', __('Twitter App Secret').' '.__('(Company)'), ['class' => 'bold']) !!}
        {!! Form::text('twitter_company_app_secret', null, array('class'=>'form-control', 'id'=>'twitter_company_app_secret', 'placeholder'=>__('Twitter App Secret').' '.__('(Company)'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'twitter_company_app_secret') !!}
    </div>

    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'google_app_id') !!}">
        {!! Form::label('google_app_id', __('Google App ID').' '.__('(Candidate)'), ['class' => 'bold']) !!}
        {!! Form::text('google_app_id', null, array('class'=>'form-control', 'id'=>'google_app_id', 'placeholder'=>__('Google App ID').' '.__('(Candidate)'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'google_app_id') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'google_app_secret') !!}">
        {!! Form::label('google_app_secret', __('Google App Secret').' '.__('(Candidate)'), ['class' => 'bold']) !!}
        {!! Form::text('google_app_secret', null, array('class'=>'form-control', 'id'=>'google_app_secret', 'placeholder'=>__('Google App Secret').' '.__('(Candidate)'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'google_app_secret') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'google_company_app_id') !!}">
        {!! Form::label('google_company_app_id', __('Google App ID').' '.__('(Company)'), ['class' => 'bold']) !!}
        {!! Form::text('google_company_app_id', null, array('class'=>'form-control', 'id'=>'google_company_app_id', 'placeholder'=>__('Google App ID').' '.__('(Company)'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'google_company_app_id') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'google_company_app_secret') !!}">
        {!! Form::label('google_company_app_secret', __('Google App Secret').' '.__('(Company)'), ['class' => 'bold']) !!}
        {!! Form::text('google_company_app_secret', null, array('class'=>'form-control', 'id'=>'google_company_app_secret', 'placeholder'=>__('Google App Secret').' '.__('(Company)'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'google_company_app_secret') !!}
    </div>


</div>
