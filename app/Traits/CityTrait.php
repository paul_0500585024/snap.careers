<?php

namespace App\Traits;

use DB;
use File;
use ImgUploader;
use App\City;

trait CityTrait
{

    private function getCityIdsAndNumJobs($limit = 21, $countryid=101)
    {
        $ret_val = DB::table('jobs')
                        ->select('manage_job_location.city_id', DB::raw('COUNT(manage_job_location.city_id) AS num_jobs'))
                        ->groupBy('manage_job_location.city_id')
                        ->join('manage_job_location','manage_job_location.job_id','=','jobs.id')
                        ->where('manage_job_location.country_id','=',$countryid)
                        ->orderBy('num_jobs', 'DESC')
                        ->limit($limit)
                        ->get();
        if(count($ret_val) < 1){
            $ret_val = DB::table('jobs')
                ->select('manage_job_location.city_id', DB::raw('COUNT(manage_job_location.city_id) AS num_jobs'))
                ->groupBy('manage_job_location.city_id')
                ->join('manage_job_location','manage_job_location.job_id','=','jobs.id')
                ->orderBy('num_jobs', 'DESC')
                ->limit($limit)
                ->get();
        }
        return $ret_val;

    }

}
