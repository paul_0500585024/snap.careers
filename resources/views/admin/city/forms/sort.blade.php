{!! APFrmErrHelp::showErrorsNotice($errors) !!}
@include('flash::message')
<div class="form-body">
    <h3>Drag and Drop to Sort Cities</h3>
    {!! Form::select('lang', ['' => __('Select Language')]+$languages, 'en', array('class'=>'form-control select-multiple-language','style' => 'width:100%', 'id'=>'lang', 'onchange'=>'refreshCitySortData();')) !!}
    <div id="citySortDataDiv"></div>
</div>
@push('scripts') 
<script>
    $(document).ready(function () {
        refreshCitySortData();
		$('.select-multiple-language').select2({
			placeholder: "{{__('Select Language')}}",
			//            allowClear: true
		});		
    });
    function refreshCitySortData() {
        var language = $('#lang').val();
        $.ajax({
            type: "GET",
            url: "{{ route('city.sort.data') }}",
            data: {lang: language},
            success: function (responseData) {
                $("#citySortDataDiv").html('');
                $("#citySortDataDiv").html(responseData);
                /**************************/
                $('#sortable').sortable({
                    update: function (event, ui) {
                        var cityOrder = $(this).sortable('toArray').toString();
                        $.post("{{ route('city.sort.update') }}", {cityOrder: cityOrder, _method: 'PUT', _token: '{{ csrf_token() }}'})
                    }
                });
                $("#sortable").disableSelection();
                /***************************/
            }
        });
    }
</script> 
@endpush