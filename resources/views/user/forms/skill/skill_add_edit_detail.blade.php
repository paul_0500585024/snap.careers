@php
    $datajobSkills = array_values($jobSkills);
@endphp
<style>
    .select-single .select2-selection__rendered {line-height: 33px !important;}
    .select-single .select2-container .select2-selection--single {height: 37px !important;}
    .select-single .select2-selection__arrow {height: 36px !important;}
    .dropdown-menu>.active>a, .dropdown-menu>.active>a:hover, .dropdown-menu>.active>a:focus {
        color: #fff;
        text-decoration: none;
        outline: 0;
        background-color: #428bca;
    }
    .dropdown-menu>li>a{
        display: block;
        padding: 3px 20px;
        clear: both;
        font-weight: 400;
        line-height: 1.42857143;
        white-space: nowrap;
    }
</style>
<div class="row">
    <div class="col-5 mb-2" style="font-size:13px;color:#999">Skill</div>
    <div class="col-3 mb-2" style="font-size:13px;color:#999">Proficiency</div>
</div>

<form class="form" id="add_edit_profile_skill" method="POST" action="{{ route('saveupdate.front.profile.skills') }}">{{ csrf_field() }}
    <div id="list_profile_skill">
    @if (isset($user) && count($user->profileSkills))
        @php
            $old_sess = session()->getOldInput();
        @endphp
        @if(count($old_sess) > 0)
            @php
                $count_job_skill_id = count($old_sess['job_skill_id']);
                $count_job_experience_id = count($old_sess['job_experience_id']);
            @endphp
            @for ($counter = 0; $counter < $count_job_skill_id; $counter++)
                <div class="row">
                    <div class="col-5 mb-2 div-typeahead">
                        <div class='formrow {!! APFrmErrHelp::hasError($errors, "job_skill_id.$counter") !!}'>
                            <?php /*                        <input name="job_skill_id[]" class="form-control basicAutoComplete typeahead tt-query" autocomplete="off" type="text" value=""> */ ?>
                            {!! Form::text('job_skill_id[]', null, array('class'=>'form-control basicAutoComplete typeahead tt-query', 'autocomplete'=>'off')) !!}
                            {!! APFrmErrHelp::showErrors($errors, "job_skill_id.$counter") !!}
                        </div>
                    </div>
                    <div class="col-3 mb-2 select-single">
                        <div class='formrow {!! APFrmErrHelp::hasError($errors, "job_experience_id.$counter") !!}'>
                            @php $job_experience_id = $old_sess['job_experience_id'][$counter]; @endphp
                            {!! Form::select("job_experience_id[".$counter."]", ['' => __('Select Job Experience').' '.__('(required)')] + $jobExperiences, $job_experience_id, array('class'=>'form-control select-multiple-job-experience', 'style' => 'width:100%')) !!}
                            {!! APFrmErrHelp::showErrors($errors, "job_experience_id.$counter") !!}
                        </div>
                    </div>
                    <div class="col-2 mb-2">
                        <a href="javascript:" onclick="delete_profile_skill_new();" class="text text-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>
                    </div>
                </div>
            @endfor
        @else
            @php $counter = 0; @endphp
            @foreach ($user->profileSkills as $skill)
                <div class="row">
                    <div class="col-5 mb-2 div-typeahead">
                        <div class='formrow {!! APFrmErrHelp::hasError($errors, "job_skill_id.$counter") !!}'>
                            <?php /*                        <input name="job_skill_id[]" class="form-control basicAutoComplete typeahead tt-query" autocomplete="off" type="text" value="{{ $skill->getJobSkill('job_skill') }}"> */ ?>
                            {!! Form::text('job_skill_id[]', $skill->getJobSkill('job_skill'), array('class'=>'form-control basicAutoComplete typeahead tt-query', 'autocomplete'=>'off')) !!}
                            {!! APFrmErrHelp::showErrors($errors, "job_skill_id.$counter") !!}
                        </div>
                    </div>
                    <div class="col-3 mb-2 select-single">
                        <div class='formrow {!! APFrmErrHelp::hasError($errors, "job_experience_id.$counter") !!}'>
                            {!! Form::select('job_experience_id['.$counter.']', ['' => __('Select Job Experience').' '.__('(required)')] + $jobExperiences, $skill->job_experience_id, array('class'=>'form-control select-multiple-job-experience', 'id'=>'job_experience','style' => 'width:100%')) !!}
                            {!! APFrmErrHelp::showErrors($errors, "job_experience_id.$counter") !!}
                        </div>
                    </div>
                    <div class="col-2 mb-2">
                        <a href="javascript:" onclick="delete_profile_skill(this,'{{ $skill->id }}');" class="text text-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>
                    </div>
                </div>
                @php $counter++; @endphp
            @endforeach
        @endif


    @endif
    </div>

    <div class="row">
        <div class="col-12 mb-2 mt-2">
            <a href="javascript:" class="fnt-theme" onclick="add_profile_skill()">
                <i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Add Skill
            </a>
        </div>
    </div>

    <div class="row">
        <div class="col-6 mb-2 mt-2">
            <div class="btn-group btn-block">
                <button type="submit" class="btn btn-theme border-radius-0">{{__('Save')}}&nbsp;<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></button>
                &nbsp
                <button type="button" class="btn btn-cancel border-radius-0" onclick="location.href='{{route('my.profile.skills')}}'">{{__('Cancel')}}</button>
            </div>
    <?php /*        <button type="submit" class="btn btn-theme border-radius-0">Update Skill and Save<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></button>
            <button type="submit" class="btn btn-theme border-radius-0">Update Skill and Save<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></button>
    */ ?>
        </div>
    </div>
</form>

<script type="text/javascript">
    function delete_profile_skill(that, id) {
        $that = $(that);

        var msg = "{{__('Are you sure you want to delete?')}}";
        if (confirm(msg)) {
        $.post("{{ route('delete.front.profile.skill') }}", {id: id, _method: 'DELETE', _token: '{{ csrf_token() }}'})
            .done(function (response) {
                if (response == 'ok')
                {
                    $that.parent().parent().fadeOut(500, function () {
                        $(this).remove();
                    });
/*                    $('#skill_' + id)
                        .fadeOut(500, function () {
                            $(this).remove();
                        });*/
                } else
                {
                    alert('Request Failed!');
                }
            });
        }
    }
    function delete_profile_skill_new(identity){
        $this = $(identity);
        $this.parent().parent().fadeOut(500, function () {
            $(this).remove();
        });
        return false;
    }

    function add_profile_skill(){
        var counter = $('#list_profile_skill').children().length;
        html = '<div class="row">';
        html += '<div class="col-5 mb-2 div-typeahead">';
        html +=  '<div class="formrow {!! APFrmErrHelp::hasError($errors, "job_skill_id") !!}">';
        html +=   '{!! Form::text('job_skill_id[]', null, array('class'=>'form-control basicAutoComplete typeahead tt-query', 'autocomplete'=>'off')) !!}';
        html +=   '{!! APFrmErrHelp::showErrors($errors, "job_skill_id") !!}';
        html +=  '</div>';
        html += '</div>';
        html += '<div class="col-3 mb-2 select-single">';
        html +=  '<div class="formrow {!! APFrmErrHelp::hasError($errors, "job_experience_id") !!}">';
        @php
            $php_job_experience_array = ["" => __("Select Job Experience")." ".__("(required)")] + $jobExperiences;
            $php_job_experience_array_encode = json_encode($php_job_experience_array);
            echo "var job_experience = ". $php_job_experience_array_encode . ";\n";
        @endphp
        html +=  '<select name="job_experience_id['+counter+']" class="form-control select-multiple-job-experience" id="job_experience" style="width:100%">';
        $.each(job_experience, function( index, value ) {
            if(index == ''){
                html +=   '<option value="'+index+'" selected=selected>'+value+'</option>';
            }else{
                html +=   '<option value="'+index+'">'+value+'</option>';
            }
        });
        html +=  '</select>';
<?php /*        html +=   '{!! Form::select("job_experience_id[]", ["" => __("Select Job Experience")." ".__("(required)")] + $jobExperiences, null, array("class"=>"form-control select-multiple-job-experience", "id"=>"job_experience","style" => "width:100%")) !!}'; */ ?>
        html +=   '{!! APFrmErrHelp::showErrors($errors, "job_experience_id") !!}';
        html +=  '</div>';
        html += '</div>';
        html += '<div class="col-2 mb-2">';
        html += '<a href="javascript:" onclick="delete_profile_skill_new(this);" class="text text-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>';
        html += '</div>';
        html += '</div>';
        <?php /*        html += '$(document).ready(function(){'; */ ?>
        <?php /*        html += 'var skills = [<?php echo '"'.implode('","', $datajobSkills).'"' ?>];'; */ ?>
        //        html += '$(".typeahead").typeahead({';
        //        html += 'source: skills';
        //        html += '});';
        //        html += '$(".select-multiple-job-experience").select2({';
        //        html += 'placeholder: "Job Experience is required",';
        //        html += '});';
        <?php /*        html += 'alert();';
        html += '});';
        html += '</script>'; */ ?>

        $('#list_profile_skill').append(html);
        $(document).ready(function(){
            $('.select-multiple-job-experience').select2({
                placeholder: 'Job Experience is required',
            });
        });
    }
</script>

<script type="text/javascript">
    $(document).ready(function(){
        $('.select-multiple-job-experience').select2({
            placeholder: 'Job Experience is required',
        });
    });
    $('#list_profile_skill').bind('DOMNodeInserted', function(e) {
        var skills = [<?php echo '"'.implode('","', $datajobSkills).'"' ?>];
        $('div#list_profile_skill div.div-typeahead input.typeahead').typeahead({
            source: skills
        });
    });
</script>
