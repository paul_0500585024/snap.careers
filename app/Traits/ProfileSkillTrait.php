<?php

namespace App\Traits;

use Auth;
use DB;
use Illuminate\Support\Facades\Gate;
use Input;
use Carbon\Carbon;
use Redirect;
use App\User;
use App\ProfileSkill;
use App\JobSkill;
use App\JobExperience;
use App\Country;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Requests\ProfileSkillFormRequest;
use App\Http\Requests\UserFrontProfileSkillFormRequest;
use App\Helpers\DataArrayHelper;

trait ProfileSkillTrait
{
    public function showProfileSkills(Request $request, $user_id)
    {
        $user = $post = User::find($user_id);
        if (!Gate::allows('check-user', $post) && !Auth::guard('admin')->user()) {
            echo view('errors.404')->render();
            return;
        }

        $lang = app()->getLocale();

/*        $profileSkill = User::join('profile_skills','profile_skills.user_id','=','users.id')
            ->join('job_skills','job_skills.id','=','profile_skills.job_skill_id')
            ->join('job_experiences','job_experiences.job_experience_id','=','profile_skills.job_experience_id')
            ->where('users.id','=',$user_id)
            ->get();*/

        $profileSkill = DB::select("SELECT `job_experiences`.job_experience_id, job_experience, GROUP_CONCAT(job_skill SEPARATOR ', ') as job_skill 
FROM `users` INNER JOIN `profile_skills` ON `profile_skills`.`user_id` = `users`.`id` 
INNER JOIN `job_skills` ON `job_skills`.`job_skill_id` = `profile_skills`.`job_skill_id` 
INNER JOIN `job_experiences` ON `job_experiences`.`job_experience_id` = `profile_skills`.`job_experience_id` 
WHERE job_experiences.lang='$lang' AND job_skills.lang='$lang' AND users.id=$user_id
GROUP BY job_experience_id,job_experience ORDER BY `job_experiences`.job_experience_id ASC");



        $html = '<div class="col-mid-12"><table class="table table-bordered table-condensed">';
        if (isset($user) && count($profileSkill)):
            $data = array();
            foreach ($profileSkill as $skill):
                $html .= '<tr>
						<td><span class="text">' . $skill->job_experience . '</span></td>
						<td><span class="text">' . $skill->job_skill . '</span></td>
								</tr>';
            endforeach;
            foreach($data as $each){
                foreach($each as $every){
                }
/*                $html .= '<tr id="skill_' . $each->id . '">
						<td><span class="text">' . $each->job_experience . '</span></td>
						<td><span class="text">' . $each->job_skill . '</span></td>
								</tr>';*/
            }
        endif;

        echo $html . '</table></div>';
    }

    public function showApplicantProfileSkills(Request $request, $user_id)
    {
        $user = $post = User::find($user_id);
        if (!Gate::allows('check-user', $post) && !Auth::guard('admin')->user() && !Auth::guard('company')->user()) {
            echo view('errors.404')->render();
            return;
        }

        $html = '<div class="col-mid-12"><table class="table table-bordered table-condensed">';
        if (isset($user) && count($user->profileSkills)):
            foreach ($user->profileSkills as $skill):

                $html .= '<tr id="skill_' . $skill->id . '">
						<td><span class="text text-success">' . $skill->getJobSkill('job_skill') . '</span></td>
						<td><span class="text text-success">' . $skill->getJobExperience('job_experience') . '</span></td></tr>';
            endforeach;
             else:
           $html .='<tr><td><center><h5>No data found</h5></center></td></tr>';
        endif;

        echo $html . '</table></div>';
    }

    public function getProfileSkillForm(Request $request, $user_id)
    {
        $user = $post = User::find($user_id);
        if (!Gate::allows('check-user', $post) && !Auth::guard('admin')->user()) {
            return response()->json(array('success' => false, 'html' => view('errors.404')->render()),404);
        }

        $jobSkills = DataArrayHelper::defaultJobSkillsArray();
        $jobExperiences = DataArrayHelper::defaultJobExperiencesArray();

        $returnHTML = view('admin.user.forms.skill.skill_modal')
                ->with('user', $user)
                ->with('jobSkills', $jobSkills)
                ->with('jobExperiences', $jobExperiences)
                ->render();
        return response()->json(array('success' => true, 'html' => $returnHTML));
    }

    public function getFrontProfileSkillForm(Request $request, $user_id)
    {

        $user = $post = User::find($user_id);
        if (!Gate::allows('check-user', $post)) {
            $returnHTMLBody = view('errors.404')->render();
            $returnHTMLFooter = '<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>';
            $returnHTMLForm = '';
            $title = __('Error 404');
            return response()->json(array('success' => false,
                'html_body' => $returnHTMLBody, 'html_footer' => $returnHTMLFooter, 'html_form' => $returnHTMLForm,'title' => $title),404);
        }

        $user = User::findOrFail(Auth::user()->id);
        $jobSkillTexts = array();
        if(isset($user) && count($user->profileSkills)){
            foreach($user->profileSkills as $skill){
                $jobSkillTexts[] = $skill->getJobSkill('job_skill');
            }
        }

        $jobSkills = DataArrayHelper::langJobSkillsArray();
        $jobExperiences = DataArrayHelper::langJobExperiencesArray();

        $returnHTMLBody = view('user.forms.skill.skill_modal_body')
            ->with('user', $user)
            ->with('jobSkills', $jobSkills)
            ->with('jobExperiences', $jobExperiences)
            ->with('jobSkillTexts', $jobSkillTexts)
            ->render();
        $returnHTMLFooter = view('user.forms.skill.skill_modal_footer')
            ->with('user', $user)
            ->with('jobSkills', $jobSkills)
            ->with('jobExperiences', $jobExperiences)
            ->render();
        $returnHTMLForm = view('user.forms.skill.skill_modal_form')
            ->with('user', $user)
            ->with('jobSkills', $jobSkills)
            ->with('jobExperiences', $jobExperiences)
            ->render();
        $title = __('Add / Edit Skill');
        return response()->json(array('success' => true,
            'html_body' => $returnHTMLBody, 'html_footer' => $returnHTMLFooter, 'html_form' => $returnHTMLForm,'title' => $title));

    }

    public function storeProfileSkill(ProfileSkillFormRequest $request, $user_id)
    {

        $user = $post = User::find($user_id);
        if (!Gate::allows('check-user', $post) && !Auth::guard('admin')->user()) {
            return response()->json(array('success' => false, 'html' => view('errors.404')->render()),404);
        }

        $profileSkill = new ProfileSkill();
        $profileSkill->user_id = $user_id;
        $profileSkill->job_skill_id = $request->input('job_skill_id');
        $profileSkill->job_experience_id = $request->input('job_experience_id');
        $profileSkill->save();
        /*         * ************************************ */
        $returnHTML = view('admin.user.forms.skill.skill_thanks')->render();
        return response()->json(array('success' => true, 'status' => 200, 'html' => $returnHTML), 200);
    }

    public function getProfileSkillEditForm(Request $request, $user_id)
    {
        $user = $post = User::find($user_id);
        if (!Gate::allows('check-user', $post) && !Auth::guard('admin')->user()) {
            return response()->json(array('success' => false, 'html' => view('errors.404')->render()),404);
        }

        $skill_id = $request->input('skill_id');
        $jobSkills = DataArrayHelper::defaultJobSkillsArray();
        $jobExperiences = DataArrayHelper::defaultJobExperiencesArray();

        $profileSkill = ProfileSkill::find($skill_id);

        $returnHTML = view('admin.user.forms.skill.skill_edit_modal')
                ->with('user', $user)
                ->with('profileSkill', $profileSkill)
                ->with('jobSkills', $jobSkills)
                ->with('jobExperiences', $jobExperiences)
                ->render();
        return response()->json(array('success' => true, 'html' => $returnHTML));
    }

    public function getFrontProfileSkillEditForm(Request $request, $user_id)
    {
        $user = $post = User::find($user_id);
        if (!Gate::allows('check-user', $post)) {
            $returnHTMLBody = view('errors.404')->render();
            $returnHTMLFooter = '<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>';
            $returnHTMLForm = '';
            $title = __('Error 404');
            return response()->json(array('success' => false,
                'html_body' => $returnHTMLBody, 'html_footer' => $returnHTMLFooter, 'html_form' => $returnHTMLForm,'title' => $title),404);
        }

        $skill_id = $request->input('skill_id');

        $jobSkills = DataArrayHelper::langJobSkillsArray();
        $jobExperiences = DataArrayHelper::langJobExperiencesArray();

        $profileSkill = ProfileSkill::find($skill_id);

        $returnHTMLBody = view('user.forms.skill.skill_edit_modal_body')
            ->with('user', $user)
            ->with('profileSkill', $profileSkill)
            ->with('jobSkills', $jobSkills)
            ->with('jobExperiences', $jobExperiences)
            ->render();
        $returnHTMLFooter = view('user.forms.skill.skill_edit_modal_footer')
            ->with('user', $user)
            ->with('profileSkill', $profileSkill)
            ->with('jobSkills', $jobSkills)
            ->with('jobExperiences', $jobExperiences)
            ->render();
        $returnHTMLForm = view('user.forms.skill.skill_edit_modal_form')
            ->with('user', $user)
            ->with('profileSkill', $profileSkill)
            ->with('jobSkills', $jobSkills)
            ->with('jobExperiences', $jobExperiences)
            ->render();
        $title = __('Edit Skill');
        return response()->json(array('success' => true,
            'html_body' => $returnHTMLBody, 'html_footer' => $returnHTMLFooter, 'html_form' => $returnHTMLForm,'title' => $title));

    }

    public function updateProfileSkill(ProfileSkillFormRequest $request, $skill_id, $user_id)
    {
        $user = $post = User::find($user_id);
        if (!Gate::allows('check-user', $post) && !Auth::guard('admin')->user()) {
            return response()->json(array('success' => false, 'html' => view('errors.404')->render()),404);
        }

        $profileSkill = ProfileSkill::find($skill_id);
        $profileSkill->user_id = $user_id;
        $profileSkill->job_skill_id = $request->input('job_skill_id');
        $profileSkill->job_experience_id = $request->input('job_experience_id');
        $profileSkill->update();
        /*         * ************************************ */

        $returnHTML = view('admin.user.forms.skill.skill_edit_thanks')->render();
        return response()->json(array('success' => true, 'status' => 200, 'html' => $returnHTML), 200);
    }

    public function updateFrontProfileSkill(ProfileSkillFormRequest $request, $skill_id, $user_id)
    {
        $profileSkill = $post = ProfileSkill::find($skill_id);
        if (!Gate::allows('check-user-with-user-id', $post)) {
            $returnHTMLBody = view('errors.404')->render();
            $returnHTMLFooter = '<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>';
            $returnHTMLForm = '';
            $title = __('Error 404');
            return response()->json(array('success' => false,
                'html_body' => $returnHTMLBody, 'html_footer' => $returnHTMLFooter, 'html_form' => $returnHTMLForm,'title' => $title),404);
        }

        $profileSkill->user_id = $user_id;
        $profileSkill->job_skill_id = $request->input('job_skill_id');
        $profileSkill->job_experience_id = $request->input('job_experience_id');
        $profileSkill->update();
        /*         * ************************************ */

        $returnHTMLBody = view('user.forms.skill.skill_edit_thanks')->render();
        $returnHTMLFooter = '<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>';
        $returnHTMLForm = '';
        $title = __('Edit Skill');
        return response()->json(array('success' => true, 'status' => 200,
            'html_body' => $returnHTMLBody, 'html_footer' => $returnHTMLFooter, 'html_form' => $returnHTMLForm,'title' => $title), 200);

    }

    public function storeFrontProfileSkill(ProfileSkillFormRequest $request, $user_id)
    {
        $user = $post = User::find($user_id);
        if (!Gate::allows('check-user', $post)) {
            $returnHTMLBody = view('errors.404')->render();
            $returnHTMLFooter = '<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>';
            $returnHTMLForm = '';
            $title = __('Error 404');
            return response()->json(array('success' => false,
                'html_body' => $returnHTMLBody, 'html_footer' => $returnHTMLFooter, 'html_form' => $returnHTMLForm,'title' => $title),404);
        }

        $skills = $request->input('job_skill_id');
        if(!is_null($skills)) {
            foreach ($skills as $skill) {
                $jobskill = JobSkill::firstOrCreate(['job_skill' => $skill], ['is_active' => 1, 'lang' => app()->getLocale()]);

                if ($jobskill->wasRecentlyCreated) {
                    $job_skill_lang_lastid = JobSkill::getLangLastid();
                    $last_lang_id = $job_skill_lang_lastid->id;
                    $jobskill->update(['job_skill_id' => $last_lang_id, 'sort_order' => $last_lang_id]);
                }
//            $id = $jobskill->id;
//            $jobskill->update([''])
            }
        }

        $profileSkill = new ProfileSkill();
        $profileSkill->user_id = $user_id;
        $profileSkill->job_skill_id = $request->input('job_skill_id');
        $profileSkill->job_experience_id = $request->input('job_experience_id');
        $profileSkill->save();
        /*         * ************************************ */

        $returnHTMLBody = view('user.forms.skill.skill_thanks')->render();
        $returnHTMLFooter = '<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>';
        $returnHTMLForm = '';
        $title = __('Add Skill');
        return response()->json(array('success' => true, 'status' => 200,
            'html_body' => $returnHTMLBody, 'html_footer' => $returnHTMLFooter, 'html_form' => $returnHTMLForm,'title' => $title), 200);

    }


    public function saveUpdateProfileSkills(UserFrontProfileSkillFormRequest $request)
    {
        $user = $post = User::findOrFail(Auth::user()->id);
        if (!Gate::allows('check-user', $post) && !Auth::guard('admin')->user()) {
            echo view('errors.404')->render();
            return;
        }
        $profileSkill = new ProfileSkill();
        $profileSkill::where('user_id', $user['id'])->delete();

        $skills = $request->input('job_skill_id');

        if(!is_null($skills)) {
            foreach ($skills as $key=>$skill) {
                $jobskill = JobSkill::firstOrCreate(['job_skill' => $skill], ['is_active' => 1, 'lang' => app()->getLocale()]);

                if ($jobskill->wasRecentlyCreated) {
                    $job_skill_lang_lastid = JobSkill::getLangLastid();
                    $last_lang_id = $job_skill_lang_lastid->id;
                    $jobskill->update(['job_skill_id' => $last_lang_id, 'sort_order' => $last_lang_id]);
                }

                $profileSkill = new ProfileSkill();
                $profileSkill->user_id = $user['id'];
                $profileSkill->job_skill_id = $jobskill->id;
                $job_experience_id = $request->input('job_experience_id');
                $profileSkill->job_experience_id = $job_experience_id[$key];
                $profileSkill->save();
            }
        }
        flash(__('You have updated your skills successfully'))->success();
        return \Redirect::route('my.profile.skills');
    }

    public function deleteProfileSkill(Request $request)
    {
        $id = $request->input('id');
        try {
            $profileSkill = $post = ProfileSkill::findOrFail($id);
            if (!Gate::allows('check-user-with-user-id', $post) && !Auth::guard('admin')->user()) {
                echo 'notok';
                return ;
            }
            $profileSkill->delete();
            echo 'ok';
        } catch (ModelNotFoundException $e) {
            echo 'notok';
        }
    }

}
