<div class="modal-body">
    <div class="form-body">
        <div class="form-group" id="div_job_skill_id">
            <label for="job_skill_id" class="bold">Job Skill</label>
            <?php
            $job_skill_id = (isset($profileSkill) ? $profileSkill->job_skill_id : null);
            ?>
            {!! Form::select('job_skill_id', ['' => __('Select Skill').' '.__('(required)')]+$jobSkills, $job_skill_id, array('class'=>'form-control select-multiple-skill','style' => 'width:100%', 'id'=>'job_skill_id')) !!} <span class="help-block job_skill_id-error"></span> </div>
        <div class="form-group" id="div_job_experience_id">
            <label for="job_experience_id" class="bold">Job Experience</label>
            <?php
            $job_experience_id = (isset($profileSkill) ? $profileSkill->job_experience_id : null);
            ?>
            {!! Form::select('job_experience_id', ['' => __('Select Experience').' '.__('(required)')]+$jobExperiences, $job_experience_id, array('class'=>'form-control select-multiple-experience','style' => 'width:100%', 'id'=>'job_experience_id')) !!} <span class="help-block job_experience_id-error"></span> </div>
    </div>
</div>
@push('scripts') 
<script>
    $(document).ready(function () {

		$('.select-multiple-skill').select2({
			placeholder: "{{__('Select Skill').' '.__('(required)')}}",
			//            allowClear: true
		});


		$('.select-multiple-experience').select2({
			placeholder: "{{__('Select Experience').' '.__('(required)')}}",
			//            allowClear: true
		});
		
    });
</script> 
@endpush
