<?php

namespace App\Traits;

trait Company
{
 
    public function scopeCompany($query,$id)
    {
         return $query->where('country_id', '=', $id);
    }
}
